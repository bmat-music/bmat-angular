/**
 * BMAT Angular Module
 */
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { BMATForms } from './forms';
import { BMATLayout } from './layout';
import { BMATMediaPlayers } from './media-players';
import { BMATSharedModule } from './shared';
import { BMATWidgets } from './widgets';

@NgModule({
  imports: [
    BMATForms,
    BMATLayout,
    BMATMediaPlayers,
    BMATSharedModule,
    BMATWidgets,
    CommonModule,
  ],
  declarations: [
  ],
  exports: [
    BMATForms,
    BMATLayout,
    BMATMediaPlayers,
    BMATSharedModule,
    BMATWidgets,
  ],
  providers: [],
})
export class BMATAngular {
  // tslint:disable-next-line:function-name
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: BMATAngular,
      providers: [
      ],
    };
  }
}
