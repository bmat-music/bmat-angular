/**
 * Jasmine Spec BMSidenavContainerComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { BMSidenavContainerComponent } from 'lib/layout/bm-sidenav';

describe('BMSidenavContainerComponent', () => {
  let component: BMSidenavContainerComponent;
  let fixture: ComponentFixture<BMSidenavContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NoopAnimationsModule],
      declarations: [BMSidenavContainerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMSidenavContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('toggleOpen should toggle the open property', () => {
    component.open = true;
    component.toggle();
    expect(component.open).toBeFalsy();
  });

  it('updating open before AfterViewInit whould not break the stack', () => {
    // tslint:disable-next-line:no-any
    component.collapsableChilds = null as any;
    component.open = true;
    // tslint:disable-next-line:no-function-expression
    expect(function() {
      component.toggle();
    }).not.toThrowError();
  });

  describe('Animation triggers', () => {
    it('should emit openMenu(true) if going from collapse to expand', () => {
      spyOn(component.openMenu, 'emit');
      // tslint:disable-next-line:no-any
      component.animationDone(<any>{
        fromState: 'collapse',
        toState: 'expand',
      });
      expect(component.openMenu.emit).toHaveBeenCalledWith(true);
    });

    it('should emit openMenu(false) if going from expand to collapse', () => {
      spyOn(component.openMenu, 'emit');
      // tslint:disable-next-line:no-any
      component.animationDone(<any>{
        fromState: 'expand',
        toState: 'collapse',
      });
      expect(component.openMenu.emit).toHaveBeenCalledWith(false);
    });
  });
});
