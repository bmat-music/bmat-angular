/**
 * Angular Component BMSidenav
 * Side navigation menu
 */
import {
  animate,
  AnimationEvent,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';

import { BMCollapsableDirective } from '../../shared/directives/collapsable';

@Component({
  selector: 'bm-sidenav',
  templateUrl: 'bm-sidenav.component.html',
  styleUrls: ['bm-sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('stretch', [
      state('expand', style({ width: '14rem', 'max-width': '100%' })),
      state('collapse', style({ width: '4em', 'max-width': 'initial' })),
      transition('expand => collapse', [
        style({
          width: '14rem',
          'max-width': '100%',
        }),
        animate('0.2s ease-in'),
      ]),
      transition('collapse => expand', [
        style({
          width: '4em',
          'max-width': 'initial',
        }),
        animate('0.2s ease-out'),
      ]),
    ]),
    trigger('rotate', [
      state('expand', style({ transform: 'rotate(0)' })),
      state('collapse', style({ transform: 'rotate(-180deg)' })),
      transition('expand => collapse', animate('0.2s ease-in')),
      transition('collapse => expand', animate('0.2s ease-out')),
    ]),
  ],
})
export class BMSidenavContainerComponent {
  @ContentChildren(BMCollapsableDirective)
  public collapsableChilds!: QueryList<BMCollapsableDirective>;
  @HostBinding('attr.role') public role = 'navigation';
  @HostBinding('attr.open')
  @Input()
  set open(v: boolean) {
    this._isOpen = v;
    if (this.collapsableChilds) {
      this.collapsableChilds.forEach(
        (child: BMCollapsableDirective) => (child.collapse = v ? null : true),
      );
    }
  }
  get open() {
    return this._isOpen;
  }

  @Output() public openMenu = new EventEmitter<boolean>();

  private _isOpen!: boolean;

  public toggle() {
    this.open = !this.open;
  }

  public animationDone(event: AnimationEvent) {
    if (event.fromState === 'expand' && event.toState === 'collapse') {
      this.openMenu.emit(false);
    }
    if (event.fromState === 'collapse' && event.toState === 'expand') {
      this.openMenu.emit(true);
    }
  }
}
