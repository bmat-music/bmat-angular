/**
 * Angular Component BMContentComponent
 */
import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'bm-content-container',
  templateUrl: 'bm-content.component.html',
  styleUrls: ['bm-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BMContentComponent {}
