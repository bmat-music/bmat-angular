/**
 * Jasmine Spec BMContentComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMContentComponent } from 'lib/layout/bm-content';

describe('BMContentComponent', () => {
  let component: BMContentComponent;
  let fixture: ComponentFixture<BMContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [BMContentComponent],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });
});
