/**
 * Jasmine Spec BMHeaderComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMHeaderComponent } from 'lib/layout/bm-header';

describe('BMHeaderComponent', () => {
  let component: BMHeaderComponent;
  let fixture: ComponentFixture<BMHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [BMHeaderComponent],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });
});
