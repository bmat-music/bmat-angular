/**
 * Angular Component. BMHeaderComponent
 * Header Toolbar
 */
import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'bm-header',
  templateUrl: 'bm-header.component.html',
  styleUrls: ['bm-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BMHeaderComponent {
  @HostBinding('attr.role') public role = 'heading';
}
