/**
 * BMAT Layout Module
 */
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { BMATSharedModule } from '../shared';
import { BMContentComponent } from './bm-content';
import { BMHeaderComponent } from './bm-header';
import { BMSidenavContainerComponent } from './bm-sidenav';

@NgModule({
  imports: [CommonModule, BMATSharedModule],
  declarations: [
    BMHeaderComponent,
    BMSidenavContainerComponent,
    BMContentComponent,
  ],
  exports: [BMHeaderComponent, BMSidenavContainerComponent, BMContentComponent],
  providers: [],
})
export class BMATLayout {
  // tslint:disable-next-line:function-name
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: BMATLayout,
      providers: [],
    };
  }
}
