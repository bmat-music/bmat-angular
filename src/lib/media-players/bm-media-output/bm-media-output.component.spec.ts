/**
 * Jasmine Spec BMMediaOutputComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMATMediaPlayers } from 'lib/media-players';
import { BMMediaOutputComponent } from 'lib/media-players/bm-media-output';

describe('BMMediaOutputComponent', () => {
  let component: BMMediaOutputComponent;
  let fixture: ComponentFixture<BMMediaOutputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BMATMediaPlayers.forRoot()],
      declarations: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMMediaOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('should update innerTime if received a non negative time', () => {
    component.time = 60;
    expect(component.innerTime).toEqual('01:00');
  });

  it('should use the property format to format the received time', () => {
    component.format = 'HH:MM:SS';
    component.time = 60;
    expect(component.innerTime).toEqual('00:01:00');
  });

  it('should not update innerTime if received an invalid input', () => {
    component.time = 60;
    // tslint:disable-next-line:no-any
    component.time = null as any;
    expect(component.innerTime).toEqual('01:00');
  });

  it('should set innert time to zero if received a negative input', () => {
    component.time = -60;
    expect(component.innerTime).toEqual('00:00');
  });
});
