/**
 * Angular Component. BMMediaOutput
 * Used in media players. To transclude and allow definition of custom controls
 */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { ParseDurationPipe } from '../../shared/pipes/format-duration';

@Component({
  selector: 'bm-media-output',
  templateUrl: 'bm-media-output.component.html',
  styleUrls: ['bm-media-output.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BMMediaOutputComponent {
  @Input() public format = 'MM:SS';

  @Input()
  set time(v: number) {
    if (v || v === 0) {
      this.innerTime = this.formater.transform(Math.max(0, v), this.format);
      this.changeDetectorRef.markForCheck();
    }
  }

  public innerTime!: string;

  private formater: ParseDurationPipe = new ParseDurationPipe();

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    this.changeDetectorRef.detach();
  }
}
