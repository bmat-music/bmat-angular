/**
 * BMAT MediaPlayers Module
 */
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { BMATAudioPlayerComponent } from './audio';
import { BMMediaOutputComponent } from './bm-media-output';

import { BMATSharedModule } from '../shared';

@NgModule({
  imports: [CommonModule, BMATSharedModule],
  declarations: [BMATAudioPlayerComponent, BMMediaOutputComponent],
  exports: [BMATAudioPlayerComponent, BMMediaOutputComponent],
  providers: [],
})
export class BMATMediaPlayers {
  // tslint:disable-next-line:function-name
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: BMATMediaPlayers,
      providers: [],
    };
  }
}
