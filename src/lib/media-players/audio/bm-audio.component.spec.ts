/**
 * Jasmine Spec BMATAudioPlayerComponent
 */
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { BMATMediaPlayers } from 'lib/media-players';
import { BMATAudioPlayerComponent } from 'lib/media-players/audio';
import { restoreEvent } from 'testing/restore-event';

describe('BMATAudioPlayerComponent', () => {
  let component: BMATAudioPlayerComponent;
  let fixture: ComponentFixture<BMATAudioPlayerComponent>;
  let player: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BMATMediaPlayers.forRoot()],
      declarations: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMATAudioPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    player = fixture.debugElement.query(By.css('audio'));

    restoreEvent();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  describe('reflection properties', () => {
    it('setting truthy src should set the src of the inner audio player', () => {
      component.src = '/SRC';
      fixture.detectChanges();
      expect(player.nativeElement.src.search(/\/SRC$/)).not.toEqual(-1);
    });

    it('setting a truthy src should enable the play button', () => {
      component.src = '/SRC';
      fixture.detectChanges();
      expect(component.isDisabled).toBeFalsy();
    });

    it('setting a falsy src should disable the play button', () => {
      component.src = '';
      fixture.detectChanges();
      expect(component.isDisabled).toBeTruthy();
    });
  });

  describe('player event bindings', () => {
    it('should set the isLoading to true after a loadeddata event', () => {
      player.triggerEventHandler('loadeddata', new Event('loadeddata'));
      fixture.detectChanges();
      expect(component.isLoading).toBeTruthy();
    });

    it('should set the isLoading to true after a waiting event', () => {
      player.triggerEventHandler('waiting', new Event('waiting'));
      fixture.detectChanges();
      expect(component.isLoading).toBeTruthy();
    });

    it('should set the isLoading to false after a canplay event', () => {
      player.triggerEventHandler('canplay', new Event('canplay'));
      fixture.detectChanges();
      expect(component.isLoading).toBeFalsy();
    });

    it('should set the isPlaying to false after a ended event', () => {
      player.triggerEventHandler('ended', new Event('ended'));
      fixture.detectChanges();
      expect(component.isPlaying).toBeFalsy();
    });

    it('should set the isPlaying to true after a play event', () => {
      player.triggerEventHandler('play', new Event('play'));
      fixture.detectChanges();
      expect(component.isPlaying).toBeTruthy();
    });

    it('should emit a component play event after a play event', () => {
      spyOn(component.play, 'emit');
      player.triggerEventHandler('play', new Event('play'));
      fixture.detectChanges();
      expect(component.play.emit).toHaveBeenCalled();
    });

    it('should set the isPlaying to false after a pause event', () => {
      player.triggerEventHandler('pause', new Event('pause'));
      fixture.detectChanges();
      expect(component.isPlaying).toBeFalsy();
    });

    it('should emit a component pause event after a pause event', () => {
      spyOn(component.pause, 'emit');
      player.triggerEventHandler('pause', new Event('pause'));
      fixture.detectChanges();
      expect(component.pause.emit).toHaveBeenCalled();
    });

    it('should set the isLoading to false and isPlaying to true after a playing event', () => {
      player.triggerEventHandler('playing', new Event('playing'));
      fixture.detectChanges();
      expect(component.isLoading).toBeFalsy();
      expect(component.isPlaying).toBeTruthy();
    });
  });

  describe('toggle play', () => {
    class FakeDate {
      private callsMade = 0;

      constructor(private values: number[]) {}

      public getTime() {
        this.callsMade += 1;
        return this.values[
          Math.min(this.callsMade - 1, this.values.length - 1)
        ];
      }
    }

    it('should set isPlaying to false if it was true and mouse was holded less than HOLD time', () => {
      const fakeDate = new FakeDate([
        0,
        BMATAudioPlayerComponent.HOLD_TIME - 1,
        0,
        BMATAudioPlayerComponent.HOLD_TIME,
      ]);
      spyOn(global, 'Date').and.callFake(() => fakeDate);

      spyOn(player.nativeElement, 'play');
      component.isPlaying = false;
      component.onButtonMousedown(new MouseEvent('mousedown'));
      component.togglePlay();
      expect(player.nativeElement.play).toHaveBeenCalled();

      spyOn(player.nativeElement, 'pause');
      component.isPlaying = true;
      component.onButtonMousedown(new MouseEvent('mousedown'));
      component.togglePlay();
      expect(player.nativeElement.pause).toHaveBeenCalled();
    });

    it('should set isPlaying to false if it was true and mouse was holded more than HOLD time', () => {
      const fakeDate = new FakeDate([
        0,
        BMATAudioPlayerComponent.HOLD_TIME + 1,
      ]);
      spyOn(global, 'Date').and.callFake(() => fakeDate);

      spyOn(player.nativeElement, 'play');
      component.isPlaying = false;
      component.onButtonMousedown(new MouseEvent('mousedown'));
      component.togglePlay();
      expect(player.nativeElement.play).not.toHaveBeenCalled();
    });
  });
});
