/**
 * Angular Component. Audio media player
 */
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  Renderer,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { tap } from 'rxjs/operators';

import { BMProgressComponent } from '../../shared/components/progress-bar';
import { BMMediaOutputComponent } from '../bm-media-output';

export type PreloadValues = 'auto' | 'metadata' | 'none';

@Component({
  selector: 'bm-audio-player',
  templateUrl: 'bm-audio.component.html',
  styleUrls: ['bm-audio.component.scss', 'bm-audio.shadow-dom.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BMATAudioPlayerComponent implements AfterContentInit {
  /**
   * Threshold of time to consider a mousedown a hold event.
   */
  public static HOLD_TIME = 200;
  /**
   * Playback rate when playing audio in accelerated mode.
   */
  public static ACCELERATED_RATE = 3;

  // Forward attributes to audio
  @Input() public autoplay = false;
  @Input() public controls = false;
  @Input() public loop = false;
  @Input() public muted = false;
  @Input() public preload: PreloadValues = 'none';
  @Input()
  set src(v: string) {
    if (v) {
      this.renderer.setElementProperty(
        this.audioElement.nativeElement,
        'src',
        v,
      );
      this.isDisabled = false;
    } else {
      this.isDisabled = true;
    }
  }

  /**
   * Echo of play HTMLAudioElement event
   */
  @Output() public play = new EventEmitter<null>();
  /**
   * Echo of play HTMLAudioElement event
   */
  @Output() public pause = new EventEmitter<null>();
  @ViewChild('audio') public audioElement!: ElementRef;

  public isDisabled = true;
  public isLoading = false;
  public isPlaying = false;

  @ContentChild(BMProgressComponent) private progressBar!: BMProgressComponent;
  @ContentChild(BMMediaOutputComponent)
  private mediaOutput!: BMMediaOutputComponent;

  private _mouseDownTime!: number;
  private _timerSubscription!: Subscription;
  private _playBackTimerSubscription!: Subscription;
  private _isMouseDown = false;
  private _progressUpdater$ = timer(500, 500).pipe(
    tap(this.calculateElapsed.bind(this)),
  );

  constructor(private renderer: Renderer) {}

  public ngAfterContentInit() {
    if (this.progressBar) {
      this.progressBar.value = 0;
      this.progressBar.staticBar = true;
    }
    if (this.mediaOutput) {
      this.mediaOutput.time = 0;
    }
  }

  public onButtonMousedown(event: MouseEvent) {
    event.preventDefault();
    this._mouseDownTime = new Date().getTime();
    this._isMouseDown = true;
    this._playBackTimerSubscription = timer(
      BMATAudioPlayerComponent.HOLD_TIME,
    ).subscribe(() => {
      const now = new Date().getTime();
      const deltaTime = now - this._mouseDownTime;
      if (this._isMouseDown && deltaTime > BMATAudioPlayerComponent.HOLD_TIME) {
        this.renderer.setElementProperty(
          this.audioElement.nativeElement,
          'playbackRate',
          BMATAudioPlayerComponent.ACCELERATED_RATE,
        );
      }
    });
  }

  public onloadeddata(event: Event) {
    event.stopPropagation();
    this.isLoading = true;
  }

  public ondurationchange() {
    this.calculateElapsed();
  }

  public oncanplay(event: Event) {
    event.stopPropagation();
    this.isLoading = false;
  }

  public onplay(event: Event) {
    event.stopPropagation();
    this.isPlaying = true;
    this.play.emit();
    if (this.progressBar) {
      this._timerSubscription = this._progressUpdater$.subscribe();
    }
  }

  public onplaying(event: Event) {
    event.stopPropagation();
    this.isPlaying = true;
    this.isLoading = false;
  }

  public onpause(event: Event) {
    event.stopPropagation();
    this.isPlaying = false;
    this.pause.emit();
    if (this._timerSubscription) {
      this._timerSubscription.unsubscribe();
    }
  }

  public onwaiting(event: Event) {
    event.stopPropagation();
    this.isLoading = true;
  }

  public onended(event: Event) {
    this.isPlaying = false;
    event.stopPropagation();
    if (this._timerSubscription) {
      this._timerSubscription.unsubscribe();
      this.progressBar.value = 100;
    }
  }

  public togglePlay() {
    this.renderer.setElementProperty(
      this.audioElement.nativeElement,
      'playbackRate',
      1,
    );
    this._isMouseDown = false;
    const now = new Date().getTime();
    const deltaTime = now - this._mouseDownTime;
    this._playBackTimerSubscription.unsubscribe();
    if (deltaTime > BMATAudioPlayerComponent.HOLD_TIME) {
      return;
    }

    if (this.isPlaying) {
      this.renderer.invokeElementMethod(
        this.audioElement.nativeElement,
        'pause',
      );
    } else {
      this.renderer.invokeElementMethod(
        this.audioElement.nativeElement,
        'play',
      );
    }
  }

  private calculateElapsed() {
    const nativeAudio = <HTMLAudioElement>this.audioElement.nativeElement;
    if (this.progressBar) {
      const currentPosition = Math.min(
        100,
        100 * nativeAudio.currentTime / nativeAudio.duration,
      );
      this.progressBar.value =
        !Number.isNaN(currentPosition) && currentPosition > 0
          ? currentPosition
          : 0;
    }
    if (this.mediaOutput) {
      this.mediaOutput.time = nativeAudio.currentTime;
    }
  }
}
