/**
 * Angular Component. Input to introduce duration in HH:MM:SS.
 * Maps the value to second when submiting forms or accessing DOM.
 */
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  forwardRef,
  Input,
  QueryList,
  Renderer2,
  ViewChildren,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'bm-input-duration',
  templateUrl: 'bm-duration-input.component.html',
  styleUrls: ['bm-duration-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => BMDurationInputComponent),
      multi: true,
    },
  ],
})
export class BMDurationInputComponent
  implements AfterViewInit, ControlValueAccessor {
  @ViewChildren('hours, minutes, seconds', { read: ElementRef })
  public viewTimeValues!: QueryList<ElementRef>;

  @Input()
  set value(val: number) {
    this.writeValue(val);
  }
  get value(): number {
    return this.computeTotalTime();
  }

  // tslint:disable-next-line:prefer-array-literal
  private timeValues: number[] = new Array(3).fill(null);

  constructor(private renderer: Renderer2) {}

  public format(index: number, $event: Event): void {
    const inputEle = <HTMLInputElement>$event.target;
    let value = Number.parseInt(inputEle.value, 10);

    if (Number.isNaN(value)) {
      value = 0;
    }

    if (index && value >= 60) {
      value = (value % 100) % 60;
    }

    this.timeValues[index] = value;

    this.propagateChange(this.computeTotalTime());

    const formatedVal =
      this.timeValues[index] < 10
        ? '0' + this.timeValues[index]
        : this.timeValues[index];
    this.renderer.setProperty(inputEle, 'value', formatedVal);
  }

  // tslint:disable-next-line:no-any
  public writeValue(value: any) {
    if (!value && value !== 0) {
      // tslint:disable-next-line:prefer-array-literal
      this.timeValues = new Array(3).fill(null);
    } else {
      let coerceInteger = Number.parseInt(value, 10);
      if (Number.isNaN(coerceInteger)) {
        coerceInteger = 0;
      }

      coerceInteger = Math.max(0, coerceInteger);
      [
        Math.floor(coerceInteger / 3600),
        Math.floor(coerceInteger / 60) % 60,
        coerceInteger % 60,
      ].forEach((val, i) => {
        this.timeValues[i] = val;
      });
    }
    this.updateInputValues();
  }

  // tslint:disable-next-line:no-any
  public registerOnChange(fn: (_: any) => {}) {
    this.propagateChange = fn;
  }

  // tslint:disable-next-line:no-empty
  public registerOnTouched() {}

  public ngAfterViewInit() {
    this.updateInputValues();
  }

  // tslint:disable:no-any
  // tslint:disable:no-empty
  private propagateChange = (_: any) => {};

  private computeTotalTime(): number {
    return this.timeValues.reduce(
      (acum: number, curr: number | null, i): number => {
        if (curr) {
          acum += (curr as number) * 60 ** (2 - i);
        }
        return acum;
      },
      0,
    );
  }

  private updateInputValues() {
    if (this.viewTimeValues) {
      this.viewTimeValues.forEach((element, index) => {
        const value = this.timeValues[index];
        const formatedValue = value < 10 ? '0' + value : value;
        this.renderer.setProperty(
          element.nativeElement,
          'value',
          formatedValue,
        );
      });
    }
  }
}
