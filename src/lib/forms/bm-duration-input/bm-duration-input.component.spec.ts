/**
 * Jasmine Spec. BMDurationInputComponent.
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { BMDurationInputComponent } from './bm-duration-input.component';

describe('BMDurationInputComponent isolated tests', () => {
  let component: BMDurationInputComponent;
  let fixture: ComponentFixture<BMDurationInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [BMDurationInputComponent],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMDurationInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('should draw 3 inputs', () => {
    const inputs = fixture.debugElement.queryAll(By.css('input'));
    expect(inputs.length).toEqual(3);
  });

  it('should set the placeholders to HH:MM:SS', () => {
    const placeholders = fixture.debugElement
      .queryAll(By.css('input'))
      .map(db => db.nativeElement)
      .map(ne => ne.getAttribute('placeholder'))
      .join(':');
    expect(placeholders).toEqual('HH:MM:SS');
  });

  describe('should add zeros to produce 2 digit cells', () => {
    it('in seconds', () => {
      component.value = 1;
      fixture.detectChanges();
      const value = fixture.debugElement
        .queryAll(By.css('input'))
        .map(db => db.nativeElement)
        .map(ne => ne.value)
        .join(':');
      expect(value).toEqual('00:00:01');
    });

    it('in minutes', () => {
      component.value = 60;
      fixture.detectChanges();
      const value = fixture.debugElement
        .queryAll(By.css('input'))
        .map(db => db.nativeElement)
        .map(ne => ne.value)
        .join(':');
      expect(value).toEqual('00:01:00');
    });

    it('in hours', () => {
      component.value = 3600;
      fixture.detectChanges();
      const value = fixture.debugElement
        .queryAll(By.css('input'))
        .map(db => db.nativeElement)
        .map(ne => ne.value)
        .join(':');
      expect(value).toEqual('01:00:00');
    });
  });

  it('should map seconds value to hh:mm:ss', () => {
    component.value = 3695;
    fixture.detectChanges();
    const value = fixture.debugElement
      .queryAll(By.css('input'))
      .map(db => db.nativeElement)
      .map(ne => ne.value)
      .join(':');
    expect(value).toEqual('01:01:35');
  });

  it('should map invalid value to 0', () => {
    // tslint:disable-next-line:no-any
    (<any>component).value = 'xxx';
    fixture.detectChanges();
    const value = fixture.debugElement
      .queryAll(By.css('input'))
      .map(db => db.nativeElement)
      .map(ne => ne.value)
      .join(':');
    expect(value).toEqual('00:00:00');
  });

  it('should map a invalid input value to 0', () => {
    // tslint:disable-next-line:no-any
    const input = fixture.debugElement.query(By.css('input')).nativeElement;
    input.value = 'X';
    // tslint:disable-next-line:no-any
    component.format(0, <any>{ target: input });
    fixture.detectChanges();
    expect(input.value).toEqual('00');
  });

  it('should set the lower bound to 0', () => {
    const mins = fixture.debugElement
      .queryAll(By.css('input'))
      .map(db => db.nativeElement)
      .map(ne => ne.getAttribute('min'));
    expect(mins).toEqual(['0', '0', '0']);
  });

  it('upper bound should not overflow minutes or seconds', () => {
    const maxs = fixture.debugElement
      .queryAll(By.css('input'))
      .map(db => db.nativeElement)
      .map(ne => ne.getAttribute('max'));
    expect(maxs[1]).toEqual('59');
    expect(maxs[2]).toEqual('59');
  });

  it('should update component value according to inner inputs values', () => {
    const vals = [1, 1, 35];

    const inputs = fixture.debugElement.queryAll(By.css('input'));

    vals.forEach((val, i) => {
      inputs[i].nativeElement.value = val;
      inputs[i].triggerEventHandler('input', {
        target: inputs[i].nativeElement,
      });
      fixture.detectChanges();
    });
    expect(component.value).toEqual(3695);
  });

  it('should prevent seconds overflows in inner inputs', () => {
    const input = fixture.debugElement.queryAll(By.css('input'))[2];
    input.nativeElement.value = '123';
    input.triggerEventHandler('input', { target: input.nativeElement });
    fixture.detectChanges();
    expect(input.nativeElement.value).toEqual('23');
  });

  it('should prevent minutes overflows in inner inputs', () => {
    const input = fixture.debugElement.queryAll(By.css('input'))[1];
    input.nativeElement.value = '60';
    input.triggerEventHandler('input', { target: input.nativeElement });
    fixture.detectChanges();
    expect(input.nativeElement.value).toEqual('00');
  });
});
