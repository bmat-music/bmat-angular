/**
 * Angular Component BMFileUploader
 * Drag & Drop File uploader
 */
import {
  ChangeDetectionStrategy,
  Component,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  Renderer2,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { FileUploadBase } from '../file-uploader-base';

@Component({
  selector: 'bm-click-file-upload',
  templateUrl: 'bm-click-file-upload.component.html',
  styleUrls: ['bm-click-file-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => BMClickFileUploaderComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BMClickFileUploaderComponent extends FileUploadBase {
  @Input() public multiple!: boolean;
  @HostBinding('class.drag-hover') public dragHover = false;

  constructor(renderer: Renderer2) {
    super(renderer);
  }

  @HostListener('dragover', ['$event'])
  public ondragover(event: DragEvent) {
    event.preventDefault();
    this._onTouched(event);
    this.dragHover = true;
  }
  @HostListener('dragleave')
  public ondragleave() {
    this.dragHover = false;
  }
  @HostListener('drop', ['$event'])
  public ondrop(event: DragEvent) {
    event.dataTransfer.dropEffect = 'copy';
    const { dataTransfer } = event;
    this.readFiles(dataTransfer.files);
    event.preventDefault();
  }

  public onInputFileChange(event: Event) {
    const input = <HTMLInputElement>event.target;
    event.preventDefault();
    if (input.files) {
      this.readFiles(input.files);
      this._onChange(input.files);
    }
  }

  public onInputBlur(event: Event) {
    this._onTouched(event);
  }
}
