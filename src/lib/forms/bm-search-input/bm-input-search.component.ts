/**
 * Angular Component. BMInputSearchComponent.
 * Input with icon
 */
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  QueryList,
  Renderer2,
  ViewChild,
  ViewChildren,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'bm-input-search',
  styleUrls: ['bm-input-search.component.scss'],
  templateUrl: 'bm-input-search.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => BMInputSearchComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BMInputSearchComponent implements ControlValueAccessor {
  @ViewChild('input') public input!: ElementRef;
  @ViewChildren('sugg') public suggestionList!: QueryList<ElementRef>;
  /**
   * Input values to directly copy to inner checkbox
   */
  @Input() public name = '';
  @Input() public placeholder = '';
  @Input() public tabIndex = 0;
  @Input() public required!: boolean;
  @Input() public disabled!: boolean;
  @Input() public readonly!: boolean;
  // tslint:disable-next-line:no-reserved-keywords
  @Input() public type = 'text';
  // tslint:disable-next-line:no-input-rename
  @Input('aria-label') public ariaLabel!: string;
  // tslint:disable-next-line:no-input-rename
  @Input('aria-labelledby') public ariaLabelledby!: string;

  @Input()
  // tslint:disable-next-line:no-any
  set value(v: any) {
    if (v) {
      this._renderer.setProperty(this.input.nativeElement, 'value', v);
    }
  }
  get value() {
    return this.input.nativeElement.value;
  }

  /**
   * Suggestions specifics
   */
  public showSuggestions = false;
  public filteredSuggestions!: string[];

  @Input() public minSuggestionChars = 2;
  @Input() public suggestions!: string[];

  @Output() public search = new EventEmitter<string>();

  constructor(private _renderer: Renderer2) {}

  public inputKeyUp(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 40: // Down arrow
        if (this.suggestionList.first) {
          (this.suggestionList.first.nativeElement as HTMLElement).focus();
        }
        break;
      case 27: // Escape
        this.closeSuggestions();
        break;
      default:
        const value = (<HTMLInputElement>event.target).value.toLowerCase();
        if (
          value.length >= this.minSuggestionChars &&
          Array.isArray(this.suggestions)
        ) {
          this.showSuggestions = true;
          this.filteredSuggestions = this.suggestions
            .map(item => item.toString().toLowerCase())
            .filter(item => item.search(value) !== -1);
        } else {
          this.showSuggestions = false;
        }
        return;
    }
  }

  // tslint:disable-next-line:no-any
  public suggestionKeyUp(event: KeyboardEvent, index: number, value: any) {
    const suggestionLi = this.suggestionList.toArray();
    switch (event.keyCode) {
      case 40: // Up arrow
        if (index === this.suggestions.length - 1) {
          (this.suggestionList.first.nativeElement as HTMLElement).focus();
        } else {
          (suggestionLi[index + 1].nativeElement as HTMLElement).focus();
        }
        event.preventDefault();
        break;
      case 38: // Down arrow
        if (index === 0) {
          (this.suggestionList.last.nativeElement as HTMLElement).focus();
        } else {
          (suggestionLi[index - 1].nativeElement as HTMLElement).focus();
        }
        event.preventDefault();
        break;
      case 27: // Escape
        this.closeSuggestions();
        break;
      case 13: // Enter
        this.setInput(value);
        break;
      default:
        return;
    }
  }

  // tslint:disable-next-line:no-any
  public setInput(v: any) {
    this.value = v;
    this.propagateChange(this.value);
    this.closeSuggestions();
  }

  /**
   * Custom Input functions
   */
  public inputBlur() {
    this.onTouched();
  }

  // tslint:disable-next-line:no-any
  public writeValue(value: any) {
    this._renderer.setProperty(this.input.nativeElement, 'value', value);
  }

  // tslint:disable-next-line:no-any
  public registerOnChange(fn: (_: any) => {}) {
    this.propagateChange = fn;
  }

  // tslint:disable-next-line:no-any no-empty
  public onTouched: () => any = () => {};
  // tslint:disable-next-line:no-any
  public registerOnTouched(fn: () => any) {
    this.onTouched = fn;
  }

  public inputChange(event: Event) {
    const { value } = <HTMLInputElement>event.target;
    this.propagateChange(value);
  }

  public focus() {
    (this.input.nativeElement as HTMLElement).focus();
  }

  // tslint:disable:no-any
  // tslint:disable:no-empty
  private propagateChange = (_: any) => {};

  private closeSuggestions() {
    (this.input.nativeElement as HTMLElement).focus();
    this.showSuggestions = false;
  }
}
