/**
 * Jasmine Spec BMSelectComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMATForms } from 'lib/forms';
import { BMInputSearchComponent } from 'lib/forms/bm-search-input';

describe('BMInputSearchComponent', () => {
  let component: BMInputSearchComponent;
  let fixture: ComponentFixture<BMInputSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BMATForms.forRoot()],
      declarations: [],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMInputSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });
});
