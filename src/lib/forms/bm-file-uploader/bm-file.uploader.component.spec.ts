/**
 * Jasmine Spec BMFileUploaderComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMATForms } from 'lib/forms';
import { BMFileUploaderComponent } from 'lib/forms/bm-file-uploader';
import { FileStatus } from 'lib/forms/file-uploader-base';
import { restoreEvent } from 'testing/restore-event';

// tslint:disable-next-line:no-any
declare var DragEvent: any; // Wrong typescript types for this event constructor

describe('BMFileUploaderComponent', () => {
  let component: BMFileUploaderComponent;
  let fixture: ComponentFixture<BMFileUploaderComponent>;
  // tslint:disable-next-line:no-any
  let fileReaderInst: any = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BMATForms.forRoot(),
      ],
      declarations: [
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMFileUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    restoreEvent();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  describe('Hostbindings', () => {
    it('should call ondragover after a dragover event', () => {
      spyOn(component, 'ondragover');
      fixture.debugElement.triggerEventHandler('dragover', new DragEvent('dragover'));
      expect(component.ondragover).toHaveBeenCalled();
    });

    it('should call ondragleave after a dragleave event', () => {
      spyOn(component, 'ondragleave');
      fixture.debugElement.triggerEventHandler('dragleave', new DragEvent('dragleave'));
      expect(component.ondragleave).toHaveBeenCalled();
    });

    it('should call ondrop after a drop event', () => {
      spyOn(component, 'ondrop');
      fixture.debugElement.triggerEventHandler('drop', new DragEvent('drop'));
      expect(component.ondrop).toHaveBeenCalled();
    });

    it('ondragover should add bm-file-upload--drag-hover css class to element', () => {
      component.ondragover(new DragEvent('dragover'));
      fixture.detectChanges();
      expect(fixture.debugElement.classes['bm-file-upload--drag-hover']).toBeTruthy();
    });

    it('ondragleave should remove add bm-file-upload--drag-hover css class to element', () => {
      component.ondragleave();
      fixture.detectChanges();
      expect(fixture.debugElement.classes['bm-file-upload--drag-hover']).toBeFalsy();
    });
  });

  describe('File ingestion', () => {
    // tslint:disable-next-line:no-any
    const sampleDropFileEvent: any = {
        preventDefault() { return; },
        dataTransfer: {
          files: [{
            name: 'name',
            size: 10,
            type: 'text',
          }],
        },
      };

    beforeEach(() => {
      fileReaderInst = {
        readAsDataURL() { return; },
        readAsArrayBuffer() { return; },
        abort() { return; },
        result: null,
      };
      // tslint:disable-next-line:no-any
      spyOn(<any> global, 'FileReader').and.returnValue(fileReaderInst);
    });

    it('ondrop should start the FileReader digest', () => {
      component.ondrop(sampleDropFileEvent);
      // tslint:disable-next-line:no-any
      expect((<any> global).FileReader).toHaveBeenCalled();
    });

    it('onInputFileChange should start the FileReader digest if contains files', () => {
      // tslint:disable-next-line:no-any
      component.onInputFileChange(<any> {
        target: {
          files: [{
            name: 'name',
            size: 10,
            type: 'text',
          }],
        },
        preventDefault() { return; },
      });
      // tslint:disable-next-line:no-any
      expect((<any> global).FileReader).toHaveBeenCalled();
    });

    it('onInputFileChange should not start the FileReader digest if does not contains files', () => {
      // tslint:disable-next-line:no-any
      component.onInputFileChange(<any> {
        target: {
        },
        preventDefault() { return; },
      });
      // tslint:disable-next-line:no-any
      expect((<any> global).FileReader).not.toHaveBeenCalled();
    });

    describe('FileReader callbacks', () => {
      const defaultValue = {
            name: 'name',
            size: 10,
            type: 'text',
            status: FileStatus.EMPTY,
            data: null,
            lastModified: undefined,
            total: 10,
            loaded: 0,
          };
      beforeEach(() => {
        component.ondrop(sampleDropFileEvent);
      });

      it('onerror callback should set status to error and emit an error event', () => {
        spyOn(component.error, 'emit');
        fileReaderInst.onerror();
        expect(component.error.emit).toHaveBeenCalledWith({
          source: component,
          value: {
            ...defaultValue,
            data: null,
            status: FileStatus.ERROR,
          },
          reader: fileReaderInst,
        });
      });

      it('onload callback should set status to done and emit a load event', () => {
        spyOn(component.load, 'emit');
        fileReaderInst.onload();
        expect(component.load.emit).toHaveBeenCalledWith({
          source: component,
          value: {
            ...defaultValue,
            status: FileStatus.DONE,
            loaded: defaultValue.size,
          },
          reader: fileReaderInst,
        });
      });

      it('onprogress callback should set status to loading and update and emit a progress event', () => {
        spyOn(component.progress, 'emit');
        fileReaderInst.onprogress({});
        expect(component.progress.emit).toHaveBeenCalledWith({
          source: component,
          value: {
            ...defaultValue,
            status: FileStatus.LOADING,
          },
          reader: fileReaderInst,
        });
      });

      it('onprogress callback should update size and loaded if progress event is lengthComputable', () => {
        spyOn(component.progress, 'emit');
        fileReaderInst.onprogress({
          lengthComputable: true,
          total: 9999,
          loaded: 999,
        });
        expect(component.progress.emit).toHaveBeenCalledWith({
          source: component,
          value: {
            ...defaultValue,
            status: FileStatus.LOADING,
            total: 9999,
            loaded: 999,
          },
          reader: fileReaderInst,
        });
      });
    });
  });
});
