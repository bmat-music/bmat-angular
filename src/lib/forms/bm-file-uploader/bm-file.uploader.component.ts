/**
 * Angular Component BMFileUploader
 * Drag & Drop File uploader
 */
import {
  ChangeDetectionStrategy,
  Component,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  Renderer2,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { FileUploadBase } from '../file-uploader-base';

@Component({
  selector: 'bm-file-upload',
  templateUrl: 'bm-file.uploader.component.html',
  styleUrls: ['bm-file.uploader.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => BMFileUploaderComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BMFileUploaderComponent extends FileUploadBase {
  public numberFiles = 0;
  @Input()
  set multiple(v) {
    this.numberFiles = v ? 1 : 0;
  }
  get multiple() {
    return this.numberFiles === 1;
  }

  @HostBinding('class.bm-file-upload--drag-hover') public dragHover = false;

  constructor(renderer: Renderer2) {
    super(renderer);
  }

  @HostListener('dragover', ['$event'])
  public ondragover(event: DragEvent) {
    event.preventDefault();
    this.dragHover = true;
  }
  @HostListener('dragleave')
  public ondragleave() {
    this.dragHover = false;
  }
  @HostListener('drop', ['$event'])
  public ondrop(event: DragEvent) {
    event.dataTransfer.dropEffect = 'copy';
    const { dataTransfer } = event;
    this.readFiles(dataTransfer.files);
    event.preventDefault();
  }

  public onInputFileChange(event: Event) {
    const input = <HTMLInputElement>event.target;
    event.preventDefault();
    if (input.files) {
      this.readFiles(input.files);
    }
  }
}
