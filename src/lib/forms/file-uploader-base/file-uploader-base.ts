import { ElementRef, EventEmitter, Input, Output, Renderer2, ViewChild} from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

export type ValidDataTypes = 'ArrayBuffer' | 'DataURL' | 'Text' | 'None';

import { BMFileUploadChangeEvent, FileStatus, FileUploaderFile} from './file-uploader-base.interfaces';

/**
 * Implements the required callback and methods of file uploading
 */
export abstract class FileUploadBase implements ControlValueAccessor {
    @Input() public dataType: ValidDataTypes = 'DataURL';
    @Output() public progress = new EventEmitter<BMFileUploadChangeEvent>();
    @Output() public load = new EventEmitter<BMFileUploadChangeEvent>();
    @Output() public loadstart = new EventEmitter<BMFileUploadChangeEvent>();
    @Output() public error = new EventEmitter<BMFileUploadChangeEvent>();

    @ViewChild('inputFile', { read: ElementRef }) private _inputElement!: ElementRef;

    constructor(protected _renderer: Renderer2) {
    }

    /**
     * Abort a FileReader action.
     * Does nothing if there is no currently active reader action.
     */
    public abort() {
      return;
    }

    // ControlValueAccessor
    // tslint:disable-next-line:no-any
    public writeValue(value: any) {
      if (value instanceof FileList) {
        this.readFiles(value);
      }
    }

    // tslint:disable-next-line:no-any
    public registerOnChange(fn: any) {
      this._onChange = fn;
    }

    // tslint:disable-next-line:no-any
    public registerOnTouched(fn: any) {
      this._onTouched = fn;
    }

    public setDisabledState(isDisabled: boolean) {
      if (this._inputElement) {
        this._renderer.setProperty(this._inputElement.nativeElement, 'disabled', isDisabled);
      }
    }

    // tslint:disable-next-line:no-any
    protected _onTouched = (_: any) => { return; };
    // tslint:disable-next-line:no-any
    protected _onChange = (_: any) => { return; };

    protected readFiles(fileList: FileList) {
      if (this.dataType !== 'None') {
        const files = Array.from(fileList);
        files.forEach(file => {
          const fileData: FileUploaderFile = {
            name: file.name,
            size: file.size,
            type: file.type,
            status: FileStatus.EMPTY,
            data: null,
            // tslint:disable-next-line:no-any
            lastModified: (<any> file).lastModified,
            total: file.size,
            loaded: 0,
          };

        let reader = new FileReader();
        switch (this.dataType) {
          case 'ArrayBuffer':
            reader.readAsArrayBuffer(file);
            break;
          case 'DataURL':
            reader.readAsDataURL(file);
            break;
          case 'Text':
            reader.readAsText(file);
            break;
          default:
            throw new TypeError('Invalid output type: Select ArrayBuffer, DataURL or Text');
        }

        this.abort = reader.abort;

        reader.onerror = () => {
          reader.abort();
          this.abort = () => { return; };
          const value = Object.assign({}, fileData, {
            status: FileStatus.ERROR,
          });
          this.error.emit({
            source: this,
            value,
            reader,
          });
        };

        reader.onloadstart = () => {
          const value = Object.assign({}, fileData, {
            status: FileStatus.DONE,
            data: reader.result,
            loaded: fileData.size,
          });
          this.loadstart.emit({
            source: this,
            value,
            reader,
          });
        };

        reader.onload = () => {
          const value = Object.assign({}, fileData, {
            status: FileStatus.DONE,
            data: reader.result,
            loaded: fileData.size,
          });
          this.load.emit({
            source: this,
            value,
            reader,
          });
          this.abort = () => { return; };
          // tslint:disable-next-line:no-any
          reader = null as any;
        };

        reader.onprogress = (e: ProgressEvent) => {
          const progressSegment = (e.lengthComputable ? {
            total: e.total,
            loaded: e.loaded,
          } : {});

          const value = Object.assign({}, fileData, progressSegment, {
            status: FileStatus.LOADING,
          });
          this.progress.emit({
            source: this,
            value,
            reader,
          });
        };
      });
    }
  }
}
