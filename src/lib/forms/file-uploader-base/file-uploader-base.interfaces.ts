/**
 * Typescript Interfaces
 */
export enum FileStatus {
  EMPTY = 0,
  LOADING = 1,
  DONE = 2,
  ERROR = 3,
}

export type PartialFile = Partial<File>;

export type FileUploaderFile = PartialFile & {
  readonly lastModified?: number;
  status: FileStatus;
  data: null | ArrayBuffer | string;
  loaded: number;
  total: number;
};

export interface BMFileUploadChangeEvent {
  // tslint:disable-next-line:no-any
  source: any;
  value: FileUploaderFile;
  reader: FileReader;
}
