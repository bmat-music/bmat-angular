/**
 * BMAT Layout Module
 */
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { BMCheckboxComponent } from './bm-checkbox';
import { BMClickFileUploaderComponent } from './bm-click-file-upload';
import { BMDurationInputComponent } from './bm-duration-input';
import { BMFileUploaderComponent } from './bm-file-uploader';
import { BMMultiselectComponent } from './bm-multiselect';
import { BMRadioComponent } from './bm-radio';
import { BMInputSearchComponent } from './bm-search-input';
import { BMOptionDirective, BMSelectComponent } from './bm-select';

@NgModule({
  imports: [CommonModule],
  declarations: [
    BMCheckboxComponent,
    BMFileUploaderComponent,
    BMInputSearchComponent,
    BMRadioComponent,
    BMSelectComponent,
    BMDurationInputComponent,
    BMMultiselectComponent,
    BMOptionDirective,
    BMClickFileUploaderComponent,
  ],
  exports: [
    BMCheckboxComponent,
    BMFileUploaderComponent,
    BMInputSearchComponent,
    BMRadioComponent,
    BMSelectComponent,
    BMDurationInputComponent,
    BMMultiselectComponent,
    BMOptionDirective,
    BMClickFileUploaderComponent,
  ],
  providers: [],
})
export class BMATForms {
  // tslint:disable-next-line:function-name
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: BMATForms,
      providers: [],
    };
  }
}
