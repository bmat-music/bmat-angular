/**
 * Angular Component. Checkbox with BMAT Style
 */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  HostBinding,
  Input,
  Renderer,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export interface BMCheckboxChangeEvent {
  source: BMCheckboxComponent;
  checked: boolean;
}

@Component({
  selector: 'bm-checkbox',
  templateUrl: 'bm-checkbox.component.html',
  styleUrls: ['bm-checkbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => BMCheckboxComponent),
      multi: true,
    },
  ],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 1 })),
      transition('void => *', [
        style({
          opacity: 0,
        }),
        animate('0.2s ease-in'),
      ]),
      transition('* => void', [
        animate(
          '0.2s ease-out',
          style({
            opacity: 0,
          }),
        ),
      ]),
    ]),
  ],
})
export class BMCheckboxComponent implements ControlValueAccessor {
  @ViewChild('input') public input!: ElementRef;
  @HostBinding('class.bm-checkbox--focused')
  get isFocused() {
    return this._focused;
  }

  /**
   * Input values to directly copy to inner checkbox
   */
  @Input() public name = '';
  @Input() public tabIndex = 0;
  @Input() public required!: boolean;
  @Input() public disabled!: boolean;
  @Input() public readonly!: boolean;
  // tslint:disable-next-line:no-input-rename
  @Input('aria-label') public ariaLabel!: string;
  // tslint:disable-next-line:no-input-rename
  @Input('aria-labelledby') public ariaLabelledby!: string;

  @Input()
  get checked() {
    return this.input.nativeElement.checked;
  }
  set checked(checked: boolean) {
    this._renderer.setElementProperty(
      this.input.nativeElement,
      'checked',
      checked,
    );
  }

  private _focused!: boolean;

  constructor(
    private _renderer: Renderer,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {}

  // Focus Blur Handle
  public inputFocus() {
    this._focused = true;
  }
  public inputBlur() {
    this._focused = false;
    this.onTouched();
  }

  // tslint:disable-next-line:no-any
  public writeValue(value: any) {
    this.checked = value;
    this._changeDetectorRef.markForCheck();
  }

  // tslint:disable-next-line:no-any
  public registerOnChange(fn: (_: any) => {}) {
    this.propagateChange = fn;
  }

  // tslint:disable-next-line:no-any no-empty
  public onTouched: () => any = () => {};
  // tslint:disable-next-line:no-any
  public registerOnTouched(fn: () => any) {
    this.onTouched = fn;
  }

  public inputChange(event: Event) {
    const { checked } = <HTMLInputElement>event.target;
    this.propagateChange(checked);
  }

  public focus() {
    this._renderer.invokeElementMethod(this.input.nativeElement, 'focus');
  }

  // tslint:disable:no-any
  // tslint:disable:no-empty
  private propagateChange = (_: any) => {};
}
