/**
 * Jasmine Spec BMCheckboxComponent
 */
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { BMCheckboxComponent } from 'lib/forms/bm-checkbox';
import { restoreEvent } from 'testing/restore-event';

@Component({
  template: `<bm-checkbox [formControl]="formControl"></bm-checkbox>`,
})
class TestContainerComponent {
  public formControl = new FormControl();
}

describe('BMCheckboxComponent', () => {
  let component: BMCheckboxComponent;
  let fixture: ComponentFixture<BMCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        NoopAnimationsModule,
      ],
      declarations: [
        TestContainerComponent,
        BMCheckboxComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    restoreEvent();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  describe('reflection properties', () => {
    let input: DebugElement;

    beforeEach(() => {
      input = fixture.debugElement.query(By.css('input[type=checkbox]'));
    });

    it('checked property should reflect the inner input checked property', () => {
      component.checked = true;
      fixture.detectChanges();
      expect(input.nativeElement.checked).toBeTruthy();
      component.checked = false;
      fixture.detectChanges();
      expect(input.nativeElement.checked).toBeFalsy();
    });

    it('focus at inner input should add the bm-checkbox--focused class', () => {
      input.triggerEventHandler('focus', new Event('focus'));
      fixture.detectChanges();
      expect(fixture.debugElement.classes['bm-checkbox--focused']).toBeTruthy();
    });

    it('blur at inner input should remove the bm-checkbox--focused class', () => {
      input.triggerEventHandler('blur', new Event('blur'));
      fixture.detectChanges();
      expect(fixture.debugElement.classes['bm-checkbox--focused']).toBeFalsy();
    });

    it('blur at inner input should notify to value accessor that input has been touched', () => {
      spyOn(component, 'onTouched');
      input.triggerEventHandler('blur', new Event('blur'));
      fixture.detectChanges();
      expect(component.onTouched).toHaveBeenCalled();
    });

    it('component focus method should call inner input focus method', () => {
      spyOn(input.nativeElement, 'focus');
      component.focus();
      fixture.detectChanges();
      expect(input.nativeElement.focus).toHaveBeenCalled();
    });
  });

  describe('Value accessor', () => {
    let input: DebugElement;
    let testContainerComponent: TestContainerComponent;
    let testContainerFixture: ComponentFixture<TestContainerComponent>;
    let fakeFun: { changeCb: (_: {}) => {}};

    beforeEach(() => {
      testContainerFixture = TestBed.createComponent(TestContainerComponent);
      testContainerComponent = testContainerFixture.componentInstance;

      component = testContainerFixture.debugElement.query(By.css('bm-checkbox')).componentInstance;
      fakeFun = {
        // tslint:disable-next-line:no-any
        changeCb(_: any) { return {}; },
      };
      spyOn(fakeFun, 'changeCb');
      component.registerOnChange(fakeFun.changeCb);
    });

    it('should register the onChange function', () => {
      input = testContainerFixture.debugElement
        .query(By.css('bm-checkbox'))
        .query(By.css('input[type=checkbox]'));
      input.triggerEventHandler('change', {
        target: input.nativeElement,
      });
      expect(fakeFun.changeCb).toHaveBeenCalled();
    });

    it('patching control should update the inner input checked value', () => {
      input = testContainerFixture.debugElement
        .query(By.css('bm-checkbox'))
        .query(By.css('input[type=checkbox]'))
        .nativeElement;
      testContainerComponent.formControl.patchValue(true);
      testContainerFixture.detectChanges();
      expect(component.checked).toBeTruthy();
    });
  });
});
