import { DebugElement } from '@angular/core';
/**
 * Jasmine Spec BMSelectComponent
 */
import { Component, ViewChild } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { BMOptionDirective, BMSelectComponent } from 'lib/forms/bm-select';
import { restoreEvent } from 'testing/restore-event';

@Component({
  template: `
    <form [formGroup]="formGroup">
      <bm-select formControlName='control'>
        <option bmOption value="null">Null</option>
        <option bmOption value="a">A</option>
        <option bmOption value="b">B</option>
        <option bmOption value="c">C</option>
        <option bmOption value="false">False</option>
      </bm-select>
  `,
})
class TestComponent {
  @ViewChild(BMSelectComponent) public bmSelect!: BMSelectComponent;
  public formGroup = new FormGroup({
    control: new FormControl('b'),
  });
}

describe('BMSelectComponent', () => {
  let hostComponent: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let component: BMSelectComponent;
  let select: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [TestComponent, BMSelectComponent, BMOptionDirective],
    }).compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(TestComponent);
    hostComponent = fixture.componentInstance;
    component = hostComponent.bmSelect;
    fixture.detectChanges();
    select = fixture.debugElement.query(By.css('select'));
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  describe('option bindings', () => {
    it('should set the correct option as checked', () => {
      const [selected] = fixture.debugElement
        .queryAll(By.directive(BMOptionDirective))
        .map(option => option.nativeElement as HTMLOptionElement)
        .filter(option => option.selected);
      expect(selected.value).toEqual('b');
    });

    it('should change to correct option after an update', () => {
      hostComponent.formGroup.patchValue({
        control: 'a',
      });

      fixture.detectChanges();
      const [selected] = fixture.debugElement
        .queryAll(By.directive(BMOptionDirective))
        .map(option => option.nativeElement as HTMLOptionElement)
        .filter(option => option.selected);
      expect(selected.value).toEqual('a');
    });

    it('blur on form should call the onTouched callback', () => {
      spyOn(component, 'onTouched');
      restoreEvent();
      select.triggerEventHandler('blur', new Event('blur'));
      fixture.detectChanges();
      expect(component.onTouched).toHaveBeenCalled();
    });

    it('should reflect the changes in value', () => {
      component.value = 'false';
      fixture.detectChanges();
      const [selected] = fixture.debugElement
        .queryAll(By.directive(BMOptionDirective))
        .map(option => option.nativeElement as HTMLOptionElement)
        .filter(option => option.selected);
      expect(selected.value).toEqual('false');
    });

    it('should reflect the focus method', () => {
      spyOn(select.nativeElement, 'focus');
      component.focus();
      expect(select.nativeElement.focus).toHaveBeenCalled();
    });

    it('should propagate changes in the input value', () => {
      restoreEvent();
      select.triggerEventHandler('change', {
        target: {
          value: 'c',
        },
      });
      fixture.detectChanges();
      expect(hostComponent.formGroup.value).toEqual({ control: 'c' });
    });
  });
});
