# BMSelectComponent
A select input element styled according to the BMAT standards. Compatible 
with *@angular/forms* module

## Usage
The directive selector is *bm-select*. To enable the reflection of 
values and selected options, options must include the directive
*[bmOption]*.