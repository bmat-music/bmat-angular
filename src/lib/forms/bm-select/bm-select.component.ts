/**
 * Angular component. BMSelectComponent
 * Complement to style Selects
 */
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  Directive,
  ElementRef,
  forwardRef,
  Input,
  OnDestroy,
  QueryList,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Subscription } from 'rxjs';

@Directive({
  selector: '[bmOption]',
})
export class BMOptionDirective {}

@Component({
  selector: 'bm-select',
  templateUrl: 'bm-select.component.html',
  styleUrls: ['bm-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => BMSelectComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BMSelectComponent
  implements ControlValueAccessor, AfterContentInit, OnDestroy {
  @ViewChild('select') public select!: ElementRef;
  @ContentChildren(BMOptionDirective, { read: ElementRef })
  public options!: QueryList<ElementRef>;

  /**
   * Input values to directly copy to inner checkbox
   */
  @Input() public name = '';
  @Input() public tabIndex = 0;
  @Input() public required!: boolean;
  @Input() public disabled!: boolean;
  @Input() public readonly!: boolean;
  // tslint:disable-next-line:no-reserved-keywords
  @Input() public type = 'text';
  // tslint:disable-next-line:no-input-rename
  @Input('aria-label') public ariaLabel!: string;
  // tslint:disable-next-line:no-input-rename
  @Input('aria-labelledby') public ariaLabelledby!: string;

  // tslint:disable-next-line:no-any
  private _valueCopy: any;
  @Input()
  // tslint:disable-next-line:no-any
  set value(v: any) {
    this._valueCopy = v;
    this.setCheckedOption();
  }
  get value() {
    return this._valueCopy;
  }

  private _optionsSubs!: Subscription;

  constructor(private _renderer: Renderer2) {}

  /**
   * Custom Input functions
   */
  public inputBlur() {
    this.onTouched();
  }

  // tslint:disable-next-line:no-any
  public writeValue(value: any) {
    this._valueCopy = value;
    this.setCheckedOption();
  }

  // tslint:disable-next-line:no-any
  public registerOnChange(fn: (_: any) => {}) {
    this.propagateChange = fn;
  }

  // tslint:disable-next-line:no-any no-empty
  public onTouched: () => any = () => {};
  // tslint:disable-next-line:no-any
  public registerOnTouched(fn: () => any) {
    this.onTouched = fn;
  }

  public inputChange(event: Event) {
    const { value } = <HTMLInputElement>event.target;
    this._valueCopy = value;
    this.propagateChange(value);
  }

  public focus() {
    (this.select.nativeElement as HTMLSelectElement).focus();
  }

  public ngAfterContentInit() {
    this._optionsSubs = this.options.changes.subscribe(() =>
      this.setCheckedOption(),
    );
    this.setCheckedOption();
  }

  public ngOnDestroy() {
    this._optionsSubs.unsubscribe();
  }

  // tslint:disable:no-any
  // tslint:disable:no-empty
  private propagateChange = (_: any) => {};

  private setCheckedOption() {
    if (this._valueCopy !== undefined) {
      this._renderer.setValue(this.select.nativeElement, this._valueCopy);
    }

    if (!this.options) {
      return;
    }

    this.options.forEach(elem => {
      const domEle: HTMLOptionElement = elem.nativeElement;
      if (domEle) {
        domEle.selected = domEle.value === this._valueCopy;
      }
    });
  }
}
