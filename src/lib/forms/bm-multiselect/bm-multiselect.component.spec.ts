/**
 * Jasmine Spec BMMultiselectComponent
 */
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { BMMultiselectComponent } from 'lib/forms/bm-multiselect';
import { restoreEvent } from 'testing/restore-event';

describe('BMMultiselectComponent', () => {
  let component: BMMultiselectComponent;
  let fixture: ComponentFixture<BMMultiselectComponent>;
  let input: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [BMMultiselectComponent],
    }).compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(BMMultiselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    await fixture.whenStable();
    input = fixture.debugElement.query(By.css('input'));
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('focus method should call the inner input focus', () => {
    spyOn(component.input.nativeElement, 'focus');
    component.focus();
    expect(component.input.nativeElement.focus).toHaveBeenCalled();
  });

  it('inner input focusout event should set the element to touched', () => {
    spyOn(component, 'onTouched');

    restoreEvent();
    input.triggerEventHandler('focusout', new Event('focusout'));
    fixture.detectChanges();
    expect(component.onTouched).toHaveBeenCalled();
  });
});
