/**
 * Angular Component BMMultiselectComponent
 * Simple multiselect element that lists the selected after it.
 */
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  HostBinding,
  Input,
  QueryList,
  Renderer,
  ViewChild,
  ViewChildren,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export interface BMCheckboxChangeEvent {
  source: BMMultiselectComponent;
  value: string[];
}

export interface BMMultiselectOption {
  displayValue: string;
  key: string;
}

@Component({
  selector: 'bm-multiselect',
  templateUrl: 'bm-multiselect.component.html',
  styleUrls: ['bm-multiselect.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => BMMultiselectComponent),
      multi: true,
    },
  ],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 1 })),
      transition('void => *', [
        style({
          opacity: 0,
        }),
        animate('0.2s ease-in'),
      ]),
      transition('* => void', [
        animate(
          '0.2s ease-out',
          style({
            opacity: 0,
          }),
        ),
      ]),
    ]),
  ],
})
export class BMMultiselectComponent
  implements ControlValueAccessor, AfterViewInit {
  @ViewChild('input') public input!: ElementRef;
  @ViewChildren('sugg') public suggestionList!: QueryList<ElementRef>;

  @HostBinding('class.bm-multiselect-focused')
  get isFocused() {
    return this.focused;
  }

  /**
   * Input values to directly copy to inner input
   */
  @Input() public name = '';
  @Input() public tabIndex = 0;
  @Input() public required!: boolean;
  @Input() public disabled!: boolean;
  @Input() public readonly!: boolean;
  // tslint:disable-next-line:no-input-rename
  @Input('aria-label') public ariaLabel!: string;
  // tslint:disable-next-line:no-input-rename
  @Input('aria-labelledby') public ariaLabelledby!: string;

  @Input()
  set value(v: string[]) {
    if (Array.isArray(v)) {
      this.selectedOptions = new Set(v);
    }
    this.updateListAndSuggestions();
    this.updateSelectedList();
  }
  get value() {
    return Array.from(this.selectedOptions.values());
  }

  @Input() public options: BMMultiselectOption[] = [];

  public availableOptions: BMMultiselectOption[] = [];
  public filteredOptions: BMMultiselectOption[] = [];
  public selectedOptionsList: BMMultiselectOption[] = [];
  public selectedOptions: Set<string> = new Set();
  public showSuggestions = false;

  private focused!: boolean;

  constructor(
    private renderer: Renderer,
    private changeDetectorRef: ChangeDetectorRef,
  ) {}

  public selectOption(option: BMMultiselectOption, event: Event) {
    const target = event.target as HTMLElement;
    if (target.nextElementSibling) {
      (target.nextElementSibling as HTMLElement).focus();
    } else if (target.previousElementSibling) {
      (target.previousElementSibling as HTMLElement).focus();
    }
    this.selectedOptions.add(option.key);
    this.valueChange();
  }
  public removeOption(option: BMMultiselectOption) {
    this.selectedOptions.delete(option.key);
    this.valueChange();
  }
  public removeAll() {
    this.selectedOptions.clear();
    this.valueChange();
  }
  public addAll() {
    this.selectedOptions = new Set(this.options.map(item => item.key));
    this.valueChange();
    this.showSuggestions = false;
  }

  public inputKeyUp(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 40: // Down arrow
        this.showSuggestions = true;
        if (this.suggestionList.first) {
          this.renderer.invokeElementMethod(
            this.suggestionList.first.nativeElement,
            'focus',
          );
        }
        break;
      case 27: // Escape
        this.showSuggestions = false;
        break;
      default:
        this.updateSuggestionList();
    }
  }

  public suggestionKeyUp(
    event: KeyboardEvent,
    index: number,
    value: BMMultiselectOption,
  ) {
    const suggestionLi = this.suggestionList.toArray();
    const keyDown = () => {
      if (index === 0) {
        this.renderer.invokeElementMethod(
          this.suggestionList.last.nativeElement,
          'focus',
        );
      } else {
        this.renderer.invokeElementMethod(
          suggestionLi[index - 1].nativeElement,
          'focus',
        );
      }
    };
    switch (event.keyCode) {
      case 40: // Up arrow
        if (index === this.filteredOptions.length - 1) {
          this.renderer.invokeElementMethod(
            this.suggestionList.first.nativeElement,
            'focus',
          );
        } else {
          this.renderer.invokeElementMethod(
            suggestionLi[index + 1].nativeElement,
            'focus',
          );
        }
        event.preventDefault();
        break;
      case 38: // Down arrow
        keyDown();
        event.preventDefault();
        break;
      case 27: // Escape
        this.showSuggestions = false;
        break;
      case 13: // Enter
        this.selectOption(value, event);
        keyDown();
        break;
      default:
        return;
    }
  }

  public selectedItemKeyup(event: KeyboardEvent, value: BMMultiselectOption) {
    if (event.keyCode === 46 || event.keyCode === 8) {
      event.preventDefault();
      this.removeOption(value);
    }
  }

  public afterOptionFocus() {
    this.showSuggestions = false;
  }

  // Focus Blur Handle
  public inputFocus() {
    this.focused = true;
    this.showSuggestions = true;
  }
  public inputFocusout(event: FocusEvent) {
    const relatedTarget = event.relatedTarget as HTMLElement;
    const suggestions = this.suggestionList.map(item => item.nativeElement);
    if (suggestions.indexOf(relatedTarget) === -1) {
      this.showSuggestions = false;
    }
    this.focused = false;
    this.onTouched();
  }
  public suggestionFocusout(event: FocusEvent) {
    const target = event.target as HTMLElement;
    const relatedTarget = event.relatedTarget as HTMLElement;

    if (
      !relatedTarget ||
      target.parentElement !== relatedTarget.parentElement
    ) {
      this.showSuggestions = false;
    }
  }

  public ngAfterViewInit() {
    this.updateListAndSuggestions();
  }

  // tslint:disable-next-line:no-any
  public writeValue(value: string[]) {
    this.value = value;
    this.changeDetectorRef.markForCheck();
  }

  // tslint:disable-next-line:no-any
  public registerOnChange(fn: (_: any) => {}) {
    this.propagateChange = fn;
  }

  // tslint:disable-next-line:no-any no-empty
  public onTouched: () => any = () => {};
  // tslint:disable-next-line:no-any
  public registerOnTouched(fn: () => any) {
    this.onTouched = fn;
  }

  public focus() {
    this.renderer.invokeElementMethod(this.input.nativeElement, 'focus');
  }

  // tslint:disable:no-any
  // tslint:disable:no-empty
  private propagateChange = (_: any) => {};

  private updateSelectedList() {
    this.selectedOptionsList = this.options.filter(option =>
      this.selectedOptions.has(option.key),
    );
  }

  private updateListAndSuggestions() {
    this.availableOptions = this.options.filter(
      option => !this.selectedOptions.has(option.key),
    );
    this.updateSuggestionList();
  }

  private updateSuggestionList() {
    const value = (<HTMLInputElement>this.input
      .nativeElement).value.toLowerCase();
    this.filteredOptions = this.availableOptions.filter(
      item => item.displayValue.toLowerCase().search(value) !== -1,
    );
  }

  private valueChange() {
    this.propagateChange(Array.from(this.selectedOptions.values()));
    this.updateListAndSuggestions();
    this.updateSelectedList();
    if (this.availableOptions.length === 0) {
      this.renderer.invokeElementMethod(this.input.nativeElement, 'focus');
    }
  }
}
