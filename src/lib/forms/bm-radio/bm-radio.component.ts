/**
 * Angular Component. Checkbox with BMAT Style
 */
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  Component,
  ElementRef,
  forwardRef,
  HostBinding,
  Input,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export interface BMRadioComponentChangeEvent {
  source: BMRadioComponent;
  checked: boolean;
}

@Component({
  selector: 'bm-radio',
  templateUrl: 'bm-radio.component.html',
  styleUrls: ['bm-radio.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => BMRadioComponent),
      multi: true,
    },
  ],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 1 })),
      transition('void => *', [
        style({
          opacity: 0,
        }),
        animate('0.2s ease-in'),
      ]),
      transition('* => void', [
        animate(
          '0.2s ease-out',
          style({
            opacity: 0,
          }),
        ),
      ]),
    ]),
  ],
})
export class BMRadioComponent implements ControlValueAccessor {
  @ViewChild('input') public input!: ElementRef;
  @HostBinding('class.bm-radio--focused')
  get isFocused() {
    return this._focused;
  }

  /**
   * Input values to directly copy to inner radio
   */
  @Input() public name = '';
  @Input() public value = '';
  @Input() public tabIndex = 0;
  @Input() public required!: boolean;
  @Input() public disabled!: boolean;
  @Input() public readonly!: boolean;
  // tslint:disable-next-line:no-input-rename
  @Input('aria-label') public ariaLabel!: string;
  // tslint:disable-next-line:no-input-rename
  @Input('aria-labelledby') public ariaLabelledby!: string;

  @Input()
  get checked() {
    return this.input.nativeElement.checked;
  }
  set checked(checked: boolean) {
    if (checked !== this.checked) {
      this._renderer.setProperty(this.input.nativeElement, 'checked', checked);
    }
  }

  // Focus Blur Handle
  private _focused!: boolean;

  constructor(private _renderer: Renderer2) {}

  public inputFocus() {
    this._focused = true;
  }
  public inputBlur() {
    this._focused = false;
    this.onTouched();
  }

  // tslint:disable-next-line:no-any
  public writeValue(value: any) {
    this.checked = value === this.value;
  }

  // tslint:disable-next-line:no-any
  public registerOnChange(fn: (_: any) => {}) {
    this.propagateChange = fn;
  }

  // tslint:disable-next-line:no-any no-empty
  public onTouched: () => any = () => {};
  // tslint:disable-next-line:no-any
  public registerOnTouched(fn: () => any) {
    this.onTouched = fn;
  }

  public inputChange() {
    this.propagateChange(this.value);
  }

  public focus() {
    (this.input.nativeElement as HTMLElement).focus();
  }

  // tslint:disable:no-any
  // tslint:disable:no-empty
  private propagateChange = (_: any) => {};
}
