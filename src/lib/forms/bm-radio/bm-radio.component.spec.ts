/**
 * Jasmine Spec BMRadioComponent
 */
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { BMRadioComponent } from 'lib/forms/bm-radio';
import { restoreEvent } from 'testing/restore-event';

@Component({
  template: `
    <bm-radio value="value1" [formControl]="formControl"></bm-radio>
    <bm-radio value="value2" [formControl]="formControl"></bm-radio>`,
})
class TestContainerComponent {
  public formControl = new FormControl();
}

describe('BMRadioComponent', () => {
  let component: BMRadioComponent;
  let fixture: ComponentFixture<BMRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NoopAnimationsModule, ReactiveFormsModule],
      declarations: [TestContainerComponent, BMRadioComponent],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    restoreEvent();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  describe('reflection properties', () => {
    let input: DebugElement;

    beforeEach(() => {
      input = fixture.debugElement.query(By.css('input[type=radio]'));
    });

    it('checked property should reflect the inner input checked property', () => {
      component.checked = true;
      fixture.detectChanges();
      expect(input.nativeElement.checked).toBeTruthy();
      component.checked = false;
      fixture.detectChanges();
      expect(input.nativeElement.checked).toBeFalsy();
    });

    it('focus at inner input should add the bm-radio--focused class', () => {
      input.triggerEventHandler('focus', new Event('focus'));
      fixture.detectChanges();
      expect(fixture.debugElement.classes['bm-radio--focused']).toBeTruthy();
    });

    it('blur at inner input should remove the bm-radio--focused class', () => {
      input.triggerEventHandler('blur', new Event('blur'));
      fixture.detectChanges();
      expect(fixture.debugElement.classes['bm-radio--focused']).toBeFalsy();
    });

    it('blur at inner input should notify to value accessor that input has been touched', () => {
      spyOn(component, 'onTouched');
      input.triggerEventHandler('blur', new Event('blur'));
      fixture.detectChanges();
      expect(component.onTouched).toHaveBeenCalled();
    });

    it('component focus method should call inner input focus method', () => {
      spyOn(input.nativeElement, 'focus');
      component.focus();
      fixture.detectChanges();
      expect(input.nativeElement.focus).toHaveBeenCalled();
    });
  });

  describe('Value accessor', () => {
    let input: DebugElement;
    let testContainerComponent: TestContainerComponent;
    let testContainerFixture: ComponentFixture<TestContainerComponent>;
    // tslint:disable-next-line:no-any
    let fakeFun: { changeCb: (_: any) => {} };

    beforeEach(() => {
      testContainerFixture = TestBed.createComponent(TestContainerComponent);
      testContainerComponent = testContainerFixture.componentInstance;

      component = testContainerFixture.debugElement.query(By.css('bm-radio'))
        .componentInstance;
      fakeFun = {
        // tslint:disable-next-line:no-any
        changeCb(_: any) {
          return {};
        },
      };
      spyOn(fakeFun, 'changeCb');
      component.registerOnChange(fakeFun.changeCb);
    });

    it('should register the onChange function', () => {
      input = testContainerFixture.debugElement
        .query(By.css('bm-radio'))
        .query(By.css('input[type=radio]'));
      input.triggerEventHandler('change', {
        target: input.nativeElement,
      });
      expect(fakeFun.changeCb).toHaveBeenCalled();
    });

    it('patching control should update the inner input checked value', () => {
      input = testContainerFixture.debugElement
        .query(By.css('bm-radio'))
        .query(By.css('input[type=radio]')).nativeElement;
      testContainerComponent.formControl.patchValue('value1');
      testContainerFixture.detectChanges();
      expect(component.checked).toBeTruthy();
    });
  });
});
