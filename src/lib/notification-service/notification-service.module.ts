import { CommonModule, DOCUMENT } from '@angular/common';
import { NgModule } from '@angular/core';

import { NotificationChipComponent } from './components/notification-chip.component';

@NgModule({
  imports: [CommonModule],
  declarations: [NotificationChipComponent],
  entryComponents: [NotificationChipComponent],
  providers: [
    {
      provide: DOCUMENT,
      useValue: document,
    },
  ],
})
/**
 * Notification service
 */
export class NotificationServiceModule {}
