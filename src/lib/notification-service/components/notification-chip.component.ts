import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'bm-notification-chip',
  templateUrl: 'notification-chip.component.html',
  styleUrls: ['notification-chip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: 1 })),
      transition('void => *', [style({ opacity: 0 }), animate(100)]),
      transition('* => void', [animate(100, style({ opacity: 0 }))]),
    ]),
  ],
})
/**
 * Angular Component
 */
export class NotificationChipComponent {
  @Input() public message!: string;
  @Input() public iconClass!: string;
}
