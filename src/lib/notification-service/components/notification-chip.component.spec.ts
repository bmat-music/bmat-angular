/**
 * Jasmine Spec BMProgressComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { NotificationChipComponent } from 'lib/notification-service/components';

describe('NotificationChipComponent', () => {
  let component: NotificationChipComponent;
  let fixture: ComponentFixture<NotificationChipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NoopAnimationsModule],
      declarations: [NotificationChipComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationChipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });
});
