import { DOCUMENT } from '@angular/common';
import {
  ApplicationRef, ComponentFactoryResolver, EmbeddedViewRef, Inject, Injectable, ReflectiveInjector,
} from '@angular/core';

import { NotificationServiceModule } from '../notification-service.module';
import { NotificationChipComponent } from './../components/notification-chip.component';

@Injectable({
  providedIn: NotificationServiceModule,
})
/**
 * Service to display notificacions
 */
export class NotificationService {
  constructor(@Inject(DOCUMENT) private _document: Document, private _componentFactoryResolver: ComponentFactoryResolver,
    private _appRef: ApplicationRef) {
  }

  public showMessage(message: string, iconClass: string = '') {
    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(NotificationChipComponent);
    const injector = ReflectiveInjector.resolveAndCreate([]);

    const notificationChip = componentFactory.create(injector);
    notificationChip.instance.message = message;
    notificationChip.instance.iconClass = iconClass;
    notificationChip.changeDetectorRef.detectChanges();

    this._appRef.attachView(notificationChip.hostView);

    const domElem = (notificationChip.hostView as EmbeddedViewRef<NotificationChipComponent>)
      .rootNodes[0] as HTMLElement;

    this._document.body.appendChild(domElem);

    setTimeout(() => {
      notificationChip.destroy();
    }, 2000);
  }
}
