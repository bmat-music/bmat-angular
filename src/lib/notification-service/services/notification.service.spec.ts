/**
 * Jasmine Spec BMProgressComponent
 */
import { DOCUMENT } from '@angular/common';
import { async, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import {
  NotificationService,
  NotificationServiceModule,
} from 'lib/notification-service';

describe('NotificationService', () => {
  let service: NotificationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NotificationServiceModule, NoopAnimationsModule],
      declarations: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    // tslint:disable-next-line:mocha-no-side-effect-code
    service = TestBed.get(NotificationService);
  });

  it('Should append a component to body after showMessage', () => {
    const injectedDocument = TestBed.get(DOCUMENT);
    spyOn(injectedDocument.body, 'appendChild');
    service.showMessage('MESSAGE');
    expect(injectedDocument.body.appendChild).toHaveBeenCalled();
  });

  it('Should pass the iconClass to the children element', () => {
    const injectedDocument = TestBed.get(DOCUMENT);
    spyOn(injectedDocument.body, 'appendChild');
    service.showMessage('MESSAGE', 'icon');
    expect(injectedDocument.body.appendChild).toHaveBeenCalled();
  });
});
