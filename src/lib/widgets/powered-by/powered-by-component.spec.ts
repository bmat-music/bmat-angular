/**
 * Jasmine Spec
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { BMATWidgets } from 'lib/widgets';
import { BMPoweredByComponent } from 'lib/widgets/powered-by';

describe('PoweredByComponent', () => {
  let component: BMPoweredByComponent;
  let fixture: ComponentFixture<BMPoweredByComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BMATWidgets],
      declarations: [],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMPoweredByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('should expose currentYear property with the 4 digits representation of the current year', () => {
    const year = new Date().getFullYear();
    expect(component.currentYear).toEqual(year);
  });

  it('should display a copyright notice', () => {
    const year = new Date().getFullYear();
    const notice = fixture.debugElement
      .query(By.css('.bm-pw-all-rights-notice'))
      .nativeElement.textContent.replace(/\s+/g, ' ')
      .replace(/\s+$/, '')
      .replace(/^\s+/, '');
    // tslint:disable-next-line:no-multiline-string
    expect(notice).toEqual(`${year} All rights reserved.`);
  });
});
