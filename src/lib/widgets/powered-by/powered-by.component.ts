/**
 * Angular Component. PoweredByComponent.
 */
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'bm-powered-by',
  templateUrl: 'powered-by.component.html',
  styleUrls: ['powered-by.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BMPoweredByComponent {
  public currentYear: number = new Date().getFullYear();
}
