/**
 * Angular Component BMUserPortraitComponent
 * Dropdown user optuions
 */
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { UserData } from '../../header-user-options';

@Component({
  selector: 'bm-user-profile-detail',
  templateUrl: 'header-user-profile-detail.component.html',
  styleUrls: ['header-user-profile-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BMUserDetailComponent {
  @Input() public userData!: UserData;
  @Output() public viewDetails = new EventEmitter<null>();
}
