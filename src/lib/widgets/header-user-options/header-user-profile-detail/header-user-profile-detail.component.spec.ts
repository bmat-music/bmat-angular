/**
 * Jasmine Spec BMUserDetailComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { BMATWidgets } from 'lib/widgets';
import { BMUserDetailComponent } from 'lib/widgets/header-user-options/header-user-profile-detail';

describe('BMUserDetailComponent', () => {
  let component: BMUserDetailComponent;
  let fixture: ComponentFixture<BMUserDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BMATWidgets.forRoot()],
      declarations: [],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMUserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('should emit a viewDetails event if the view profile button is clicked', () => {
    const btn = fixture.debugElement.query(By.css('.bm-header-view-profile'));
    spyOn(component.viewDetails, 'emit');
    btn.triggerEventHandler('click', {});
    expect(component.viewDetails.emit).toHaveBeenCalled();
  });
});
