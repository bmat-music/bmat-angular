/**
 * Jasmine Spec BMUserProfileLanguageComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup } from '@angular/forms';

import { BMATWidgets } from 'lib/widgets';
import { BMUserProfileLanguageComponent } from 'lib/widgets/header-user-options/user-profile-language-selector';

describe('BMUserProfileLanguageComponent', () => {
  let component: BMUserProfileLanguageComponent;
  let fixture: ComponentFixture<BMUserProfileLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BMATWidgets.forRoot(),
      ],
      declarations: [
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMUserProfileLanguageComponent);
    component = fixture.componentInstance;
    component.form = new FormGroup({
      selectedLanguage: new FormControl(),
    });
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });
});
