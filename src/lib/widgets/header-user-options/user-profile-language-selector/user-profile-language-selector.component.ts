/**
 * Angular Component BMUserProfileLanguageComponent
 */
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { LanguageOption } from '../header-user-options.interfaces';

@Component({
  selector: 'bm-header-profile-languages',
  templateUrl: 'user-profile-language-selector.component.html',
  styleUrls: ['user-profile-language-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BMUserProfileLanguageComponent {
  @Input() public form!: FormGroup;
  @Input() public languageList!: LanguageOption[];
}
