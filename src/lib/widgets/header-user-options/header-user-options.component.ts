/**
 * Angular Component BMHeaderUserOptionsComponent
 * Dropdown user optuions
 */
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { LanguageOption, UserData } from './header-user-options.interfaces';

@Component({
  selector: 'bm-header-user-options',
  templateUrl: 'header-user-options.component.html',
  styleUrls: ['header-user-options.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('flyInOut', [
      state('in', style({ transform: 'translateX(0)' })),
      transition('void => *', [
        style({ transform: 'translateX(100%)' }),
        animate(100),
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'translateX(100%)' })),
      ]),
    ]),
  ],
})
export class BMHeaderUserOptionsComponent implements OnInit {
  @HostBinding('attr.aria-haspopup') public ariaHaspopup = true;
  @HostBinding('attr.aria-pressed') public open = false;
  @ViewChild('toggleContainer') public toggleContainer!: ElementRef;

  @Input()
  public userData: UserData = {
    name: 'Admin',
    email: 'johnsmit@bmat.com',
    selectedLanguage: 'en',
  };

  @Input() public availableLanguages!: LanguageOption[];

  @Output() public viewProfile = new EventEmitter<null>();
  @Output() public changeLanguage = new EventEmitter<string>();
  @Output() public logOut = new EventEmitter<null>();

  public languageSelector = new FormGroup({
    selectedLanguage: new FormControl(),
  });

  constructor(
    private elementRef: ElementRef,
    private changeDetectorRef: ChangeDetectorRef,
  ) {}

  @HostListener('document:click', ['$event'])
  public documentClick($event: Event) {
    let outside = true;
    let target: HTMLElement = <HTMLElement>$event.target;
    const container = this.elementRef.nativeElement;

    if (!this.open) {
      return;
    }

    while (target !== document.body && outside) {
      if (target === container) {
        outside = false;
      }
      target = <HTMLElement>target.parentNode;
    }

    if (outside) {
      this.toggleOpen();
    }
    this.changeDetectorRef.detectChanges();
  }

  public toggleOpen() {
    this.open = !this.open;
  }

  public ngOnInit() {
    if (this.userData) {
      this.languageSelector.patchValue(this.userData, { emitEvent: false });
    }
    this.languageSelector.valueChanges.subscribe(val =>
      this.changeLanguage.emit(val),
    );
  }
}
