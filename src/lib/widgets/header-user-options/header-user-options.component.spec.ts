/**
 * Jasmine Spec BMHeaderUserOptionsComponent
 */
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BMATWidgets } from 'lib/widgets';
import { BMHeaderUserOptionsComponent } from 'lib/widgets/header-user-options';

describe('BMHeaderUserOptionsComponent', () => {
  let component: BMHeaderUserOptionsComponent;
  let fixture: ComponentFixture<BMHeaderUserOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NoopAnimationsModule, BMATWidgets.forRoot()],
      declarations: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMHeaderUserOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('toggleOpen should toggle the property open', () => {
    component.open = true;
    component.toggleOpen();
    expect(component.open).toBeFalsy();
  });

  it('should no patch the form data if the received data is not valid at OnInit Hook', () => {
    spyOn(component.languageSelector, 'patchValue');
    // tslint:disable-next-line:no-any
    component.userData = null as any;
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.languageSelector.patchValue).not.toHaveBeenCalled();
  });

  it('should emit changeLanguage after changes in the language selector form', () => {
    component.ngOnInit();
    spyOn(component.changeLanguage, 'emit');
    component.languageSelector.patchValue({
      selectedLanguage: 'ca',
    });
    fixture.detectChanges();
    expect(component.changeLanguage.emit).toHaveBeenCalledWith({
      selectedLanguage: 'ca',
    });
  });

  describe('click events', () => {
    let clickable: DebugElement;

    beforeEach(() => {
      component.open = false;
      clickable = fixture.debugElement.query(By.css('.bm-header-toggle'));
    });

    it('should open if clicked', () => {
      clickable.triggerEventHandler('click', new MouseEvent('click'));
      fixture.detectChanges();
      expect(component.open).toBeTruthy();
    });

    it('should close the data container if open and clicked outside the element', () => {
      const {
        parentNode,
      }: { parentNode: HTMLElement } = fixture.debugElement.nativeElement;
      clickable.triggerEventHandler('click', new MouseEvent('click'));
      fixture.detectChanges();
      parentNode.click();
      fixture.detectChanges();
      expect(component.open).toBeFalsy();
    });

    it('should do nothing if the data container is closed and clicked outside the element', () => {
      const {
        parentNode,
      }: { parentNode: HTMLElement } = fixture.debugElement.nativeElement;
      parentNode.click();
      fixture.detectChanges();
      expect(component.open).toBeFalsy();
    });

    it('should do nothing if the data container is open and clicked the element', () => {
      clickable.triggerEventHandler('click', new MouseEvent('click'));
      fixture.detectChanges();

      fixture.debugElement.nativeElement.click();
      fixture.detectChanges();
      expect(component.open).toBeTruthy();
    });

    it('should do nothing if the data container is open and clicked a child of the element', () => {
      clickable.triggerEventHandler('click', new MouseEvent('click'));
      fixture.detectChanges();

      const child = fixture.debugElement.query(
        By.css('.bm-header-user-data-container'),
      ).nativeElement;

      child.click();
      fixture.detectChanges();
      expect(component.open).toBeTruthy();
    });
  });
});
