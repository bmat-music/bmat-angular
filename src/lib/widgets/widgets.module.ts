/**
 * BMAT Widget Module
 * Module with different quasi-static components
 */
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { BMATForms } from '../forms';

import { BMBmatMenuIconComponent } from './bmat-menu-icon';
import { BMGraphQLPaginationComponent } from './grahpql-collection-pagination';
import { BMHeaderUserOptionsComponent, BMUserDetailComponent, BMUserProfileLanguageComponent,
} from './header-user-options';
import { BMPoweredByComponent } from './powered-by';
import { BMUserPortraitComponent } from './user-portrait';

@NgModule({
  imports: [
    BMATForms,
    CommonModule,
    ReactiveFormsModule,
  ],
  declarations: [
    BMBmatMenuIconComponent,
    BMGraphQLPaginationComponent,
    BMHeaderUserOptionsComponent,
    BMPoweredByComponent,
    BMUserDetailComponent,
    BMUserPortraitComponent,
    BMUserProfileLanguageComponent,
  ],
  exports: [
    BMBmatMenuIconComponent,
    BMGraphQLPaginationComponent,
    BMHeaderUserOptionsComponent,
    BMPoweredByComponent,
  ],
  providers: [],
})
export class BMATWidgets {
  // tslint:disable-next-line:function-name
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: BMATWidgets,
      providers: [
      ],
    };
  }
}
