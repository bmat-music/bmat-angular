/**
 * Jasmine Spec BMUserPortraitComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMATWidgets } from 'lib/widgets';
import { BMUserPortraitComponent } from 'lib/widgets/user-portrait';

describe('BMUserPortraitComponent', () => {
  let component: BMUserPortraitComponent;
  let fixture: ComponentFixture<BMUserPortraitComponent>;

  // tslint:disable-next-line:no-any
  let imgInst: any = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BMATWidgets.forRoot()],
      declarations: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    imgInst = {};
    fixture = TestBed.createComponent(BMUserPortraitComponent);
    component = fixture.componentInstance;
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('should create a img object and assign copy the received src to it', () => {
    // tslint:disable-next-line:no-any
    spyOn(<any>global, 'Image').and.returnValue(imgInst);
    component.src = 'SRC';
    fixture.detectChanges();
    expect(imgInst.src).toEqual('SRC');
  });

  it('should return the received src in the getter', () => {
    // tslint:disable-next-line:no-any
    spyOn(<any>global, 'Image').and.returnValue(imgInst);
    component.src = 'SRC';
    expect(component.src).toEqual('SRC');
  });

  it('if the image loading ends, should set validSrc to true', () => {
    // tslint:disable-next-line:no-any
    spyOn(<any>global, 'Image').and.returnValue(imgInst);
    component.src = 'SRC';
    fixture.detectChanges();
    imgInst.onload();
    expect(component.validSrc).toBeTruthy();
  });

  it('if the image loading fails, should set validSrc to false', () => {
    // tslint:disable-next-line:no-any
    spyOn(<any>global, 'Image').and.returnValue(imgInst);
    component.src = 'SRC';
    fixture.detectChanges();
    imgInst.onerror();
    expect(component.validSrc).toBeFalsy();
  });
});
