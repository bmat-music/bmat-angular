/**
 * Angular Component BMUserPortraitComponent
 * Dropdown user optuions
 */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';

@Component({
  selector: 'bm-user-portrait',
  templateUrl: 'user-portrait.component.html',
  styleUrls: ['user-portrait.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BMUserPortraitComponent {
  @Input()
  set src(v: string) {
    this.srcCopy = v;
    this.validSrc = false;
    this.avatarImg = new Image(100, 100);
    this.avatarImg.src = this.srcCopy;
    this.avatarImg.onload = () => {
      this.validSrc = true;
      this.changeDetectorRef.markForCheck();
    };

    this.avatarImg.onerror = () => {
      this.validSrc = false;
      this.changeDetectorRef.markForCheck();
    };
  }
  get src() {
    return this.srcCopy;
  }

  public validSrc = false;
  public avatarImg!: HTMLImageElement;
  private srcCopy!: string;

  constructor(private changeDetectorRef: ChangeDetectorRef) {}
}
