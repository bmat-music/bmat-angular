/**
 * Jasmine Spec
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMATWidgets } from 'lib/widgets';
import { BMBmatMenuIconComponent } from 'lib/widgets/bmat-menu-icon';

describe('BMBmatMenuIconComponent', () => {
  let component: BMBmatMenuIconComponent;
  let fixture: ComponentFixture<BMBmatMenuIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BMATWidgets],
      declarations: [],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMBmatMenuIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });
});
