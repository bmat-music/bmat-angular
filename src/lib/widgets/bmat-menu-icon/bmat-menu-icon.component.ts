/**
 * Angular Component. PoweredByComponent.
 */
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'bm-menu-icon',
  templateUrl: 'bmat-menu-icon.component.html',
  styleUrls: ['bmat-menu-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BMBmatMenuIconComponent {
  public currentYear: number = new Date().getFullYear();
}
