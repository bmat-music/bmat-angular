/**
 * Typescript interfaces
 */

export interface GraphQLPagination {
  /**
   * Index of first received element.
   */
  start: number;
  /**
   * Number of received items
   */
  total: number;
  /**
   * Page size
   */
  limit: number;
  /**
   * Total number of items in the server.
   */
  length: number;
}
