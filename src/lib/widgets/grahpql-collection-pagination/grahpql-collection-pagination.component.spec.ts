/**
 * Jasmine Spec BMGraphQLPaginationComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { BMGraphQLPaginationComponent } from 'lib/widgets/grahpql-collection-pagination';
import { GraphQLPagination } from 'lib/widgets/grahpql-collection-pagination/graphql-collection-pagination.interfaces';
import { restoreEvent } from 'testing/restore-event';

describe('BMGraphQLPaginationComponent', () => {
  let component: BMGraphQLPaginationComponent;
  let fixture: ComponentFixture<BMGraphQLPaginationComponent>;
  const pagination: GraphQLPagination = {
    start: 20,
    length: 20,
    limit: 20,
    total: 200,
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [BMGraphQLPaginationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMGraphQLPaginationComponent);

    component = fixture.componentInstance;
    component.pagination = pagination;
    fixture.detectChanges();

    restoreEvent();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('should not break if received a null input', () => {
    expect(() => {
      // tslint:disable-next-line:no-any
      component.pagination = null as any;
      fixture.detectChanges();
    }).not.toThrow();
  });

  describe('pagination mappings', () => {
    it('should patch the page input with the current page', () => {
      spyOn(component.pageInput, 'patchValue');
      component.pagination = {
        ...pagination,
      };
      expect(component.pageInput.patchValue).toHaveBeenCalledWith({
        page: 2,
      });

      component.pagination = {
        ...pagination,
        start: 0,
      };
      expect(component.pageInput.patchValue).toHaveBeenCalledWith({
        page: 1,
      });

      component.pagination = {
        ...pagination,
        start: 21,
      };
      expect(component.pageInput.patchValue).toHaveBeenCalledWith({
        page: 2,
      });
    });
  });

  describe('first and last page functions', () => {
    it('should correctly set the boolean isFirst', () => {
      component.pagination = {
        ...pagination,
      };
      expect(component.isFirst).toBeFalsy();

      component.pagination = {
        ...pagination,
        start: 0,
      };
      expect(component.isFirst).toBeTruthy();
    });

    it('should correctly set the boolean isLast', () => {
      component.pagination = {
        ...pagination,
      };
      expect(component.isLast).toBeFalsy();

      component.pagination = {
        ...pagination,
        start: 0,
      };
      expect(component.isLast).toBeFalsy();

      component.pagination = {
        ...pagination,
        start: 180,
      };
      expect(component.isLast).toBeTruthy();

      component.pagination = {
        ...pagination,
        start: 180,
        limit: 20,
        length: 10,
        total: 185,
      };
      expect(component.isLast).toBeTruthy();
    });
  });

  describe('jump to page functions', () => {
    it('goToFirst method should emit the correct goToPage number', () => {
      spyOn(component.goToPage, 'emit');
      component.goToFirst(new Event('click'));
      expect(component.goToPage.emit).toHaveBeenCalledWith(1);
    });

    it('goToLast method should emit the correct goToPage number', () => {
      spyOn(component.goToPage, 'emit');
      component.goToLast(new Event('click'));
      expect(component.goToPage.emit).toHaveBeenCalledWith(10);
    });

    it('deltaPage should emit the requested goToPage using the delta argument', () => {
      spyOn(component.goToPage, 'emit');
      component.deltaPage(new Event('click'), -1);
      expect(component.goToPage.emit).toHaveBeenCalledWith(1);

      component.deltaPage(new Event('click'), 1);
      expect(component.goToPage.emit).toHaveBeenCalledWith(3);
    });

    it('deltaPage should respect the lower page bound', () => {
      spyOn(component.goToPage, 'emit');
      component.deltaPage(new Event('click'), -100);
      expect(component.goToPage.emit).toHaveBeenCalledWith(1);
    });

    it('deltaPage should respect the upper page bound', () => {
      spyOn(component.goToPage, 'emit');
      component.deltaPage(new Event('click'), 100);
      expect(component.goToPage.emit).toHaveBeenCalledWith(10);
    });
  });

  describe('submit form function', () => {
    it('goToPage should emit the value of the input', () => {
      spyOn(component.goToPage, 'emit');
      component.submit(new Event('submit'));
      expect(component.goToPage.emit).toHaveBeenCalledWith(2);
    });

    it('should respect the lower page bound', () => {
      spyOn(component.goToPage, 'emit');
      component.pageInput.patchValue({ page: -10 });
      component.submit(new Event('submit'));
      expect(component.goToPage.emit).toHaveBeenCalledWith(1);
    });

    it('should respect the upper page bound', () => {
      spyOn(component.goToPage, 'emit');
      component.pageInput.patchValue({ page: 100 });
      component.submit(new Event('submit'));
      expect(component.goToPage.emit).toHaveBeenCalledWith(10);
    });
  });
});
