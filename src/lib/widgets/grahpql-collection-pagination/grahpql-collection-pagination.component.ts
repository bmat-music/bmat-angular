import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { GraphQLPagination } from './graphql-collection-pagination.interfaces';

@Component({
  selector: 'bm-graphql-pagination',
  templateUrl: 'grahpql-collection-pagination.component.html',
  styleUrls: ['grahpql-collection-pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
/**
 * Angular Component. Displays controls of pagination in tables
 */
export class BMGraphQLPaginationComponent {
  public pageInput = new FormGroup({
    page: new FormControl(1),
  });

  @Input()
  set pagination(v: GraphQLPagination) {
    if (v) {
      this.currentPage = 1 + Math.floor(v.start / v.limit);
      this.numberPages = Math.ceil(v.total / v.limit);
      this.isFirst = v.start === 0;
      this.isLast = v.length + v.start >= v.total;
      this.pageInput.patchValue({
        page: this.currentPage,
      });

      this.inputWidth = 2 + this.numberPages.toString().length + 'rem';
    }
  }

  @Output() public goToPage = new EventEmitter<number>();

  public numberPages!: number;
  public isFirst!: boolean;
  public isLast!: boolean;
  public inputWidth = '5rem';

  private currentPage!: number;

  public deltaPage($event: Event, delta: number) {
    $event.preventDefault();
    const destination = Math.min(
      Math.max(1, this.currentPage + delta),
      this.numberPages,
    );
    this.goToPage.emit(destination);
  }

  public goToFirst($event: Event) {
    $event.preventDefault();
    this.goToPage.emit(1);
  }

  public goToLast($event: Event) {
    $event.preventDefault();
    this.goToPage.emit(this.numberPages);
  }

  public submit($event: Event) {
    $event.preventDefault();
    const destination = Math.min(
      Math.max(1, this.pageInput.value.page),
      this.numberPages,
    );

    this.goToPage.emit(destination);
  }
}
