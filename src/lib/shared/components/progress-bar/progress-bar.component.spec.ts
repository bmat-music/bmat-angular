/**
 * Jasmine Spec BMProgressComponent
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMATSharedModule } from 'lib/shared';
import { BMProgressComponent } from 'lib/shared/components/progress-bar';

describe('BMProgressComponent', () => {
  let component: BMProgressComponent;
  let fixture: ComponentFixture<BMProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BMATSharedModule.forRoot()],
      declarations: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BMProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeTruthy();
  });

  it('setting a non-zero falsy of the component should set the innerStyle to {}', () => {
    // tslint:disable-next-line:no-any
    component.value = null as any;
    fixture.detectChanges();
    expect(component.innerStyle).toEqual({});
  });

  it(
    'setting the value of the component should generate style.width with the % of ' +
      'the total with up to 2 decimals of precision',
    () => {
      component.value = 80;
      fixture.detectChanges();
      expect(component.innerStyle.width).toEqual('80%');
    },
  );

  it('the getter of the value should return 0 if not set or invalid', () => {
    fixture.detectChanges();
    expect(component.value).toEqual(0);
  });

  it('after setting the value of the component the value getter should return the absolute value of the %', () => {
    component.value = 80;
    fixture.detectChanges();
    expect(component.value).toEqual(80);
  });
});
