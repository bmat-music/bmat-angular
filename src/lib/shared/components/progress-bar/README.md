# BMProgressComponent
Selector **bm-progress**

An animated progress bar.

@Input max: The maximum value of the progress bar.

@Input value: The value of the progress bar. Will set 
the width of inner bar to the % of value / max.

@Input animate: Animate the transitions, defaults to true