/**
 * Angular Component BMProgressBar
 * Polyfill/Stylable progress bar
 */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
} from '@angular/core';

@Component({
  selector: 'bm-progress',
  templateUrl: 'progress-bar.component.html',
  styleUrls: ['progress-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BMProgressComponent {
  @Input() public max = 100;
  @Input()
  set value(v: number) {
    if ((!v && v !== 0) || Number.isNaN(v)) {
      this.innerStyle = {};
    } else {
      const width = v / this.max;
      this.innerStyle = {
        width: Math.round(10000 * width) / 100 + '%',
      };
    }
    this.changeDetectorRef.markForCheck();
  }
  get value() {
    if (this.innerStyle.width) {
      return Number.parseFloat(this.innerStyle.width.slice(0, -1));
    }
    return 0;
  }
  /**
   * Sets if the bar should display or not an animation after value changes
   */
  @Input()
  @HostBinding('attr.static')
  public staticBar = false;

  public innerStyle: { width?: string } = {};

  constructor(private changeDetectorRef: ChangeDetectorRef) {}
}
