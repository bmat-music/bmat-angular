/**
 * Jasmine Spec BMTemplateMarkerDirective
 */
import { Component } from '@angular/core';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMTemplateMarkerDirective } from 'lib/shared/directives/template-marker/template-marker.directive';

@Component({
  template:
    '<ng-container><ng-template bmTemplate></ng-template></ng-container>',
})
export class WrapperComponent {}

describe('BMTemplateMarkerDirective', () => {
  let component: WrapperComponent;
  let fixture: ComponentFixture<WrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [WrapperComponent, BMTemplateMarkerDirective],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('directive should not break component', () => {
    expect(component).toBeTruthy();
  });
});
