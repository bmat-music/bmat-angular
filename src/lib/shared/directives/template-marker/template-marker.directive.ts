import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[bmTemplate]',
})
/**
 * Directive to look for children anonymous templates
 */
export class BMTemplateMarkerDirective {
  /**
   * To name the kind of the template. Values depend on the
   * parent component.
   */
  @Input() public bmTemplate!: string;
  constructor(
    // tslint:disable-next-line:no-any
    public templateRef: TemplateRef<any>,
    public viewContainerRef: ViewContainerRef,
  ) {}
}
