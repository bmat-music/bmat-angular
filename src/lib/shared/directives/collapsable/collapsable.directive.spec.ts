/**
 * Jasmine Spec BMCollapsableDirective
 */
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BMCollapsableDirective } from 'lib/shared/directives/collapsable/collapsable.directive';

@Component({
  template: '<div bmCollapsable></div>',
})
export class WrapperComponent {}

describe('BMCollapsableDirective', () => {
  // tslint:disable-next-line:no-any
  let component: any;
  let fixture: ComponentFixture<WrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [WrapperComponent, BMCollapsableDirective],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('directive should not break component', () => {
    expect(component).toBeTruthy();
  });

  it('directive should bind attribute collapse to collapse property', () => {
    const deb = fixture.debugElement.query(
      By.directive(BMCollapsableDirective),
    );
    const directive = deb.injector.get(
      BMCollapsableDirective,
    ) as BMCollapsableDirective;

    directive.collapse = true;
    fixture.detectChanges();
    expect(deb.nativeElement.hasAttribute('collapse')).toBeTruthy();

    directive.collapse = false;
    fixture.detectChanges();
    expect(deb.nativeElement.getAttribute('collapse')).toEqual('false');
  });
});
