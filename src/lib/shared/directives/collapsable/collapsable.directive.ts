/**
 * Angular Directive BMCollapsableDirective
 * Indicates that a class is has a collapse class than can
 * be triggered using selector
 */
import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[bmCollapsable]',
})
export class BMCollapsableDirective {
  @Input()
  @HostBinding('attr.collapse')
  public collapse!: boolean | null;
}
