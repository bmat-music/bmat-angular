# Collapsable directive
Selector **[bmCollapsable]**

Used by components with a collapse state to add the attribute collapse
 to the hosts of the directive.