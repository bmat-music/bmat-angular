# ParseDurationPipe
Angular pipe to transform seconds to a string with format like HH:MM:SS. Uses the selector
**bmParseDuration**.

For example
```
{{'182' | bmParseDuration}}
```
will output
```
00:03:02
```
## Format options
Format defaults to **HH:MM:SS**. The meaning is
* H - Hours
* M - Minutes
* S - Seconds

The output format can be overriden with a custom one. Combine the letters 
with the **:** character. Using two times the same letter forces the output to show 2 digits.

For example, using again an input of 182, the output will be:
* **MM:SS**: 03:02
* **M:SS** : 3:02
* **H:MM:SS**: 0:03:02
