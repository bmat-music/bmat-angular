/**
 * Angular Pipe. ParseDurationPipe.
 * Transforms second to HH:MM:SS
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bmParseDuration',
})
export class ParseDurationPipe implements PipeTransform {
  // tslint:disable-next-line:no-any
  public transform(value: any, format: string = 'HH:MM:SS') {
    const coerceInteger = Number.parseInt(value, 10);
    if (Number.isNaN(coerceInteger)) {
      return 'N/A';
    }

    const seconds = coerceInteger % 60;
    const minutes = Math.floor(coerceInteger / 60) % 60;
    const hours = Math.floor(coerceInteger / 3600);

    return format
      .split(':')
      .map(segment => {
        const { length } = segment;
        if (length === 0) {
          throw new TypeError('Invalid format. Empty segment.');
        }
        if (length > 2) {
          throw new TypeError('Invalid format for segment ' + segment);
        }
        if (segment.length === 2 && segment[0] !== segment[1]) {
          throw new TypeError('Invalid format for segment ' + segment);
        }

        let val: string;
        switch (segment[0]) {
          case 'H':
            val = hours.toString();
            break;
          case 'M':
            val = minutes.toString();
            break;
          case 'S':
            val = seconds.toString();
            break;
          default:
            throw new TypeError('Invalid format for segment ' + segment);
        }
        while (val.length < length) {
          val = '0' + val;
        }
        return val;
      })
      .join(':');
  }
}
