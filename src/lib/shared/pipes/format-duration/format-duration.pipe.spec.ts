/**
 * Jasmine Spec. ParseDurationPipe.
 */
import { ParseDurationPipe } from 'lib/shared/pipes/format-duration/format-duration.pipe';

describe('ParseDurationPipe', () => {
  // This pipe is a pure, stateless function so no need for BeforeEach
  let pipe: ParseDurationPipe;

  beforeEach(() => {
    pipe = new ParseDurationPipe();
  });

  it('should display N/A if not given a number like value', () => {
    expect(pipe.transform('XX')).toEqual('N/A');
  });

  it('should add zeros to produce 2 digit cells', () => {
    expect(pipe.transform(1)).toEqual('00:00:01');
    expect(pipe.transform(60)).toEqual('00:01:00');
    expect(pipe.transform(3600)).toEqual('01:00:00');
  });

  it('should map seconds to hh:mm:ss', () => {
    expect(pipe.transform(3695)).toEqual('01:01:35');
  });

  describe('custom formats', () => {
    it('should map Hs to hours', () => {
      expect(pipe.transform(3695, 'H')).toEqual('1');
      expect(pipe.transform(3695, 'HH')).toEqual('01');
    });

    it('should map Ms to minutes', () => {
      expect(pipe.transform(60, 'M')).toEqual('1');
      expect(pipe.transform(60, 'MM')).toEqual('01');
    });

    it('should map Ss to seconds', () => {
      expect(pipe.transform(6, 'S')).toEqual('6');
      expect(pipe.transform(6, 'SS')).toEqual('06');
    });

    it('should throw an error if receives format segments without H, M or S', () => {
      // tslint:disable-next-line:no-function-expression
      expect(function () {
        pipe.transform(3695, 'XX:MM:SS');
      }).toThrow('Invalid format for segment XX');
    });

    it('should throw an error if receives format segments with 3 or more letters', () => {
      // tslint:disable-next-line:no-function-expression
      expect(function () {
        pipe.transform(3695, 'HHH:MM:SS');
      }).toThrow('Invalid format for segment HHH');
    });

    it('should throw an error if receives format segments with 0 letters', () => {
      // tslint:disable-next-line:no-function-expression
      expect(function () {
        pipe.transform(3695, ':MM:SS');
      }).toThrow('Invalid format. Empty segment.');
    });

    it('should throw an error if receives format segments with differents letters', () => {
      // tslint:disable-next-line:no-function-expression
      expect(function () {
        pipe.transform(3695, 'HM:MM:SS');
      }).toThrow('Invalid format for segment HM');
    });
  });

});
