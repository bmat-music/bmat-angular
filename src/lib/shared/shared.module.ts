/**
 * BMAT Core Module
 */
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { BMProgressComponent } from './components/progress-bar';

import { BMCollapsableDirective } from './directives/collapsable';
import { BMTemplateMarkerDirective } from './directives/template-marker';

import { ParseDurationPipe } from './pipes/format-duration';

@NgModule({
  imports: [CommonModule],
  declarations: [
    BMCollapsableDirective,
    BMProgressComponent,
    BMTemplateMarkerDirective,
    ParseDurationPipe,
  ],
  exports: [
    BMCollapsableDirective,
    BMProgressComponent,
    BMTemplateMarkerDirective,
    ParseDurationPipe,
  ],
  providers: [],
})
export class BMATSharedModule {
  // tslint:disable-next-line:function-name
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: BMATSharedModule,
      providers: [],
    };
  }
}
