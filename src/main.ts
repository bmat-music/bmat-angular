/**
 * Demo Entry File
 */
import 'font-awesome/css/font-awesome.css';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { DemoModule } from 'demo-app/demo.module';

platformBrowserDynamic().bootstrapModule(DemoModule);
