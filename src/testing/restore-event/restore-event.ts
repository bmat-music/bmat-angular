import { JSDOM } from 'jsdom';

/**
 * Angular v6, to support Universal,
 */
export function restoreEvent() {
  Object.defineProperty(window, 'Event', {
    value: new JSDOM().window.Event,
  });

  Object.defineProperty(window, 'DragEvent', {
    value: new JSDOM().window.Event,
  });
}
