/**
 * Jest Mocks
 */
const mock = () => {
  let storage: { [key: string]: string } = {};
  return {
    getItem: (key: string) => (key in storage ? storage[key] : null),
    setItem: (key: string, value: string) => (storage[key] = value || ''),
    removeItem: (key: string) => delete storage[key],
    clear: () => (storage = {}),
  };
};

Object.defineProperty(window, 'localStorage', { value: mock() });
Object.defineProperty(window, 'sessionStorage', { value: mock() });
Object.defineProperty(window, 'getComputedStyle', {
  value: () => ['-webkit-appearance'],
});
Object.defineProperty(window, 'DragEvent', { value: mock() });
Object.defineProperty(window, 'dialogPolyfill', {
  value: {
    registerDialog(e: HTMLDialogElement) {
      Object.defineProperties(e, {
        close: {
          value: jest.fn(() => {
            if (!e.open) {
              e.open = false;
            }
          }),
        },
        showModal: {
          value: jest.fn(() => {
            if (!e.open) {
              e.open = true;
            }
          }),
        },
      });
    },
  },
});

Object.defineProperty(window, 'vericast', {
  value: {},
});

Object.defineProperty(document.body.style, 'transform', {
  value: () => {
    return {
      enumerable: true,
      configurable: true,
    };
  },
});