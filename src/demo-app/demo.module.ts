/**
 * Demo Module. Page with the differents widgets
 */
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule  } from '@angular/platform-browser';
import { BrowserAnimationsModule  } from '@angular/platform-browser/animations';

import { BMATAngular, NotificationServiceModule } from 'lib';
import { DemoComponent } from './demo.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BMATAngular.forRoot(),
    NotificationServiceModule,
  ],
  declarations: [
    DemoComponent,
  ],
  providers: [],
  bootstrap: [DemoComponent],
})
export class DemoModule { }
