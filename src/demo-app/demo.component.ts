/**
 * Angular Component.
 * Demo with all the components
 */
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BMFileUploadChangeEvent } from 'lib/forms/file-uploader-base';
import { NotificationService } from 'lib/notification-service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'demo-app',
  templateUrl: 'demo.component.html',
  styleUrls: ['demo.component.scss'],
})
export class DemoComponent {
  public formValue: {
    checkbox: boolean;
    radio: string;
    // tslint:disable-next-line:no-any
    select: any;
    input: string;
    progress: number;
    time: number | null;
    channels: string[];
  } = {
    checkbox: true,
    radio: 'value1',
    select: 2,
    input: 'text',
    progress: 80,
    time: null,
    channels: ['te-tes-2----------------------02'],
  };

  public formGroup = new FormGroup({
    check: new FormControl(true),
    radio: new FormControl('value2'),
    select: new FormControl(2),
    input: new FormControl(null, Validators.required),
    progress: new FormControl(80),
  });

  // tslint:disable-next-line:no-any
  public uploaderEvents: any = [];

  public dataTable = [
    {
      field_1: 'SRC1',
      field_2: 'MUSIC 1',
    },
    {
      field_1: 'SRC2',
      field_2: 'MUSIC 2',
    },
    {
      field_1: 'SRC3',
      field_2: 'MUSIC 3',
      overlay: 'delete',
    },
  ];

  public possibleChannels = [
    { key: 'te-tes-2----------------------02', displayValue: 'Venue' },
    { key: 'ie-radio-kerry----------------01', displayValue: 'Radio Kerry' },
    { key: 'los40-direct-url--------------01', displayValue: '40 Principales' },
    { key: 'fr-radio-fun-radio------------01', displayValue: 'Fun Radio' },
    { key: 'es-tdt-antena-3---------------01', displayValue: 'Antena 3' },
    { key: 'dreambox----------------------01', displayValue: 'DreamBoxTest' },
    { key: 'au-sky-sports-radio-----------01', displayValue: 'Sky Sports Radio' },
    { key: 'dreambox----------------------02', displayValue: 'DreamBoxVid' },
    { key: 'py-radio-aspen-102-7-fm-------01', displayValue: 'Radio Aspen 102.7 FM' },
    { key: 'amadeus-lri-711---------------01', displayValue: 'RADIO TKM FM 103.7' },
  ];

  public languageList = [
    {code: 'en', name: 'English'},
    {code: 'es', name: 'Español'},
    {code: 'jp', name: '日本語'},
  ];

  constructor(private _notificacionService: NotificationService) {

  }

  public showMsg() {
    this._notificacionService.showMessage('HOLA');
  }

  public uploaderChange(event: BMFileUploadChangeEvent) {
    // tslint:disable-next-line:no-console
    console.log(event);
    this.uploaderEvents = [...this.uploaderEvents, event.value];
  }

  // tslint:disable-next-line:no-any
  public echo(v: any) {
    // tslint:disable-next-line:no-console
    console.log(v);
  }
}
