# Changelog
## Release 3.0.0
* Updated to Angular v6.
* Separated Notification Module from BMAT Angular.

## Release 2.0.1
* Missing decorators options.

## Release 2.0.0
* Updated to Angular 5.

## Release 1.0.8
* Fixed CSS issues.

## Release 1.0.5
* Fixed CSS issues.

## Release 1.0.4
* Fixed CSS issues.

## Release 1.0.3
* Change section header width.

## Release 1.0.2
* Font size issues.

## Release 1.0.1
* Added AOT and editor folders to npmignore.

## Release 1.0.0
* Incompatible API.
* Change in ViewEncapsulation + Use CSS BEM and variables
* Removed deprecated FileUploader due to the addition of
  HTTPClient to Angular.
* Updated packages.

## Release 0.7.0
* Update Angular packages.

## Release 0.6.3
* Notification system.

## Release 0.6.2
* Updated dependencies.

## Release 0.6.1
* Updated dependencies.
* Solved Linting errors.
* Add 1 rem to GraphQL pagination width.

## Release 0.6.0
* Strictier Typescript settings.

## Release 0.5.0
* UI adjustments.
* Updated dependencies.

## Release 0.4.1
* Input duration displays the placeholder if received a non zero falsy
  value.
* Added bm-button:disabled styles.
* Use ChromeHeadless as test browser.

## Release 0.4.0
* Corrected semantic of elements.
* Forced box-sizing: border-box in bm-select to have the same cross
  browser behaviour.
* Set user header info width using rem and vw units.
* Improvements in accessibility.
## Release 0.3.1
* Bug in animation definition.
## Release 0.3.0
* New clickable file uploader.
* Solved bug in bm-select. May change behaviour.
## Release 0.2.0
* Language list in user preferences is now configurable.
* Improvements in linting and test coverage.
* Updated dependencies.
* Solved bug in BMSelectComponent propagation changes.
* Cosmetic changes.
* GraphQLPagination will be shown only if the set is not empty.
* Updated packages.json.
## Release 0.1.0
* Added GraphQL pagination widget.
* Improved linting.
* Updated dependencies.
## Release 0.0.5
* Bug in template typings.
* Updated package.json
## Release 0.0.4
* Bug in translations
## Release 0.0.3
* Solved bug in i18n support of BMFileUploader.
## Release 0.0.2
* Added AOT suport (compatibility with @angular/cli).
## Release 0.0.1
* Basic set of widgets and components.
* Partial testing coverage.
* Demo app with a single page showing all the included components
  and directives.
* Build scripts to generate NPM packages.
