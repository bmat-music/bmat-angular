const gulp = require('gulp');
const sass = require('node-sass');
const runSequence = require('run-sequence');
const embedTemplates = require('gulp-inline-ng2-template');
const { exec } = require('child_process');

//----
//build steps
gulp.task('build', done => {
  runSequence(
    'clean',
    'embedd-templates',
    'ngc',
    'copy-tmp-to-lib',
    'copy-styles');
});

gulp.task('clean', () => {
  runSequence(
    'clean-lib',
    'clean-tmp');
});

//----
//clearing the output dir
gulp.task('clean-lib', done => {
  exec('rm -rf lib', err =>  {
    if (err){
      done(err);
    } else {
      done(null);
    }
  });
});

gulp.task('clean-tmp', done => {
  exec('rm -rf tmp', err =>  {
    if (err){
      done(err);
    } else {
      done(null);
    }
  });
});

gulp.task('ngc', done => {
  exec('npm run build:aot', err =>  {
    if (err){
      done(err);
    } else {
      done(null);
    }
  });
});

gulp.task('copy-styles', () => {
  return gulp.src([
    'src/styles.scss',
    'src/page.scss',
    'src/angular_components.scss',
    'src/styles/**/*.scss',
  ], { base : 'src' })
  .pipe(gulp.dest('lib'));
})

gulp.task('copy-tmp-to-lib', () => {
  return gulp.src([
    'tmp/**/*d.ts',
    'tmp/**/*.json',
    'tmp/**/*.js',
    'tmp/**/*.js.map',
  ])
  .pipe(gulp.dest('lib'));
});

gulp.task('embedd-templates', function() {

//loading typings file

  return gulp.src([
      'src/lib/**/*.ts',
      '!src/lib/**/*.spec.ts'
    ])
    .pipe(embedTemplates({
       useRelativePaths: true,
       includePaths: [
         'src/lib',
       ],
       styleProcessor: (path, ext, file, cb) => {
         sass.render({
           file: path,
         }, (err, result)  => {
           if (err) {
             cb(err, null);
           } else {
             cb(null, result.css.toString());
           }
         });
       }
    }))
    .pipe(gulp.dest('tmp'));
});