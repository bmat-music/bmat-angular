var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Angular Component BMContentComponent
 */
import { ChangeDetectionStrategy, Component, ViewEncapsulation, } from '@angular/core';
var BMContentComponent = /** @class */ (function () {
    function BMContentComponent() {
    }
    BMContentComponent = __decorate([
        Component({
            selector: 'bm-content-container',
            template: "\n    <ng-content></ng-content>\n  ",
            styles: ["\n    bm-content-container {\n      flex: 1 1 0;\n      overflow-x: auto;\n      height: calc(100% - 48px - 3em);\n      box-sizing: content-box;\n      padding: 1em 1.5em; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
        })
    ], BMContentComponent);
    return BMContentComponent;
}());
export { BMContentComponent };
//# sourceMappingURL=bm-content.component.js.map