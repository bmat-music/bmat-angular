var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component. BMHeaderComponent
 * Header Toolbar
 */
import { ChangeDetectionStrategy, Component, HostBinding, ViewEncapsulation, } from '@angular/core';
var BMHeaderComponent = /** @class */ (function () {
    function BMHeaderComponent() {
        this.role = 'heading';
    }
    __decorate([
        HostBinding('attr.role'),
        __metadata("design:type", Object)
    ], BMHeaderComponent.prototype, "role", void 0);
    BMHeaderComponent = __decorate([
        Component({
            selector: 'bm-header',
            template: "\n    <ng-content></ng-content>\n  ",
            styles: ["\n    bm-header {\n      padding: 6px 16px;\n      display: flex;\n      flex-direction: row-reverse;\n      align-items: center;\n      background-color: var(--header-background-color, #3e71ad);\n      color: var(--header-color, #f8f8f8);\n      font-weight: 600;\n      font-size: 2em;\n      flex: 1 1 100%;\n      box-sizing: border-box; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
        })
    ], BMHeaderComponent);
    return BMHeaderComponent;
}());
export { BMHeaderComponent };
//# sourceMappingURL=bm-header.component.js.map