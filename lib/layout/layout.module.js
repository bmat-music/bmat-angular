var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * BMAT Layout Module
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BMATSharedModule } from '../shared';
import { BMContentComponent } from './bm-content';
import { BMHeaderComponent } from './bm-header';
import { BMSidenavContainerComponent } from './bm-sidenav';
var BMATLayout = /** @class */ (function () {
    function BMATLayout() {
    }
    BMATLayout_1 = BMATLayout;
    // tslint:disable-next-line:function-name
    BMATLayout.forRoot = function () {
        return {
            ngModule: BMATLayout_1,
            providers: [],
        };
    };
    BMATLayout = BMATLayout_1 = __decorate([
        NgModule({
            imports: [CommonModule, BMATSharedModule],
            declarations: [
                BMHeaderComponent,
                BMSidenavContainerComponent,
                BMContentComponent,
            ],
            exports: [BMHeaderComponent, BMSidenavContainerComponent, BMContentComponent],
            providers: [],
        })
    ], BMATLayout);
    return BMATLayout;
    var BMATLayout_1;
}());
export { BMATLayout };
//# sourceMappingURL=layout.module.js.map