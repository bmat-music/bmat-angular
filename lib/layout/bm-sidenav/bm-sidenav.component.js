var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component BMSidenav
 * Side navigation menu
 */
import { animate, state, style, transition, trigger, } from '@angular/animations';
import { ChangeDetectionStrategy, Component, ContentChildren, EventEmitter, HostBinding, Input, Output, QueryList, ViewEncapsulation, } from '@angular/core';
import { BMCollapsableDirective } from '../../shared/directives/collapsable';
var BMSidenavContainerComponent = /** @class */ (function () {
    function BMSidenavContainerComponent() {
        this.role = 'navigation';
        this.openMenu = new EventEmitter();
    }
    Object.defineProperty(BMSidenavContainerComponent.prototype, "open", {
        get: function () {
            return this._isOpen;
        },
        set: function (v) {
            this._isOpen = v;
            if (this.collapsableChilds) {
                this.collapsableChilds.forEach(function (child) { return (child.collapse = v ? null : true); });
            }
        },
        enumerable: true,
        configurable: true
    });
    BMSidenavContainerComponent.prototype.toggle = function () {
        this.open = !this.open;
    };
    BMSidenavContainerComponent.prototype.animationDone = function (event) {
        if (event.fromState === 'expand' && event.toState === 'collapse') {
            this.openMenu.emit(false);
        }
        if (event.fromState === 'collapse' && event.toState === 'expand') {
            this.openMenu.emit(true);
        }
    };
    __decorate([
        ContentChildren(BMCollapsableDirective),
        __metadata("design:type", QueryList)
    ], BMSidenavContainerComponent.prototype, "collapsableChilds", void 0);
    __decorate([
        HostBinding('attr.role'),
        __metadata("design:type", Object)
    ], BMSidenavContainerComponent.prototype, "role", void 0);
    __decorate([
        HostBinding('attr.open'),
        Input(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], BMSidenavContainerComponent.prototype, "open", null);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], BMSidenavContainerComponent.prototype, "openMenu", void 0);
    BMSidenavContainerComponent = __decorate([
        Component({
            selector: 'bm-sidenav',
            template: "\n    <div class=\"bm-sidenav__container\" [@stretch]=\"(open ? 'expand' : 'collapse')\" (@stretch.done)=\"animationDone($event)\">\n      <div class=\"bm-sidenav__toggle-container\">\n        <button class=\"bm-sidenav__toggle-button\" i18n-arial-label aria-label=\"Toggle menu\" i18n-title title=\"Toggle menu\" (click)=\"toggle()\">\n          <i class=\"fa fa-chevron-right\" aria-hidden=\"true\" [@rotate]=\"(open ? 'expand' : 'collapse')\"></i>\n        </button>\n      </div>\n      <ng-content></ng-content>\n    </div>\n  ",
            styles: ["\n    bm-sidenav {\n      box-sizing: content-box;\n      height: calc(100% - 48px);\n      position: relative;\n      display: inline-block;\n      color: var(--header-color, #f8f8f8);\n      flex: 0 1 auto; }\n\n    .bm-sidenav__container {\n      height: 100%;\n      display: flex; }\n\n    .bm-sidenav__toggle-container {\n      position: absolute;\n      right: -1.2em;\n      top: 50%; }\n\n    .bm-sidenav__toggle-button {\n      vertical-align: middle;\n      border: none;\n      background: none;\n      margin-left: auto;\n      margin-right: 0;\n      padding: 0;\n      width: 0.6em;\n      height: 1.5em;\n      color: var(--header-background-color, #3e71ad); }\n\n    .bm-sidenav__sections-container {\n      padding: 0;\n      list-style: none;\n      margin: 0;\n      height: 100%;\n      width: 4.3em;\n      background-color: var(--header-background-color, #3e71ad); }\n\n    .bm-sidenav-section-link {\n      display: block;\n      text-decoration: none;\n      color: #f8f8f8;\n      font-size: inherit; }\n      .bm-sidenav-section-link__icon {\n        font-size: 2em;\n        display: inline-block;\n        padding: 0.5em; }\n      .bm-sidenav-section-link--selected {\n        color: var(--header-background-color, #3e71ad);\n        background-color: var(--header-color, #f8f8f8); }\n\n    .bm-sidenav-submenu__container {\n      background-color: var(--header-color, #f8f8f8);\n      height: 100%;\n      flex: 1 1 auto; }\n\n    .bm-sidenav-product-logo {\n      width: 100%;\n      text-align: center;\n      position: relative;\n      display: block;\n      height: 5em; }\n      .bm-sidenav-product-logo__graphic {\n        position: absolute;\n        top: -0.5em;\n        left: 0.3em;\n        display: inline-block;\n        color: var(--header-background-color, #3e71ad);\n        height: 3.2em;\n        width: 3.2em; }\n\n    .bm-sidenav-subsection {\n      padding: 0 0.5em; }\n      .bm-sidenav-subsection--first {\n        margin-top: 3rem; }\n      .bm-sidenav-subsection__separator-line {\n        border: 1px solid #d8d8d8; }\n      .bm-sidenav-subsection-title {\n        height: 14px;\n        width: 100%;\n        color: #333;\n        font-size: 12px;\n        font-weight: 600;\n        line-height: 14px;\n        text-transform: uppercase;\n        padding: 0.5em; }\n      .bm-sidenav-subsection-menu {\n        padding: 0;\n        list-style: none;\n        margin: 0;\n        width: 100%;\n        color: var(--header-background-color, #3e71ad); }\n        .bm-sidenav-subsection-menu__item {\n          padding: 0.5em;\n          display: block; }\n          .bm-sidenav-subsection-menu__item--selected {\n            background-color: #eaeaea; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
            animations: [
                trigger('stretch', [
                    state('expand', style({ width: '14rem', 'max-width': '100%' })),
                    state('collapse', style({ width: '4em', 'max-width': 'initial' })),
                    transition('expand => collapse', [
                        style({
                            width: '14rem',
                            'max-width': '100%',
                        }),
                        animate('0.2s ease-in'),
                    ]),
                    transition('collapse => expand', [
                        style({
                            width: '4em',
                            'max-width': 'initial',
                        }),
                        animate('0.2s ease-out'),
                    ]),
                ]),
                trigger('rotate', [
                    state('expand', style({ transform: 'rotate(0)' })),
                    state('collapse', style({ transform: 'rotate(-180deg)' })),
                    transition('expand => collapse', animate('0.2s ease-in')),
                    transition('collapse => expand', animate('0.2s ease-out')),
                ]),
            ],
        })
    ], BMSidenavContainerComponent);
    return BMSidenavContainerComponent;
}());
export { BMSidenavContainerComponent };
//# sourceMappingURL=bm-sidenav.component.js.map