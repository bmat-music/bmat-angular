/**
 * Angular Component BMSidenav
 * Side navigation menu
 */
import { AnimationEvent } from '@angular/animations';
import { EventEmitter, QueryList } from '@angular/core';
import { BMCollapsableDirective } from '../../shared/directives/collapsable';
export declare class BMSidenavContainerComponent {
    collapsableChilds: QueryList<BMCollapsableDirective>;
    role: string;
    open: boolean;
    openMenu: EventEmitter<boolean>;
    private _isOpen;
    toggle(): void;
    animationDone(event: AnimationEvent): void;
}
