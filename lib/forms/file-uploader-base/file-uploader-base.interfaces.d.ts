/**
 * Typescript Interfaces
 */
export declare enum FileStatus {
    EMPTY = 0,
    LOADING = 1,
    DONE = 2,
    ERROR = 3,
}
export declare type PartialFile = Partial<File>;
export declare type FileUploaderFile = PartialFile & {
    readonly lastModified?: number;
    status: FileStatus;
    data: null | ArrayBuffer | string;
    loaded: number;
    total: number;
};
export interface BMFileUploadChangeEvent {
    source: any;
    value: FileUploaderFile;
    reader: FileReader;
}
