/**
 * Typescript Interfaces
 */
export var FileStatus;
(function (FileStatus) {
    FileStatus[FileStatus["EMPTY"] = 0] = "EMPTY";
    FileStatus[FileStatus["LOADING"] = 1] = "LOADING";
    FileStatus[FileStatus["DONE"] = 2] = "DONE";
    FileStatus[FileStatus["ERROR"] = 3] = "ERROR";
})(FileStatus || (FileStatus = {}));
//# sourceMappingURL=file-uploader-base.interfaces.js.map