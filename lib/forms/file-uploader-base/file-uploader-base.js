var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FileStatus } from './file-uploader-base.interfaces';
/**
 * Implements the required callback and methods of file uploading
 */
var FileUploadBase = /** @class */ (function () {
    function FileUploadBase(_renderer) {
        this._renderer = _renderer;
        this.dataType = 'DataURL';
        this.progress = new EventEmitter();
        this.load = new EventEmitter();
        this.loadstart = new EventEmitter();
        this.error = new EventEmitter();
        // tslint:disable-next-line:no-any
        this._onTouched = function (_) { return; };
        // tslint:disable-next-line:no-any
        this._onChange = function (_) { return; };
    }
    /**
     * Abort a FileReader action.
     * Does nothing if there is no currently active reader action.
     */
    FileUploadBase.prototype.abort = function () {
        return;
    };
    // ControlValueAccessor
    // tslint:disable-next-line:no-any
    FileUploadBase.prototype.writeValue = function (value) {
        if (value instanceof FileList) {
            this.readFiles(value);
        }
    };
    // tslint:disable-next-line:no-any
    FileUploadBase.prototype.registerOnChange = function (fn) {
        this._onChange = fn;
    };
    // tslint:disable-next-line:no-any
    FileUploadBase.prototype.registerOnTouched = function (fn) {
        this._onTouched = fn;
    };
    FileUploadBase.prototype.setDisabledState = function (isDisabled) {
        if (this._inputElement) {
            this._renderer.setProperty(this._inputElement.nativeElement, 'disabled', isDisabled);
        }
    };
    FileUploadBase.prototype.readFiles = function (fileList) {
        var _this = this;
        if (this.dataType !== 'None') {
            var files = Array.from(fileList);
            files.forEach(function (file) {
                var fileData = {
                    name: file.name,
                    size: file.size,
                    type: file.type,
                    status: FileStatus.EMPTY,
                    data: null,
                    // tslint:disable-next-line:no-any
                    lastModified: file.lastModified,
                    total: file.size,
                    loaded: 0,
                };
                var reader = new FileReader();
                switch (_this.dataType) {
                    case 'ArrayBuffer':
                        reader.readAsArrayBuffer(file);
                        break;
                    case 'DataURL':
                        reader.readAsDataURL(file);
                        break;
                    case 'Text':
                        reader.readAsText(file);
                        break;
                    default:
                        throw new TypeError('Invalid output type: Select ArrayBuffer, DataURL or Text');
                }
                _this.abort = reader.abort;
                reader.onerror = function () {
                    reader.abort();
                    _this.abort = function () { return; };
                    var value = Object.assign({}, fileData, {
                        status: FileStatus.ERROR,
                    });
                    _this.error.emit({
                        source: _this,
                        value: value,
                        reader: reader,
                    });
                };
                reader.onloadstart = function () {
                    var value = Object.assign({}, fileData, {
                        status: FileStatus.DONE,
                        data: reader.result,
                        loaded: fileData.size,
                    });
                    _this.loadstart.emit({
                        source: _this,
                        value: value,
                        reader: reader,
                    });
                };
                reader.onload = function () {
                    var value = Object.assign({}, fileData, {
                        status: FileStatus.DONE,
                        data: reader.result,
                        loaded: fileData.size,
                    });
                    _this.load.emit({
                        source: _this,
                        value: value,
                        reader: reader,
                    });
                    _this.abort = function () { return; };
                    // tslint:disable-next-line:no-any
                    reader = null;
                };
                reader.onprogress = function (e) {
                    var progressSegment = (e.lengthComputable ? {
                        total: e.total,
                        loaded: e.loaded,
                    } : {});
                    var value = Object.assign({}, fileData, progressSegment, {
                        status: FileStatus.LOADING,
                    });
                    _this.progress.emit({
                        source: _this,
                        value: value,
                        reader: reader,
                    });
                };
            });
        }
    };
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], FileUploadBase.prototype, "dataType", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], FileUploadBase.prototype, "progress", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], FileUploadBase.prototype, "load", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], FileUploadBase.prototype, "loadstart", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], FileUploadBase.prototype, "error", void 0);
    __decorate([
        ViewChild('inputFile', { read: ElementRef }),
        __metadata("design:type", ElementRef)
    ], FileUploadBase.prototype, "_inputElement", void 0);
    return FileUploadBase;
}());
export { FileUploadBase };
//# sourceMappingURL=file-uploader-base.js.map