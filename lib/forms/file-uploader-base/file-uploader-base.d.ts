import { EventEmitter, Renderer2 } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare type ValidDataTypes = 'ArrayBuffer' | 'DataURL' | 'Text' | 'None';
import { BMFileUploadChangeEvent } from './file-uploader-base.interfaces';
/**
 * Implements the required callback and methods of file uploading
 */
export declare abstract class FileUploadBase implements ControlValueAccessor {
    protected _renderer: Renderer2;
    dataType: ValidDataTypes;
    progress: EventEmitter<BMFileUploadChangeEvent>;
    load: EventEmitter<BMFileUploadChangeEvent>;
    loadstart: EventEmitter<BMFileUploadChangeEvent>;
    error: EventEmitter<BMFileUploadChangeEvent>;
    private _inputElement;
    constructor(_renderer: Renderer2);
    /**
     * Abort a FileReader action.
     * Does nothing if there is no currently active reader action.
     */
    abort(): void;
    writeValue(value: any): void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    setDisabledState(isDisabled: boolean): void;
    protected _onTouched: (_: any) => void;
    protected _onChange: (_: any) => void;
    protected readFiles(fileList: FileList): void;
}
