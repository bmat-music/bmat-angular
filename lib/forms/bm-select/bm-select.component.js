var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular component. BMSelectComponent
 * Complement to style Selects
 */
import { ChangeDetectionStrategy, Component, ContentChildren, Directive, ElementRef, forwardRef, Input, QueryList, Renderer2, ViewChild, ViewEncapsulation, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
var BMOptionDirective = /** @class */ (function () {
    function BMOptionDirective() {
    }
    BMOptionDirective = __decorate([
        Directive({
            selector: '[bmOption]',
        })
    ], BMOptionDirective);
    return BMOptionDirective;
}());
export { BMOptionDirective };
var BMSelectComponent = /** @class */ (function () {
    function BMSelectComponent(_renderer) {
        this._renderer = _renderer;
        /**
         * Input values to directly copy to inner checkbox
         */
        this.name = '';
        this.tabIndex = 0;
        // tslint:disable-next-line:no-reserved-keywords
        this.type = 'text';
        // tslint:disable-next-line:no-any no-empty
        this.onTouched = function () { };
        // tslint:disable:no-any
        // tslint:disable:no-empty
        this.propagateChange = function (_) { };
    }
    BMSelectComponent_1 = BMSelectComponent;
    Object.defineProperty(BMSelectComponent.prototype, "value", {
        get: function () {
            return this._valueCopy;
        },
        set: function (v) {
            this._valueCopy = v;
            this.setCheckedOption();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Custom Input functions
     */
    BMSelectComponent.prototype.inputBlur = function () {
        this.onTouched();
    };
    // tslint:disable-next-line:no-any
    BMSelectComponent.prototype.writeValue = function (value) {
        this._valueCopy = value;
        this.setCheckedOption();
    };
    // tslint:disable-next-line:no-any
    BMSelectComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    // tslint:disable-next-line:no-any
    BMSelectComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    BMSelectComponent.prototype.inputChange = function (event) {
        var value = event.target.value;
        this._valueCopy = value;
        this.propagateChange(value);
    };
    BMSelectComponent.prototype.focus = function () {
        this.select.nativeElement.focus();
    };
    BMSelectComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        this._optionsSubs = this.options.changes.subscribe(function () {
            return _this.setCheckedOption();
        });
        this.setCheckedOption();
    };
    BMSelectComponent.prototype.ngOnDestroy = function () {
        this._optionsSubs.unsubscribe();
    };
    BMSelectComponent.prototype.setCheckedOption = function () {
        var _this = this;
        if (this._valueCopy !== undefined) {
            this._renderer.setValue(this.select.nativeElement, this._valueCopy);
        }
        if (!this.options) {
            return;
        }
        this.options.forEach(function (elem) {
            var domEle = elem.nativeElement;
            if (domEle) {
                domEle.selected = domEle.value === _this._valueCopy;
            }
        });
    };
    __decorate([
        ViewChild('select'),
        __metadata("design:type", ElementRef)
    ], BMSelectComponent.prototype, "select", void 0);
    __decorate([
        ContentChildren(BMOptionDirective, { read: ElementRef }),
        __metadata("design:type", QueryList)
    ], BMSelectComponent.prototype, "options", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMSelectComponent.prototype, "name", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMSelectComponent.prototype, "tabIndex", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMSelectComponent.prototype, "required", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMSelectComponent.prototype, "disabled", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMSelectComponent.prototype, "readonly", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMSelectComponent.prototype, "type", void 0);
    __decorate([
        Input('aria-label'),
        __metadata("design:type", String)
    ], BMSelectComponent.prototype, "ariaLabel", void 0);
    __decorate([
        Input('aria-labelledby'),
        __metadata("design:type", String)
    ], BMSelectComponent.prototype, "ariaLabelledby", void 0);
    __decorate([
        Input()
        // tslint:disable-next-line:no-any
        ,
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], BMSelectComponent.prototype, "value", null);
    BMSelectComponent = BMSelectComponent_1 = __decorate([
        Component({
            selector: 'bm-select',
            template: "\n    <select #select\n      class=\"bm-select__select\"\n      [required]=\"required\"\n      [disabled]=\"disabled\"\n      [name]=\"name\"\n      [tabIndex]=\"tabIndex\"\n      [attr.aria-label]=\"ariaLabel\"\n      [attr.aria-labelledby]=\"ariaLabelledby\"\n      (change)=\"inputChange($event)\"\n      (blur)=\"inputBlur()\">\n      <ng-content></ng-content>\n    </select>\n  ",
            styles: ["\n    .bm-select__select {\n      -moz-appearance: none;\n      -ms-word-break: normal;\n      -webkit-appearance: none;\n      appearance: none;\n      background: var(--header-color, #f8f8f8);\n      border-radius: 0.333em;\n      border: none;\n      box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.2);\n      font-family: 'proxima-regular';\n      font-size: 0.815rem;\n      font-weight: 400;\n      line-height: 1.45rem;\n      padding: 0.3em 2em 0.3em 0.6em;\n      width: 100%;\n      word-break: normal; }\n\n    bm-select {\n      min-width: 200;\n      display: inline-block;\n      position: relative; }\n      bm-select--inverse .bm-select__select {\n        background: white;\n        box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.2); }\n      bm-select::after {\n        content: '\\f0d7';\n        font: normal normal normal 15px/1 FontAwesome;\n        right: 8px;\n        top: 6px;\n        position: absolute;\n        pointer-events: none; }\n  "],
            providers: [
                {
                    provide: NG_VALUE_ACCESSOR,
                    // tslint:disable-next-line:no-forward-ref
                    useExisting: forwardRef(function () { return BMSelectComponent_1; }),
                    multi: true,
                },
            ],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
        }),
        __metadata("design:paramtypes", [Renderer2])
    ], BMSelectComponent);
    return BMSelectComponent;
    var BMSelectComponent_1;
}());
export { BMSelectComponent };
//# sourceMappingURL=bm-select.component.js.map