/**
 * Angular component. BMSelectComponent
 * Complement to style Selects
 */
import { AfterContentInit, ElementRef, OnDestroy, QueryList, Renderer2 } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare class BMOptionDirective {
}
export declare class BMSelectComponent implements ControlValueAccessor, AfterContentInit, OnDestroy {
    private _renderer;
    select: ElementRef;
    options: QueryList<ElementRef>;
    /**
     * Input values to directly copy to inner checkbox
     */
    name: string;
    tabIndex: number;
    required: boolean;
    disabled: boolean;
    readonly: boolean;
    type: string;
    ariaLabel: string;
    ariaLabelledby: string;
    private _valueCopy;
    value: any;
    private _optionsSubs;
    constructor(_renderer: Renderer2);
    /**
     * Custom Input functions
     */
    inputBlur(): void;
    writeValue(value: any): void;
    registerOnChange(fn: (_: any) => {}): void;
    onTouched: () => any;
    registerOnTouched(fn: () => any): void;
    inputChange(event: Event): void;
    focus(): void;
    ngAfterContentInit(): void;
    ngOnDestroy(): void;
    private propagateChange;
    private setCheckedOption();
}
