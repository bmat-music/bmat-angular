/**
 * Angular Component. BMInputSearchComponent.
 * Input with icon
 */
import { ElementRef, EventEmitter, QueryList, Renderer2 } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare class BMInputSearchComponent implements ControlValueAccessor {
    private _renderer;
    input: ElementRef;
    suggestionList: QueryList<ElementRef>;
    /**
     * Input values to directly copy to inner checkbox
     */
    name: string;
    placeholder: string;
    tabIndex: number;
    required: boolean;
    disabled: boolean;
    readonly: boolean;
    type: string;
    ariaLabel: string;
    ariaLabelledby: string;
    value: any;
    /**
     * Suggestions specifics
     */
    showSuggestions: boolean;
    filteredSuggestions: string[];
    minSuggestionChars: number;
    suggestions: string[];
    search: EventEmitter<string>;
    constructor(_renderer: Renderer2);
    inputKeyUp(event: KeyboardEvent): void;
    suggestionKeyUp(event: KeyboardEvent, index: number, value: any): void;
    setInput(v: any): void;
    /**
     * Custom Input functions
     */
    inputBlur(): void;
    writeValue(value: any): void;
    registerOnChange(fn: (_: any) => {}): void;
    onTouched: () => any;
    registerOnTouched(fn: () => any): void;
    inputChange(event: Event): void;
    focus(): void;
    private propagateChange;
    private closeSuggestions();
}
