var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component. BMInputSearchComponent.
 * Input with icon
 */
import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, forwardRef, Input, Output, QueryList, Renderer2, ViewChild, ViewChildren, ViewEncapsulation, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
var BMInputSearchComponent = /** @class */ (function () {
    function BMInputSearchComponent(_renderer) {
        this._renderer = _renderer;
        /**
         * Input values to directly copy to inner checkbox
         */
        this.name = '';
        this.placeholder = '';
        this.tabIndex = 0;
        // tslint:disable-next-line:no-reserved-keywords
        this.type = 'text';
        /**
         * Suggestions specifics
         */
        this.showSuggestions = false;
        this.minSuggestionChars = 2;
        this.search = new EventEmitter();
        // tslint:disable-next-line:no-any no-empty
        this.onTouched = function () { };
        // tslint:disable:no-any
        // tslint:disable:no-empty
        this.propagateChange = function (_) { };
    }
    BMInputSearchComponent_1 = BMInputSearchComponent;
    Object.defineProperty(BMInputSearchComponent.prototype, "value", {
        get: function () {
            return this.input.nativeElement.value;
        },
        set: function (v) {
            if (v) {
                this._renderer.setProperty(this.input.nativeElement, 'value', v);
            }
        },
        enumerable: true,
        configurable: true
    });
    BMInputSearchComponent.prototype.inputKeyUp = function (event) {
        switch (event.keyCode) {
            case 40:// Down arrow
                if (this.suggestionList.first) {
                    this.suggestionList.first.nativeElement.focus();
                }
                break;
            case 27:// Escape
                this.closeSuggestions();
                break;
            default:
                var value_1 = event.target.value.toLowerCase();
                if (value_1.length >= this.minSuggestionChars &&
                    Array.isArray(this.suggestions)) {
                    this.showSuggestions = true;
                    this.filteredSuggestions = this.suggestions
                        .map(function (item) { return item.toString().toLowerCase(); })
                        .filter(function (item) { return item.search(value_1) !== -1; });
                }
                else {
                    this.showSuggestions = false;
                }
                return;
        }
    };
    // tslint:disable-next-line:no-any
    BMInputSearchComponent.prototype.suggestionKeyUp = function (event, index, value) {
        var suggestionLi = this.suggestionList.toArray();
        switch (event.keyCode) {
            case 40:// Up arrow
                if (index === this.suggestions.length - 1) {
                    this.suggestionList.first.nativeElement.focus();
                }
                else {
                    suggestionLi[index + 1].nativeElement.focus();
                }
                event.preventDefault();
                break;
            case 38:// Down arrow
                if (index === 0) {
                    this.suggestionList.last.nativeElement.focus();
                }
                else {
                    suggestionLi[index - 1].nativeElement.focus();
                }
                event.preventDefault();
                break;
            case 27:// Escape
                this.closeSuggestions();
                break;
            case 13:// Enter
                this.setInput(value);
                break;
            default:
                return;
        }
    };
    // tslint:disable-next-line:no-any
    BMInputSearchComponent.prototype.setInput = function (v) {
        this.value = v;
        this.propagateChange(this.value);
        this.closeSuggestions();
    };
    /**
     * Custom Input functions
     */
    BMInputSearchComponent.prototype.inputBlur = function () {
        this.onTouched();
    };
    // tslint:disable-next-line:no-any
    BMInputSearchComponent.prototype.writeValue = function (value) {
        this._renderer.setProperty(this.input.nativeElement, 'value', value);
    };
    // tslint:disable-next-line:no-any
    BMInputSearchComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    // tslint:disable-next-line:no-any
    BMInputSearchComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    BMInputSearchComponent.prototype.inputChange = function (event) {
        var value = event.target.value;
        this.propagateChange(value);
    };
    BMInputSearchComponent.prototype.focus = function () {
        this.input.nativeElement.focus();
    };
    BMInputSearchComponent.prototype.closeSuggestions = function () {
        this.input.nativeElement.focus();
        this.showSuggestions = false;
    };
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], BMInputSearchComponent.prototype, "input", void 0);
    __decorate([
        ViewChildren('sugg'),
        __metadata("design:type", QueryList)
    ], BMInputSearchComponent.prototype, "suggestionList", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMInputSearchComponent.prototype, "name", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMInputSearchComponent.prototype, "placeholder", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMInputSearchComponent.prototype, "tabIndex", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMInputSearchComponent.prototype, "required", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMInputSearchComponent.prototype, "disabled", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMInputSearchComponent.prototype, "readonly", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMInputSearchComponent.prototype, "type", void 0);
    __decorate([
        Input('aria-label'),
        __metadata("design:type", String)
    ], BMInputSearchComponent.prototype, "ariaLabel", void 0);
    __decorate([
        Input('aria-labelledby'),
        __metadata("design:type", String)
    ], BMInputSearchComponent.prototype, "ariaLabelledby", void 0);
    __decorate([
        Input()
        // tslint:disable-next-line:no-any
        ,
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], BMInputSearchComponent.prototype, "value", null);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMInputSearchComponent.prototype, "minSuggestionChars", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], BMInputSearchComponent.prototype, "suggestions", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], BMInputSearchComponent.prototype, "search", void 0);
    BMInputSearchComponent = BMInputSearchComponent_1 = __decorate([
        Component({
            selector: 'bm-input-search',
            styles: ["\n    bm-input-search {\n      display: inline-block;\n      position: relative; }\n\n    .bm-input-search__input {\n      padding: 0.3rem 3rem 0.3rem 1rem !important;\n      width: 100%;\n      box-sizing: border-box; }\n\n    .bm-input-search__input-container {\n      position: relative;\n      width: 100%;\n      height: 100%; }\n\n    .bm-input-search__search-button {\n      display: block;\n      position: absolute;\n      right: 0;\n      top: 1px;\n      bottom: 1px;\n      border-top-left-radius: 0;\n      border-bottom-left-radius: 0;\n      padding: 0 0.75em;\n      font-weight: 400; }\n\n    .bm-input-search--inverse .bm-input-search__input {\n      background-color: white; }\n\n    .bm-input-search__suggestions-container {\n      position: absolute;\n      top: 2.4em;\n      width: 100%;\n      max-height: 10em;\n      box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.09);\n      overflow-x: auto; }\n\n    .bm-input-search__suggestions-list {\n      list-style: none;\n      padding: 0;\n      margin: 0; }\n\n    .bm-input-search__suggestions-list-item {\n      padding: 0.2em 1em;\n      cursor: pointer; }\n      .bm-input-search__suggestions-list-item:nth-child(odd) {\n        background-color: white; }\n      .bm-input-search__suggestions-list-item:nth-child(even) {\n        background-color: var(--secondary-color, #bcbcbc); }\n      .bm-input-search__suggestions-list-item:hover, .bm-input-search__suggestions-list-item:focus {\n        background-color: var(--secondary-color-hover, #959494); }\n  "],
            template: "\n    <div class=\"bm-input bm-input-search__input-container\">\n      <input #input class=\"bm-input bm-input-search__input\"\n        autocomplete=\"off\"\n        [type]=\"type\"\n        [required]=\"required\"\n        [disabled]=\"disabled\"\n        [name]=\"name\"\n        [tabIndex]=\"tabIndex\"\n        [readonly]=\"readonly\"\n        [placeholder]=\"placeholder\"\n        [attr.aria-label]=\"ariaLabel\"\n        [attr.aria-labelledby]=\"ariaLabelledby\"\n        (keyup)=\"inputKeyUp($event)\"\n        (change)=\"inputChange($event)\"\n        (blur)=\"inputBlur()\"/>\n      <button class=\"bm-button bm-button--primary bm-input-search__search-button\"\n        (click)=\"search.emit(input.value)\"\n      >\n        <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\n      </button>\n    </div>\n\n    <div class=\"bm-input-search__suggestions-container\" *ngIf=\"showSuggestions\">\n      <ul class=\"bm-input-search__suggestions-list\">\n        <li i18n tabindex=\"-1\"\n          *ngIf=\"filteredSuggestions.length === 0\"\n          class=\"bm-input-search__suggestions-list-item\">No matches found</li>\n        <li #sugg tabindex=\"0\" *ngFor=\"let suggestion of filteredSuggestions; let i = index;\"\n          class=\"bm-input-search__suggestions-list-item\"\n          (click)=\"setInput(suggestion)\"\n          (keydown)=\"suggestionKeyUp($event, i, suggestion)\">\n          {{suggestion}}\n        </li>\n      </ul>\n    </div>\n  ",
            providers: [
                {
                    provide: NG_VALUE_ACCESSOR,
                    // tslint:disable-next-line:no-forward-ref
                    useExisting: forwardRef(function () { return BMInputSearchComponent_1; }),
                    multi: true,
                },
            ],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
        }),
        __metadata("design:paramtypes", [Renderer2])
    ], BMInputSearchComponent);
    return BMInputSearchComponent;
    var BMInputSearchComponent_1;
}());
export { BMInputSearchComponent };
//# sourceMappingURL=bm-input-search.component.js.map