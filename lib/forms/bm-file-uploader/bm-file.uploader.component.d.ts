/**
 * Angular Component BMFileUploader
 * Drag & Drop File uploader
 */
import { Renderer2 } from '@angular/core';
import { FileUploadBase } from '../file-uploader-base';
export declare class BMFileUploaderComponent extends FileUploadBase {
    numberFiles: number;
    multiple: boolean;
    dragHover: boolean;
    constructor(renderer: Renderer2);
    ondragover(event: DragEvent): void;
    ondragleave(): void;
    ondrop(event: DragEvent): void;
    onInputFileChange(event: Event): void;
}
