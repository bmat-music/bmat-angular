var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component BMFileUploader
 * Drag & Drop File uploader
 */
import { ChangeDetectionStrategy, Component, forwardRef, HostBinding, HostListener, Input, Renderer2, ViewEncapsulation, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FileUploadBase } from '../file-uploader-base';
var BMFileUploaderComponent = /** @class */ (function (_super) {
    __extends(BMFileUploaderComponent, _super);
    function BMFileUploaderComponent(renderer) {
        var _this = _super.call(this, renderer) || this;
        _this.numberFiles = 0;
        _this.dragHover = false;
        return _this;
    }
    BMFileUploaderComponent_1 = BMFileUploaderComponent;
    Object.defineProperty(BMFileUploaderComponent.prototype, "multiple", {
        get: function () {
            return this.numberFiles === 1;
        },
        set: function (v) {
            this.numberFiles = v ? 1 : 0;
        },
        enumerable: true,
        configurable: true
    });
    BMFileUploaderComponent.prototype.ondragover = function (event) {
        event.preventDefault();
        this.dragHover = true;
    };
    BMFileUploaderComponent.prototype.ondragleave = function () {
        this.dragHover = false;
    };
    BMFileUploaderComponent.prototype.ondrop = function (event) {
        event.dataTransfer.dropEffect = 'copy';
        var dataTransfer = event.dataTransfer;
        this.readFiles(dataTransfer.files);
        event.preventDefault();
    };
    BMFileUploaderComponent.prototype.onInputFileChange = function (event) {
        var input = event.target;
        event.preventDefault();
        if (input.files) {
            this.readFiles(input.files);
        }
    };
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], BMFileUploaderComponent.prototype, "multiple", null);
    __decorate([
        HostBinding('class.bm-file-upload--drag-hover'),
        __metadata("design:type", Object)
    ], BMFileUploaderComponent.prototype, "dragHover", void 0);
    __decorate([
        HostListener('dragover', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [DragEvent]),
        __metadata("design:returntype", void 0)
    ], BMFileUploaderComponent.prototype, "ondragover", null);
    __decorate([
        HostListener('dragleave'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], BMFileUploaderComponent.prototype, "ondragleave", null);
    __decorate([
        HostListener('drop', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [DragEvent]),
        __metadata("design:returntype", void 0)
    ], BMFileUploaderComponent.prototype, "ondrop", null);
    BMFileUploaderComponent = BMFileUploaderComponent_1 = __decorate([
        Component({
            selector: 'bm-file-upload',
            template: "\n    <input #inputFile type=\"file\" class=\"bm-visual-hidden\"\n      [multiple]=\"multiple\"\n      (change)=\"onInputFileChange($event)\"/>\n    <div #dropArea class=\"bm-file-upload-drop-area\">\n      <ng-content></ng-content>\n      <div>\n        <i class=\"fa fa-upload bm-file-upload__upload-icon\" aria-hidden=\"true\"></i>\n        <div i18n>\n          <p>Drag &amp; Drop your audio {numberFiles, plural, =0 {file} =1 {files}}</p>\n          <p>\n            <button class=\"bm-button bm-button--primary\" (click)=\"inputFile.click()\">select {numberFiles, plural, =0 {file} =1 {files}}</button>\n          </p>\n        </div>\n      </div>\n      <ng-content select=\"[bmFileUploadFooter]\"></ng-content>\n    </div>\n  ",
            styles: ["\n    .bm-file-upload--drag-hover .bm-file-upload-drop-area {\n      border-width: 2px; }\n\n    .bm-file-upload__upload-icon {\n      font-size: 3em; }\n\n    .bm-file-upload-drop-area {\n      box-sizing: border-box;\n      border: 1px dashed var(--header-background-color, #3e71ad);\n      border-radius: 3px;\n      display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n      text-align: center;\n      padding: 1em; }\n      .bm-file-upload-drop-area p {\n        font-size: 0.9rem;\n        margin: 0.5rem 0; }\n      .bm-file-upload-drop-area * {\n        pointer-events: none; }\n      .bm-file-upload-drop-area button {\n        pointer-events: auto; }\n  "],
            providers: [
                {
                    provide: NG_VALUE_ACCESSOR,
                    // tslint:disable-next-line:no-forward-ref
                    useExisting: forwardRef(function () { return BMFileUploaderComponent_1; }),
                    multi: true,
                },
            ],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
        }),
        __metadata("design:paramtypes", [Renderer2])
    ], BMFileUploaderComponent);
    return BMFileUploaderComponent;
    var BMFileUploaderComponent_1;
}(FileUploadBase));
export { BMFileUploaderComponent };
//# sourceMappingURL=bm-file.uploader.component.js.map