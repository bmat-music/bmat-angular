var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component. Checkbox with BMAT Style
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, forwardRef, HostBinding, Input, Renderer, ViewChild, ViewEncapsulation, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { animate, state, style, transition, trigger, } from '@angular/animations';
var BMCheckboxComponent = /** @class */ (function () {
    function BMCheckboxComponent(_renderer, _changeDetectorRef) {
        this._renderer = _renderer;
        this._changeDetectorRef = _changeDetectorRef;
        /**
         * Input values to directly copy to inner checkbox
         */
        this.name = '';
        this.tabIndex = 0;
        // tslint:disable-next-line:no-any no-empty
        this.onTouched = function () { };
        // tslint:disable:no-any
        // tslint:disable:no-empty
        this.propagateChange = function (_) { };
    }
    BMCheckboxComponent_1 = BMCheckboxComponent;
    Object.defineProperty(BMCheckboxComponent.prototype, "isFocused", {
        get: function () {
            return this._focused;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BMCheckboxComponent.prototype, "checked", {
        get: function () {
            return this.input.nativeElement.checked;
        },
        set: function (checked) {
            this._renderer.setElementProperty(this.input.nativeElement, 'checked', checked);
        },
        enumerable: true,
        configurable: true
    });
    // Focus Blur Handle
    BMCheckboxComponent.prototype.inputFocus = function () {
        this._focused = true;
    };
    BMCheckboxComponent.prototype.inputBlur = function () {
        this._focused = false;
        this.onTouched();
    };
    // tslint:disable-next-line:no-any
    BMCheckboxComponent.prototype.writeValue = function (value) {
        this.checked = value;
        this._changeDetectorRef.markForCheck();
    };
    // tslint:disable-next-line:no-any
    BMCheckboxComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    // tslint:disable-next-line:no-any
    BMCheckboxComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    BMCheckboxComponent.prototype.inputChange = function (event) {
        var checked = event.target.checked;
        this.propagateChange(checked);
    };
    BMCheckboxComponent.prototype.focus = function () {
        this._renderer.invokeElementMethod(this.input.nativeElement, 'focus');
    };
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], BMCheckboxComponent.prototype, "input", void 0);
    __decorate([
        HostBinding('class.bm-checkbox--focused'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], BMCheckboxComponent.prototype, "isFocused", null);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMCheckboxComponent.prototype, "name", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMCheckboxComponent.prototype, "tabIndex", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMCheckboxComponent.prototype, "required", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMCheckboxComponent.prototype, "disabled", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMCheckboxComponent.prototype, "readonly", void 0);
    __decorate([
        Input('aria-label'),
        __metadata("design:type", String)
    ], BMCheckboxComponent.prototype, "ariaLabel", void 0);
    __decorate([
        Input('aria-labelledby'),
        __metadata("design:type", String)
    ], BMCheckboxComponent.prototype, "ariaLabelledby", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Boolean])
    ], BMCheckboxComponent.prototype, "checked", null);
    BMCheckboxComponent = BMCheckboxComponent_1 = __decorate([
        Component({
            selector: 'bm-checkbox',
            template: "\n    <label class=\"bm-checkbox__label-container\">\n      <input #input type=\"checkbox\" class=\"bm-visual-hidden\"\n        [required]=\"required\"\n        [disabled]=\"disabled\"\n        [name]=\"name\"\n        [tabIndex]=\"tabIndex\"\n        [readonly]=\"readonly\"\n        [attr.aria-label]=\"ariaLabel\"\n        [attr.aria-labelledby]=\"ariaLabelledby\"\n        (change)=\"inputChange($event)\"\n        (focus)=\"inputFocus()\"\n        (blur)=\"inputBlur()\"\n      />\n      <div class=\"bm-checkbox__container\">\n        <svg *ngIf=\"checked\" [@fadeInOut]=\"'in'\" class=\"bm-checkbox__tick\"\n          xml:space=\"preserve\" version=\"1.1\" viewBox=\"0 0 24 24\" xmlns=\"http://www.w3.org/2000/svg\">\n          <path class=\"bm-checkbox__tick-path\" d=\"M4.1,12.7 9,17.6 20.3,6.3\" fill=\"none\" stroke=\"white\"></path>\n        </svg>\n      </div>\n      <span class=\"bm-checkbox__label\"><ng-content></ng-content></span>\n    </label>\n  ",
            styles: ["\n    .bm-checkbox--focused .bm-checkbox-container {\n      border-color: var(--bm-checkbox-path-color, #4399fd); }\n\n    .bm-checkbox__container {\n      border: 1px solid var(--secondary-color, #bcbcbc);\n      border-radius: 3px;\n      display: inline-block;\n      width: 1em;\n      height: 1em;\n      position: relative;\n      vertical-align: middle; }\n\n    .bm-checkbox__tick {\n      position: absolute; }\n\n    .bm-checkbox__tick-path {\n      stroke: var(--bm-checkbox-path-color, #4399fd);\n      stroke-width: 5px;\n      vertical-align: middle; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
            providers: [
                {
                    provide: NG_VALUE_ACCESSOR,
                    // tslint:disable-next-line:no-forward-ref
                    useExisting: forwardRef(function () { return BMCheckboxComponent_1; }),
                    multi: true,
                },
            ],
            animations: [
                trigger('fadeInOut', [
                    state('in', style({ opacity: 1 })),
                    transition('void => *', [
                        style({
                            opacity: 0,
                        }),
                        animate('0.2s ease-in'),
                    ]),
                    transition('* => void', [
                        animate('0.2s ease-out', style({
                            opacity: 0,
                        })),
                    ]),
                ]),
            ],
        }),
        __metadata("design:paramtypes", [Renderer,
            ChangeDetectorRef])
    ], BMCheckboxComponent);
    return BMCheckboxComponent;
    var BMCheckboxComponent_1;
}());
export { BMCheckboxComponent };
//# sourceMappingURL=bm-checkbox.component.js.map