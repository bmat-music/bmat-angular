/**
 * Angular Component. Checkbox with BMAT Style
 */
import { ChangeDetectorRef, ElementRef, Renderer } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export interface BMCheckboxChangeEvent {
    source: BMCheckboxComponent;
    checked: boolean;
}
export declare class BMCheckboxComponent implements ControlValueAccessor {
    private _renderer;
    private _changeDetectorRef;
    input: ElementRef;
    readonly isFocused: boolean;
    /**
     * Input values to directly copy to inner checkbox
     */
    name: string;
    tabIndex: number;
    required: boolean;
    disabled: boolean;
    readonly: boolean;
    ariaLabel: string;
    ariaLabelledby: string;
    checked: boolean;
    private _focused;
    constructor(_renderer: Renderer, _changeDetectorRef: ChangeDetectorRef);
    inputFocus(): void;
    inputBlur(): void;
    writeValue(value: any): void;
    registerOnChange(fn: (_: any) => {}): void;
    onTouched: () => any;
    registerOnTouched(fn: () => any): void;
    inputChange(event: Event): void;
    focus(): void;
    private propagateChange;
}
