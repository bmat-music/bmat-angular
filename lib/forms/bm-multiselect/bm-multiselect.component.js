var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component BMMultiselectComponent
 * Simple multiselect element that lists the selected after it.
 */
import { animate, state, style, transition, trigger, } from '@angular/animations';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, forwardRef, HostBinding, Input, QueryList, Renderer, ViewChild, ViewChildren, ViewEncapsulation, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
var BMMultiselectComponent = /** @class */ (function () {
    function BMMultiselectComponent(renderer, changeDetectorRef) {
        this.renderer = renderer;
        this.changeDetectorRef = changeDetectorRef;
        /**
         * Input values to directly copy to inner input
         */
        this.name = '';
        this.tabIndex = 0;
        this.options = [];
        this.availableOptions = [];
        this.filteredOptions = [];
        this.selectedOptionsList = [];
        this.selectedOptions = new Set();
        this.showSuggestions = false;
        // tslint:disable-next-line:no-any no-empty
        this.onTouched = function () { };
        // tslint:disable:no-any
        // tslint:disable:no-empty
        this.propagateChange = function (_) { };
    }
    BMMultiselectComponent_1 = BMMultiselectComponent;
    Object.defineProperty(BMMultiselectComponent.prototype, "isFocused", {
        get: function () {
            return this.focused;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BMMultiselectComponent.prototype, "value", {
        get: function () {
            return Array.from(this.selectedOptions.values());
        },
        set: function (v) {
            if (Array.isArray(v)) {
                this.selectedOptions = new Set(v);
            }
            this.updateListAndSuggestions();
            this.updateSelectedList();
        },
        enumerable: true,
        configurable: true
    });
    BMMultiselectComponent.prototype.selectOption = function (option, event) {
        var target = event.target;
        if (target.nextElementSibling) {
            target.nextElementSibling.focus();
        }
        else if (target.previousElementSibling) {
            target.previousElementSibling.focus();
        }
        this.selectedOptions.add(option.key);
        this.valueChange();
    };
    BMMultiselectComponent.prototype.removeOption = function (option) {
        this.selectedOptions.delete(option.key);
        this.valueChange();
    };
    BMMultiselectComponent.prototype.removeAll = function () {
        this.selectedOptions.clear();
        this.valueChange();
    };
    BMMultiselectComponent.prototype.addAll = function () {
        this.selectedOptions = new Set(this.options.map(function (item) { return item.key; }));
        this.valueChange();
        this.showSuggestions = false;
    };
    BMMultiselectComponent.prototype.inputKeyUp = function (event) {
        switch (event.keyCode) {
            case 40:// Down arrow
                this.showSuggestions = true;
                if (this.suggestionList.first) {
                    this.renderer.invokeElementMethod(this.suggestionList.first.nativeElement, 'focus');
                }
                break;
            case 27:// Escape
                this.showSuggestions = false;
                break;
            default:
                this.updateSuggestionList();
        }
    };
    BMMultiselectComponent.prototype.suggestionKeyUp = function (event, index, value) {
        var _this = this;
        var suggestionLi = this.suggestionList.toArray();
        var keyDown = function () {
            if (index === 0) {
                _this.renderer.invokeElementMethod(_this.suggestionList.last.nativeElement, 'focus');
            }
            else {
                _this.renderer.invokeElementMethod(suggestionLi[index - 1].nativeElement, 'focus');
            }
        };
        switch (event.keyCode) {
            case 40:// Up arrow
                if (index === this.filteredOptions.length - 1) {
                    this.renderer.invokeElementMethod(this.suggestionList.first.nativeElement, 'focus');
                }
                else {
                    this.renderer.invokeElementMethod(suggestionLi[index + 1].nativeElement, 'focus');
                }
                event.preventDefault();
                break;
            case 38:// Down arrow
                keyDown();
                event.preventDefault();
                break;
            case 27:// Escape
                this.showSuggestions = false;
                break;
            case 13:// Enter
                this.selectOption(value, event);
                keyDown();
                break;
            default:
                return;
        }
    };
    BMMultiselectComponent.prototype.selectedItemKeyup = function (event, value) {
        if (event.keyCode === 46 || event.keyCode === 8) {
            event.preventDefault();
            this.removeOption(value);
        }
    };
    BMMultiselectComponent.prototype.afterOptionFocus = function () {
        this.showSuggestions = false;
    };
    // Focus Blur Handle
    BMMultiselectComponent.prototype.inputFocus = function () {
        this.focused = true;
        this.showSuggestions = true;
    };
    BMMultiselectComponent.prototype.inputFocusout = function (event) {
        var relatedTarget = event.relatedTarget;
        var suggestions = this.suggestionList.map(function (item) { return item.nativeElement; });
        if (suggestions.indexOf(relatedTarget) === -1) {
            this.showSuggestions = false;
        }
        this.focused = false;
        this.onTouched();
    };
    BMMultiselectComponent.prototype.suggestionFocusout = function (event) {
        var target = event.target;
        var relatedTarget = event.relatedTarget;
        if (!relatedTarget ||
            target.parentElement !== relatedTarget.parentElement) {
            this.showSuggestions = false;
        }
    };
    BMMultiselectComponent.prototype.ngAfterViewInit = function () {
        this.updateListAndSuggestions();
    };
    // tslint:disable-next-line:no-any
    BMMultiselectComponent.prototype.writeValue = function (value) {
        this.value = value;
        this.changeDetectorRef.markForCheck();
    };
    // tslint:disable-next-line:no-any
    BMMultiselectComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    // tslint:disable-next-line:no-any
    BMMultiselectComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    BMMultiselectComponent.prototype.focus = function () {
        this.renderer.invokeElementMethod(this.input.nativeElement, 'focus');
    };
    BMMultiselectComponent.prototype.updateSelectedList = function () {
        var _this = this;
        this.selectedOptionsList = this.options.filter(function (option) {
            return _this.selectedOptions.has(option.key);
        });
    };
    BMMultiselectComponent.prototype.updateListAndSuggestions = function () {
        var _this = this;
        this.availableOptions = this.options.filter(function (option) { return !_this.selectedOptions.has(option.key); });
        this.updateSuggestionList();
    };
    BMMultiselectComponent.prototype.updateSuggestionList = function () {
        var value = this.input
            .nativeElement.value.toLowerCase();
        this.filteredOptions = this.availableOptions.filter(function (item) { return item.displayValue.toLowerCase().search(value) !== -1; });
    };
    BMMultiselectComponent.prototype.valueChange = function () {
        this.propagateChange(Array.from(this.selectedOptions.values()));
        this.updateListAndSuggestions();
        this.updateSelectedList();
        if (this.availableOptions.length === 0) {
            this.renderer.invokeElementMethod(this.input.nativeElement, 'focus');
        }
    };
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], BMMultiselectComponent.prototype, "input", void 0);
    __decorate([
        ViewChildren('sugg'),
        __metadata("design:type", QueryList)
    ], BMMultiselectComponent.prototype, "suggestionList", void 0);
    __decorate([
        HostBinding('class.bm-multiselect-focused'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], BMMultiselectComponent.prototype, "isFocused", null);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMMultiselectComponent.prototype, "name", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMMultiselectComponent.prototype, "tabIndex", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMMultiselectComponent.prototype, "required", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMMultiselectComponent.prototype, "disabled", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMMultiselectComponent.prototype, "readonly", void 0);
    __decorate([
        Input('aria-label'),
        __metadata("design:type", String)
    ], BMMultiselectComponent.prototype, "ariaLabel", void 0);
    __decorate([
        Input('aria-labelledby'),
        __metadata("design:type", String)
    ], BMMultiselectComponent.prototype, "ariaLabelledby", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], BMMultiselectComponent.prototype, "value", null);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], BMMultiselectComponent.prototype, "options", void 0);
    BMMultiselectComponent = BMMultiselectComponent_1 = __decorate([
        Component({
            selector: 'bm-multiselect',
            template: "\n    <div class=\"bm-multiselect-input-container\">\n    <input class=\"bm-multiselect-input bm-input\" #input autocomplete=\"off\"\n      [required]=\"required\"\n      [disabled]=\"disabled\"\n      [name]=\"name\"\n      [tabIndex]=\"tabIndex\"\n      [readonly]=\"readonly\"\n      [attr.aria-label]=\"ariaLabel\"\n      [attr.aria-labelledby]=\"ariaLabelledby\"\n      (keyup)=\"inputKeyUp($event)\"\n      (focus)=\"inputFocus()\"\n      (focusout)=\"inputFocusout($event)\"/>\n      <div *ngIf=\"showSuggestions\" tabindex=\"-1\" class=\"bm-multiselect-suggestions-container\" [@fadeInOut]=\"'in'\">\n        <ul>\n          <li i18n tabindex=\"-1\" *ngIf=\"filteredOptions.length === 0\">No matches found</li>\n          <li #sugg tabindex=\"0\" *ngFor=\"let option of filteredOptions; let i = index;\"\n            (click)=\"selectOption(option, $event)\"\n            (focusout)=\"suggestionFocusout($event)\"\n            (keydown)=\"suggestionKeyUp($event, i, option)\">\n            {{option.displayValue}}\n          </li>\n        </ul>\n      </div>\n      <button class=\"bm-multiselect-add-all\" i18n\n        (click)=\"addAll()\"\n        (focus)=\"afterOptionFocus()\">Add all</button>\n    </div>\n    <div *ngIf=\"value.length\" class=\"bm-multiselect-selected-options\">\n      <div class=\"bm-multiselect-button-container\">\n        <button class=\"bm-multiselect-remove-all\" i18n (click)=\"removeAll()\">Remove all</button>\n      </div>\n      <ul>\n        <li *ngFor=\"let option of selectedOptionsList\" tabindex=\"0\"\n          (keyup)=\"selectedItemKeyup($event, option)\">\n          <span>{{option.displayValue}}</span>\n          <button bmIconButton class=\"bm-multiselect-remove-item fa fa-trash\" arial-label=\"Remove this item\"\n            (click)=\"removeOption(option)\" i18n-arial-label></button>\n        </li>\n      </ul>\n    </div>\n  ",
            styles: ["\n    bm-multiselect {\n      display: inline-block; }\n\n    .bm-multiselect-input-container {\n      display: block;\n      position: relative; }\n\n    .bm-multiselect-input {\n      width: 100%;\n      box-sizing: border-box; }\n\n    .bm-multiselect-remove-all,\n    .bm-multiselect-remove-item,\n    .bm-multiselect-add-all {\n      background: none;\n      color: #4399fd;\n      border: none; }\n\n    .bm-multiselect-add-all {\n      position: absolute;\n      right: 0;\n      top: 0.5em; }\n\n    .bm-multiselect-suggestions-container {\n      position: absolute;\n      top: 1.8em;\n      width: 100%;\n      background: white;\n      max-height: 10em;\n      box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.09);\n      overflow-x: auto;\n      z-index: 1000; }\n      .bm-multiselect-suggestions-container ul {\n        list-style: none;\n        padding: 0;\n        margin: 0; }\n      .bm-multiselect-suggestions-container li {\n        padding: 0.2em 1em;\n        cursor: pointer; }\n      .bm-multiselect-suggestions-container li:nth-child(odd) {\n        background-color: white; }\n      .bm-multiselect-suggestions-container li:nth-child(even) {\n        background-color: var(--secondary-color, #bcbcbc); }\n      .bm-multiselect-suggestions-container li:hover,\n      .bm-multiselect-suggestions-container li:focus {\n        background-color: var(--secondary-color-hover, #959494); }\n\n    .bm-multiselect-selected-options {\n      text-align: left; }\n      .bm-multiselect-selected-options .bm-multiselect-button-container {\n        text-align: right;\n        padding: 1em 0; }\n      .bm-multiselect-selected-options .bm-multiselect-remove-item {\n        display: none; }\n      .bm-multiselect-selected-options ul {\n        margin: 0;\n        padding: 0;\n        max-height: 15em;\n        overflow-x: auto;\n        list-style-type: none; }\n      .bm-multiselect-selected-options li {\n        padding: 0.5em;\n        display: flex; }\n        .bm-multiselect-selected-options li:nth-child(odd) {\n          background-color: #f5f7f8; }\n        .bm-multiselect-selected-options li:focus {\n          background-color: var(--secondary-color-hover, #959494); }\n        .bm-multiselect-selected-options li:hover .bm-multiselect-remove-item {\n          display: block;\n          margin-right: 0;\n          margin-left: auto; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
            providers: [
                {
                    provide: NG_VALUE_ACCESSOR,
                    // tslint:disable-next-line:no-forward-ref
                    useExisting: forwardRef(function () { return BMMultiselectComponent_1; }),
                    multi: true,
                },
            ],
            animations: [
                trigger('fadeInOut', [
                    state('in', style({ opacity: 1 })),
                    transition('void => *', [
                        style({
                            opacity: 0,
                        }),
                        animate('0.2s ease-in'),
                    ]),
                    transition('* => void', [
                        animate('0.2s ease-out', style({
                            opacity: 0,
                        })),
                    ]),
                ]),
            ],
        }),
        __metadata("design:paramtypes", [Renderer,
            ChangeDetectorRef])
    ], BMMultiselectComponent);
    return BMMultiselectComponent;
    var BMMultiselectComponent_1;
}());
export { BMMultiselectComponent };
//# sourceMappingURL=bm-multiselect.component.js.map