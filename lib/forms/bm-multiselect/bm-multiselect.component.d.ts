import { AfterViewInit, ChangeDetectorRef, ElementRef, QueryList, Renderer } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export interface BMCheckboxChangeEvent {
    source: BMMultiselectComponent;
    value: string[];
}
export interface BMMultiselectOption {
    displayValue: string;
    key: string;
}
export declare class BMMultiselectComponent implements ControlValueAccessor, AfterViewInit {
    private renderer;
    private changeDetectorRef;
    input: ElementRef;
    suggestionList: QueryList<ElementRef>;
    readonly isFocused: boolean;
    /**
     * Input values to directly copy to inner input
     */
    name: string;
    tabIndex: number;
    required: boolean;
    disabled: boolean;
    readonly: boolean;
    ariaLabel: string;
    ariaLabelledby: string;
    value: string[];
    options: BMMultiselectOption[];
    availableOptions: BMMultiselectOption[];
    filteredOptions: BMMultiselectOption[];
    selectedOptionsList: BMMultiselectOption[];
    selectedOptions: Set<string>;
    showSuggestions: boolean;
    private focused;
    constructor(renderer: Renderer, changeDetectorRef: ChangeDetectorRef);
    selectOption(option: BMMultiselectOption, event: Event): void;
    removeOption(option: BMMultiselectOption): void;
    removeAll(): void;
    addAll(): void;
    inputKeyUp(event: KeyboardEvent): void;
    suggestionKeyUp(event: KeyboardEvent, index: number, value: BMMultiselectOption): void;
    selectedItemKeyup(event: KeyboardEvent, value: BMMultiselectOption): void;
    afterOptionFocus(): void;
    inputFocus(): void;
    inputFocusout(event: FocusEvent): void;
    suggestionFocusout(event: FocusEvent): void;
    ngAfterViewInit(): void;
    writeValue(value: string[]): void;
    registerOnChange(fn: (_: any) => {}): void;
    onTouched: () => any;
    registerOnTouched(fn: () => any): void;
    focus(): void;
    private propagateChange;
    private updateSelectedList();
    private updateListAndSuggestions();
    private updateSuggestionList();
    private valueChange();
}
