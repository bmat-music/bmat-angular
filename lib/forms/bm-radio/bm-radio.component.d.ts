import { ElementRef, Renderer2 } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export interface BMRadioComponentChangeEvent {
    source: BMRadioComponent;
    checked: boolean;
}
export declare class BMRadioComponent implements ControlValueAccessor {
    private _renderer;
    input: ElementRef;
    readonly isFocused: boolean;
    /**
     * Input values to directly copy to inner radio
     */
    name: string;
    value: string;
    tabIndex: number;
    required: boolean;
    disabled: boolean;
    readonly: boolean;
    ariaLabel: string;
    ariaLabelledby: string;
    checked: boolean;
    private _focused;
    constructor(_renderer: Renderer2);
    inputFocus(): void;
    inputBlur(): void;
    writeValue(value: any): void;
    registerOnChange(fn: (_: any) => {}): void;
    onTouched: () => any;
    registerOnTouched(fn: () => any): void;
    inputChange(): void;
    focus(): void;
    private propagateChange;
}
