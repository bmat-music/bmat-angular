var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component. Checkbox with BMAT Style
 */
import { animate, state, style, transition, trigger, } from '@angular/animations';
import { Component, ElementRef, forwardRef, HostBinding, Input, Renderer2, ViewChild, ViewEncapsulation, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
var BMRadioComponent = /** @class */ (function () {
    function BMRadioComponent(_renderer) {
        this._renderer = _renderer;
        /**
         * Input values to directly copy to inner radio
         */
        this.name = '';
        this.value = '';
        this.tabIndex = 0;
        // tslint:disable-next-line:no-any no-empty
        this.onTouched = function () { };
        // tslint:disable:no-any
        // tslint:disable:no-empty
        this.propagateChange = function (_) { };
    }
    BMRadioComponent_1 = BMRadioComponent;
    Object.defineProperty(BMRadioComponent.prototype, "isFocused", {
        get: function () {
            return this._focused;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BMRadioComponent.prototype, "checked", {
        get: function () {
            return this.input.nativeElement.checked;
        },
        set: function (checked) {
            if (checked !== this.checked) {
                this._renderer.setProperty(this.input.nativeElement, 'checked', checked);
            }
        },
        enumerable: true,
        configurable: true
    });
    BMRadioComponent.prototype.inputFocus = function () {
        this._focused = true;
    };
    BMRadioComponent.prototype.inputBlur = function () {
        this._focused = false;
        this.onTouched();
    };
    // tslint:disable-next-line:no-any
    BMRadioComponent.prototype.writeValue = function (value) {
        this.checked = value === this.value;
    };
    // tslint:disable-next-line:no-any
    BMRadioComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    // tslint:disable-next-line:no-any
    BMRadioComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    BMRadioComponent.prototype.inputChange = function () {
        this.propagateChange(this.value);
    };
    BMRadioComponent.prototype.focus = function () {
        this.input.nativeElement.focus();
    };
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], BMRadioComponent.prototype, "input", void 0);
    __decorate([
        HostBinding('class.bm-radio--focused'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], BMRadioComponent.prototype, "isFocused", null);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMRadioComponent.prototype, "name", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMRadioComponent.prototype, "value", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMRadioComponent.prototype, "tabIndex", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMRadioComponent.prototype, "required", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMRadioComponent.prototype, "disabled", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMRadioComponent.prototype, "readonly", void 0);
    __decorate([
        Input('aria-label'),
        __metadata("design:type", String)
    ], BMRadioComponent.prototype, "ariaLabel", void 0);
    __decorate([
        Input('aria-labelledby'),
        __metadata("design:type", String)
    ], BMRadioComponent.prototype, "ariaLabelledby", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Boolean])
    ], BMRadioComponent.prototype, "checked", null);
    BMRadioComponent = BMRadioComponent_1 = __decorate([
        Component({
            selector: 'bm-radio',
            template: "\n    <label class=\"bm-radio__label_container\">\n      <input #input type=\"radio\" class=\"bm-visual-hidden\"\n        [required]=\"required\"\n        [disabled]=\"disabled\"\n        [name]=\"name\"\n        [tabIndex]=\"tabIndex\"\n        [readonly]=\"readonly\"\n        [attr.aria-label]=\"ariaLabel\"\n        [attr.aria-labelledby]=\"ariaLabelledby\"\n        (change)=\"inputChange()\"\n        (focus)=\"inputFocus()\"\n        (blur)=\"inputBlur()\"\n      />\n      <div class=\"bm-radio__container\">\n        <span class=\"bm-radio__checked-mark\" *ngIf=\"checked\" [@fadeInOut]=\"'in'\">\n        </span>\n      </div>\n      <span class=\"bm-radio__label\"><ng-content></ng-content></span>\n    </label>\n  ",
            styles: ["\n    .bm-radio--focused .bm-radio-container {\n      border-color: var(--bm-checkbox-path-color, #4399fd); }\n\n    .bm-radio__container {\n      border: 1px solid var(--secondary-color-hover, #959494);\n      border-radius: 50%;\n      display: inline-block;\n      width: 1em;\n      height: 1em;\n      position: relative;\n      text-align: center;\n      vertical-align: middle; }\n\n    .bm-radio__checked-mark {\n      background-color: var(--bm-checkbox-path-color, #4399fd);\n      box-sizing: border-box;\n      border: 2px solid white;\n      border-radius: 50%;\n      width: 100%;\n      height: 100%;\n      display: block; }\n  "],
            encapsulation: ViewEncapsulation.None,
            providers: [
                {
                    provide: NG_VALUE_ACCESSOR,
                    // tslint:disable-next-line:no-forward-ref
                    useExisting: forwardRef(function () { return BMRadioComponent_1; }),
                    multi: true,
                },
            ],
            animations: [
                trigger('fadeInOut', [
                    state('in', style({ opacity: 1 })),
                    transition('void => *', [
                        style({
                            opacity: 0,
                        }),
                        animate('0.2s ease-in'),
                    ]),
                    transition('* => void', [
                        animate('0.2s ease-out', style({
                            opacity: 0,
                        })),
                    ]),
                ]),
            ],
        }),
        __metadata("design:paramtypes", [Renderer2])
    ], BMRadioComponent);
    return BMRadioComponent;
    var BMRadioComponent_1;
}());
export { BMRadioComponent };
//# sourceMappingURL=bm-radio.component.js.map