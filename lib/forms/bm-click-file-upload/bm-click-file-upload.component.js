var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component BMFileUploader
 * Drag & Drop File uploader
 */
import { ChangeDetectionStrategy, Component, forwardRef, HostBinding, HostListener, Input, Renderer2, ViewEncapsulation, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FileUploadBase } from '../file-uploader-base';
var BMClickFileUploaderComponent = /** @class */ (function (_super) {
    __extends(BMClickFileUploaderComponent, _super);
    function BMClickFileUploaderComponent(renderer) {
        var _this = _super.call(this, renderer) || this;
        _this.dragHover = false;
        return _this;
    }
    BMClickFileUploaderComponent_1 = BMClickFileUploaderComponent;
    BMClickFileUploaderComponent.prototype.ondragover = function (event) {
        event.preventDefault();
        this._onTouched(event);
        this.dragHover = true;
    };
    BMClickFileUploaderComponent.prototype.ondragleave = function () {
        this.dragHover = false;
    };
    BMClickFileUploaderComponent.prototype.ondrop = function (event) {
        event.dataTransfer.dropEffect = 'copy';
        var dataTransfer = event.dataTransfer;
        this.readFiles(dataTransfer.files);
        event.preventDefault();
    };
    BMClickFileUploaderComponent.prototype.onInputFileChange = function (event) {
        var input = event.target;
        event.preventDefault();
        if (input.files) {
            this.readFiles(input.files);
            this._onChange(input.files);
        }
    };
    BMClickFileUploaderComponent.prototype.onInputBlur = function (event) {
        this._onTouched(event);
    };
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], BMClickFileUploaderComponent.prototype, "multiple", void 0);
    __decorate([
        HostBinding('class.drag-hover'),
        __metadata("design:type", Object)
    ], BMClickFileUploaderComponent.prototype, "dragHover", void 0);
    __decorate([
        HostListener('dragover', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [DragEvent]),
        __metadata("design:returntype", void 0)
    ], BMClickFileUploaderComponent.prototype, "ondragover", null);
    __decorate([
        HostListener('dragleave'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], BMClickFileUploaderComponent.prototype, "ondragleave", null);
    __decorate([
        HostListener('drop', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [DragEvent]),
        __metadata("design:returntype", void 0)
    ], BMClickFileUploaderComponent.prototype, "ondrop", null);
    BMClickFileUploaderComponent = BMClickFileUploaderComponent_1 = __decorate([
        Component({
            selector: 'bm-click-file-upload',
            template: "\n    <input #inputFile type=\"file\" class=\"bm-visual-hidden\"\n      [multiple]=\"multiple\"\n      (blur)=\"onInputBlur($event)\"\n      (change)=\"onInputFileChange($event)\"/>\n    <div #dropArea class=\"bm-click-file-upload-drop-area\" (click)=\"inputFile.click()\">\n      <ng-content></ng-content>\n    </div>\n  ",
            styles: ["\n    .bm-click-file-upload-drop-area {\n      display: inline-block; }\n  "],
            providers: [
                {
                    provide: NG_VALUE_ACCESSOR,
                    // tslint:disable-next-line:no-forward-ref
                    useExisting: forwardRef(function () { return BMClickFileUploaderComponent_1; }),
                    multi: true,
                },
            ],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
        }),
        __metadata("design:paramtypes", [Renderer2])
    ], BMClickFileUploaderComponent);
    return BMClickFileUploaderComponent;
    var BMClickFileUploaderComponent_1;
}(FileUploadBase));
export { BMClickFileUploaderComponent };
//# sourceMappingURL=bm-click-file-upload.component.js.map