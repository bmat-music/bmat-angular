var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * BMAT Layout Module
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BMCheckboxComponent } from './bm-checkbox';
import { BMClickFileUploaderComponent } from './bm-click-file-upload';
import { BMDurationInputComponent } from './bm-duration-input';
import { BMFileUploaderComponent } from './bm-file-uploader';
import { BMMultiselectComponent } from './bm-multiselect';
import { BMRadioComponent } from './bm-radio';
import { BMInputSearchComponent } from './bm-search-input';
import { BMOptionDirective, BMSelectComponent } from './bm-select';
var BMATForms = /** @class */ (function () {
    function BMATForms() {
    }
    BMATForms_1 = BMATForms;
    // tslint:disable-next-line:function-name
    BMATForms.forRoot = function () {
        return {
            ngModule: BMATForms_1,
            providers: [],
        };
    };
    BMATForms = BMATForms_1 = __decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [
                BMCheckboxComponent,
                BMFileUploaderComponent,
                BMInputSearchComponent,
                BMRadioComponent,
                BMSelectComponent,
                BMDurationInputComponent,
                BMMultiselectComponent,
                BMOptionDirective,
                BMClickFileUploaderComponent,
            ],
            exports: [
                BMCheckboxComponent,
                BMFileUploaderComponent,
                BMInputSearchComponent,
                BMRadioComponent,
                BMSelectComponent,
                BMDurationInputComponent,
                BMMultiselectComponent,
                BMOptionDirective,
                BMClickFileUploaderComponent,
            ],
            providers: [],
        })
    ], BMATForms);
    return BMATForms;
    var BMATForms_1;
}());
export { BMATForms };
//# sourceMappingURL=forms.module.js.map