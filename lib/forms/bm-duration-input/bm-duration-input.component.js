var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component. Input to introduce duration in HH:MM:SS.
 * Maps the value to second when submiting forms or accessing DOM.
 */
import { ChangeDetectionStrategy, Component, ElementRef, forwardRef, Input, QueryList, Renderer2, ViewChildren, ViewEncapsulation, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
var BMDurationInputComponent = /** @class */ (function () {
    function BMDurationInputComponent(renderer) {
        this.renderer = renderer;
        // tslint:disable-next-line:prefer-array-literal
        this.timeValues = new Array(3).fill(null);
        // tslint:disable:no-any
        // tslint:disable:no-empty
        this.propagateChange = function (_) { };
    }
    BMDurationInputComponent_1 = BMDurationInputComponent;
    Object.defineProperty(BMDurationInputComponent.prototype, "value", {
        get: function () {
            return this.computeTotalTime();
        },
        set: function (val) {
            this.writeValue(val);
        },
        enumerable: true,
        configurable: true
    });
    BMDurationInputComponent.prototype.format = function (index, $event) {
        var inputEle = $event.target;
        var value = Number.parseInt(inputEle.value, 10);
        if (Number.isNaN(value)) {
            value = 0;
        }
        if (index && value >= 60) {
            value = (value % 100) % 60;
        }
        this.timeValues[index] = value;
        this.propagateChange(this.computeTotalTime());
        var formatedVal = this.timeValues[index] < 10
            ? '0' + this.timeValues[index]
            : this.timeValues[index];
        this.renderer.setProperty(inputEle, 'value', formatedVal);
    };
    // tslint:disable-next-line:no-any
    BMDurationInputComponent.prototype.writeValue = function (value) {
        var _this = this;
        if (!value && value !== 0) {
            // tslint:disable-next-line:prefer-array-literal
            this.timeValues = new Array(3).fill(null);
        }
        else {
            var coerceInteger = Number.parseInt(value, 10);
            if (Number.isNaN(coerceInteger)) {
                coerceInteger = 0;
            }
            coerceInteger = Math.max(0, coerceInteger);
            [
                Math.floor(coerceInteger / 3600),
                Math.floor(coerceInteger / 60) % 60,
                coerceInteger % 60,
            ].forEach(function (val, i) {
                _this.timeValues[i] = val;
            });
        }
        this.updateInputValues();
    };
    // tslint:disable-next-line:no-any
    BMDurationInputComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    // tslint:disable-next-line:no-empty
    BMDurationInputComponent.prototype.registerOnTouched = function () { };
    BMDurationInputComponent.prototype.ngAfterViewInit = function () {
        this.updateInputValues();
    };
    BMDurationInputComponent.prototype.computeTotalTime = function () {
        return this.timeValues.reduce(function (acum, curr, i) {
            if (curr) {
                acum += curr * Math.pow(60, (2 - i));
            }
            return acum;
        }, 0);
    };
    BMDurationInputComponent.prototype.updateInputValues = function () {
        var _this = this;
        if (this.viewTimeValues) {
            this.viewTimeValues.forEach(function (element, index) {
                var value = _this.timeValues[index];
                var formatedValue = value < 10 ? '0' + value : value;
                _this.renderer.setProperty(element.nativeElement, 'value', formatedValue);
            });
        }
    };
    __decorate([
        ViewChildren('hours, minutes, seconds', { read: ElementRef }),
        __metadata("design:type", QueryList)
    ], BMDurationInputComponent.prototype, "viewTimeValues", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], BMDurationInputComponent.prototype, "value", null);
    BMDurationInputComponent = BMDurationInputComponent_1 = __decorate([
        Component({
            selector: 'bm-input-duration',
            template: "\n    <input #hours type=\"number\" min=\"0\" class=\"bm-input bm-duration-input__input\"\n      (input)=\"format(0, $event)\" (keyup)=\"format(0, $event)\" placeholder=\"HH\" />\n    <input #minutes type=\"number\" min=\"0\" max=\"59\"  class=\"bm-input bm-duration-input__input\"\n      (input)=\"format(1, $event)\" (keyup)=\"format(1, $event)\" placeholder=\"MM\"/>\n    <input #seconds type=\"number\" min=\"0\" max=\"59\"  class=\"bm-input bm-duration-input__input\"\n      (input)=\"format(2, $event)\" (keyup)=\"format(2, $event)\" placeholder=\"SS\"/>\n  ",
            styles: ["\n    bm-input-duration {\n      display: inline-flex;\n      flex-wrap: nowrap; }\n\n    .bm-duration-input--inverse .bm-duration-input__input {\n      background-color: white; }\n\n    .bm-duration-input__input {\n      width: 3em; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
            providers: [
                {
                    provide: NG_VALUE_ACCESSOR,
                    // tslint:disable-next-line:no-forward-ref
                    useExisting: forwardRef(function () { return BMDurationInputComponent_1; }),
                    multi: true,
                },
            ],
        }),
        __metadata("design:paramtypes", [Renderer2])
    ], BMDurationInputComponent);
    return BMDurationInputComponent;
    var BMDurationInputComponent_1;
}());
export { BMDurationInputComponent };
//# sourceMappingURL=bm-duration-input.component.js.map