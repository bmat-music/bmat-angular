/**
 * Angular Component. Input to introduce duration in HH:MM:SS.
 * Maps the value to second when submiting forms or accessing DOM.
 */
import { AfterViewInit, ElementRef, QueryList, Renderer2 } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare class BMDurationInputComponent implements AfterViewInit, ControlValueAccessor {
    private renderer;
    viewTimeValues: QueryList<ElementRef>;
    value: number;
    private timeValues;
    constructor(renderer: Renderer2);
    format(index: number, $event: Event): void;
    writeValue(value: any): void;
    registerOnChange(fn: (_: any) => {}): void;
    registerOnTouched(): void;
    ngAfterViewInit(): void;
    private propagateChange;
    private computeTotalTime();
    private updateInputValues();
}
