export interface ISO3166Alpha2Conutry {
    name: string;
    code: string;
}
/**
 * ISO 3166-2 2 letter country list
 */
export declare const iso3116Alpha2: ISO3166Alpha2Conutry[];
