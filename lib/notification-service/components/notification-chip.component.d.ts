export declare class NotificationChipComponent {
    message: string;
    iconClass: string;
}
