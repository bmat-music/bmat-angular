var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { animate, state, style, transition, trigger, } from '@angular/animations';
import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation, } from '@angular/core';
var NotificationChipComponent = /** @class */ (function () {
    /**
     * Angular Component
     */
    function NotificationChipComponent() {
    }
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NotificationChipComponent.prototype, "message", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], NotificationChipComponent.prototype, "iconClass", void 0);
    NotificationChipComponent = __decorate([
        Component({
            selector: 'bm-notification-chip',
            template: "\n    <div *ngIf=\"message\"\n      class=\"bm-notification-chip-content\" \n      [@flyInOut]=\"'in'\"\n    >\n      <i *ngIf=\"iconClass\" [className]=\"iconClass\"></i>\n      {{message}}\n    </div>\n  ",
            styles: ["\n    bm-notification-chip {\n      position: fixed;\n      top: 0;\n      left: 0;\n      right: 0;\n      height: 0;\n      display: block;\n      text-align: center; }\n      bm-notification-chip .bm-notification-chip-content {\n        display: inline-block;\n        background-color: #f7fef0;\n        border-bottom-left-radius: 3px;\n        border-bottom-right-radius: 3px;\n        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);\n        padding: 0.5em 1em; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
            animations: [
                trigger('flyInOut', [
                    state('in', style({ opacity: 1 })),
                    transition('void => *', [style({ opacity: 0 }), animate(100)]),
                    transition('* => void', [animate(100, style({ opacity: 0 }))]),
                ]),
            ],
        })
        /**
         * Angular Component
         */
    ], NotificationChipComponent);
    return NotificationChipComponent;
}());
export { NotificationChipComponent };
//# sourceMappingURL=notification-chip.component.js.map