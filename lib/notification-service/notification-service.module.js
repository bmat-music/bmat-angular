var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { CommonModule, DOCUMENT } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationChipComponent } from './components/notification-chip.component';
var ɵ0 = document;
var NotificationServiceModule = /** @class */ (function () {
    /**
     * Notification service
     */
    function NotificationServiceModule() {
    }
    NotificationServiceModule = __decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [NotificationChipComponent],
            entryComponents: [NotificationChipComponent],
            providers: [
                {
                    provide: DOCUMENT,
                    useValue: ɵ0,
                },
            ],
        })
        /**
         * Notification service
         */
    ], NotificationServiceModule);
    return NotificationServiceModule;
}());
export { NotificationServiceModule };
export { ɵ0 };
//# sourceMappingURL=notification-service.module.js.map