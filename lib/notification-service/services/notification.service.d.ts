import { ApplicationRef, ComponentFactoryResolver } from '@angular/core';
export declare class NotificationService {
    private _document;
    private _componentFactoryResolver;
    private _appRef;
    constructor(_document: Document, _componentFactoryResolver: ComponentFactoryResolver, _appRef: ApplicationRef);
    showMessage(message: string, iconClass?: string): void;
}
