var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { DOCUMENT } from '@angular/common';
import { ApplicationRef, ComponentFactoryResolver, Inject, Injectable, ReflectiveInjector, } from '@angular/core';
import { NotificationServiceModule } from '../notification-service.module';
import { NotificationChipComponent } from './../components/notification-chip.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "../notification-service.module";
var NotificationService = /** @class */ (function () {
    function NotificationService(_document, _componentFactoryResolver, _appRef) {
        this._document = _document;
        this._componentFactoryResolver = _componentFactoryResolver;
        this._appRef = _appRef;
    }
    NotificationService.prototype.showMessage = function (message, iconClass) {
        if (iconClass === void 0) { iconClass = ''; }
        var componentFactory = this._componentFactoryResolver.resolveComponentFactory(NotificationChipComponent);
        var injector = ReflectiveInjector.resolveAndCreate([]);
        var notificationChip = componentFactory.create(injector);
        notificationChip.instance.message = message;
        notificationChip.instance.iconClass = iconClass;
        notificationChip.changeDetectorRef.detectChanges();
        this._appRef.attachView(notificationChip.hostView);
        var domElem = notificationChip.hostView
            .rootNodes[0];
        this._document.body.appendChild(domElem);
        setTimeout(function () {
            notificationChip.destroy();
        }, 2000);
    };
    NotificationService.ngInjectableDef = i0.defineInjectable({ factory: function NotificationService_Factory() { return new NotificationService(i0.inject(i1.DOCUMENT), i0.inject(i0.ComponentFactoryResolver), i0.inject(i0.ApplicationRef)); }, token: NotificationService, providedIn: i2.NotificationServiceModule });
    NotificationService = __decorate([
        Injectable({
            providedIn: NotificationServiceModule,
        })
        /**
         * Service to display notificacions
         */
        ,
        __param(0, Inject(DOCUMENT)),
        __metadata("design:paramtypes", [Document, ComponentFactoryResolver,
            ApplicationRef])
    ], NotificationService);
    return NotificationService;
}());
export { NotificationService };
//# sourceMappingURL=notification.service.js.map