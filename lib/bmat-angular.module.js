var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * BMAT Angular Module
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BMATForms } from './forms';
import { BMATLayout } from './layout';
import { BMATMediaPlayers } from './media-players';
import { BMATSharedModule } from './shared';
import { BMATWidgets } from './widgets';
var BMATAngular = /** @class */ (function () {
    function BMATAngular() {
    }
    BMATAngular_1 = BMATAngular;
    // tslint:disable-next-line:function-name
    BMATAngular.forRoot = function () {
        return {
            ngModule: BMATAngular_1,
            providers: [],
        };
    };
    BMATAngular = BMATAngular_1 = __decorate([
        NgModule({
            imports: [
                BMATForms,
                BMATLayout,
                BMATMediaPlayers,
                BMATSharedModule,
                BMATWidgets,
                CommonModule,
            ],
            declarations: [],
            exports: [
                BMATForms,
                BMATLayout,
                BMATMediaPlayers,
                BMATSharedModule,
                BMATWidgets,
            ],
            providers: [],
        })
    ], BMATAngular);
    return BMATAngular;
    var BMATAngular_1;
}());
export { BMATAngular };
//# sourceMappingURL=bmat-angular.module.js.map