var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component. BMMediaOutput
 * Used in media players. To transclude and allow definition of custom controls
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, ViewEncapsulation, } from '@angular/core';
import { ParseDurationPipe } from '../../shared/pipes/format-duration';
var BMMediaOutputComponent = /** @class */ (function () {
    function BMMediaOutputComponent(changeDetectorRef) {
        this.changeDetectorRef = changeDetectorRef;
        this.format = 'MM:SS';
        this.formater = new ParseDurationPipe();
        this.changeDetectorRef.detach();
    }
    Object.defineProperty(BMMediaOutputComponent.prototype, "time", {
        set: function (v) {
            if (v || v === 0) {
                this.innerTime = this.formater.transform(Math.max(0, v), this.format);
                this.changeDetectorRef.markForCheck();
            }
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMMediaOutputComponent.prototype, "format", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], BMMediaOutputComponent.prototype, "time", null);
    BMMediaOutputComponent = __decorate([
        Component({
            selector: 'bm-media-output',
            template: "\n    <span class=\"bm-media-output__content\">{{innerTime}}</span>\n  ",
            styles: ["\n    .bm-media-output__content {\n      padding: 0 0.4em; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
        }),
        __metadata("design:paramtypes", [ChangeDetectorRef])
    ], BMMediaOutputComponent);
    return BMMediaOutputComponent;
}());
export { BMMediaOutputComponent };
//# sourceMappingURL=bm-media-output.component.js.map