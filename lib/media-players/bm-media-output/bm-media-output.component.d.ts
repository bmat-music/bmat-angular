/**
 * Angular Component. BMMediaOutput
 * Used in media players. To transclude and allow definition of custom controls
 */
import { ChangeDetectorRef } from '@angular/core';
export declare class BMMediaOutputComponent {
    private changeDetectorRef;
    format: string;
    time: number;
    innerTime: string;
    private formater;
    constructor(changeDetectorRef: ChangeDetectorRef);
}
