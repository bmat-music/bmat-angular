var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component. Audio media player
 */
import { ChangeDetectionStrategy, Component, ContentChild, ElementRef, EventEmitter, Input, Output, Renderer, ViewChild, ViewEncapsulation, } from '@angular/core';
import { timer } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BMProgressComponent } from '../../shared/components/progress-bar';
import { BMMediaOutputComponent } from '../bm-media-output';
var BMATAudioPlayerComponent = /** @class */ (function () {
    function BMATAudioPlayerComponent(renderer) {
        this.renderer = renderer;
        // Forward attributes to audio
        this.autoplay = false;
        this.controls = false;
        this.loop = false;
        this.muted = false;
        this.preload = 'none';
        /**
         * Echo of play HTMLAudioElement event
         */
        this.play = new EventEmitter();
        /**
         * Echo of play HTMLAudioElement event
         */
        this.pause = new EventEmitter();
        this.isDisabled = true;
        this.isLoading = false;
        this.isPlaying = false;
        this._isMouseDown = false;
        this._progressUpdater$ = timer(500, 500).pipe(tap(this.calculateElapsed.bind(this)));
    }
    BMATAudioPlayerComponent_1 = BMATAudioPlayerComponent;
    Object.defineProperty(BMATAudioPlayerComponent.prototype, "src", {
        set: function (v) {
            if (v) {
                this.renderer.setElementProperty(this.audioElement.nativeElement, 'src', v);
                this.isDisabled = false;
            }
            else {
                this.isDisabled = true;
            }
        },
        enumerable: true,
        configurable: true
    });
    BMATAudioPlayerComponent.prototype.ngAfterContentInit = function () {
        if (this.progressBar) {
            this.progressBar.value = 0;
            this.progressBar.staticBar = true;
        }
        if (this.mediaOutput) {
            this.mediaOutput.time = 0;
        }
    };
    BMATAudioPlayerComponent.prototype.onButtonMousedown = function (event) {
        var _this = this;
        event.preventDefault();
        this._mouseDownTime = new Date().getTime();
        this._isMouseDown = true;
        this._playBackTimerSubscription = timer(BMATAudioPlayerComponent_1.HOLD_TIME).subscribe(function () {
            var now = new Date().getTime();
            var deltaTime = now - _this._mouseDownTime;
            if (_this._isMouseDown && deltaTime > BMATAudioPlayerComponent_1.HOLD_TIME) {
                _this.renderer.setElementProperty(_this.audioElement.nativeElement, 'playbackRate', BMATAudioPlayerComponent_1.ACCELERATED_RATE);
            }
        });
    };
    BMATAudioPlayerComponent.prototype.onloadeddata = function (event) {
        event.stopPropagation();
        this.isLoading = true;
    };
    BMATAudioPlayerComponent.prototype.ondurationchange = function () {
        this.calculateElapsed();
    };
    BMATAudioPlayerComponent.prototype.oncanplay = function (event) {
        event.stopPropagation();
        this.isLoading = false;
    };
    BMATAudioPlayerComponent.prototype.onplay = function (event) {
        event.stopPropagation();
        this.isPlaying = true;
        this.play.emit();
        if (this.progressBar) {
            this._timerSubscription = this._progressUpdater$.subscribe();
        }
    };
    BMATAudioPlayerComponent.prototype.onplaying = function (event) {
        event.stopPropagation();
        this.isPlaying = true;
        this.isLoading = false;
    };
    BMATAudioPlayerComponent.prototype.onpause = function (event) {
        event.stopPropagation();
        this.isPlaying = false;
        this.pause.emit();
        if (this._timerSubscription) {
            this._timerSubscription.unsubscribe();
        }
    };
    BMATAudioPlayerComponent.prototype.onwaiting = function (event) {
        event.stopPropagation();
        this.isLoading = true;
    };
    BMATAudioPlayerComponent.prototype.onended = function (event) {
        this.isPlaying = false;
        event.stopPropagation();
        if (this._timerSubscription) {
            this._timerSubscription.unsubscribe();
            this.progressBar.value = 100;
        }
    };
    BMATAudioPlayerComponent.prototype.togglePlay = function () {
        this.renderer.setElementProperty(this.audioElement.nativeElement, 'playbackRate', 1);
        this._isMouseDown = false;
        var now = new Date().getTime();
        var deltaTime = now - this._mouseDownTime;
        this._playBackTimerSubscription.unsubscribe();
        if (deltaTime > BMATAudioPlayerComponent_1.HOLD_TIME) {
            return;
        }
        if (this.isPlaying) {
            this.renderer.invokeElementMethod(this.audioElement.nativeElement, 'pause');
        }
        else {
            this.renderer.invokeElementMethod(this.audioElement.nativeElement, 'play');
        }
    };
    BMATAudioPlayerComponent.prototype.calculateElapsed = function () {
        var nativeAudio = this.audioElement.nativeElement;
        if (this.progressBar) {
            var currentPosition = Math.min(100, 100 * nativeAudio.currentTime / nativeAudio.duration);
            this.progressBar.value =
                !Number.isNaN(currentPosition) && currentPosition > 0
                    ? currentPosition
                    : 0;
        }
        if (this.mediaOutput) {
            this.mediaOutput.time = nativeAudio.currentTime;
        }
    };
    /**
     * Threshold of time to consider a mousedown a hold event.
     */
    BMATAudioPlayerComponent.HOLD_TIME = 200;
    /**
     * Playback rate when playing audio in accelerated mode.
     */
    BMATAudioPlayerComponent.ACCELERATED_RATE = 3;
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMATAudioPlayerComponent.prototype, "autoplay", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMATAudioPlayerComponent.prototype, "controls", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMATAudioPlayerComponent.prototype, "loop", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMATAudioPlayerComponent.prototype, "muted", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], BMATAudioPlayerComponent.prototype, "preload", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], BMATAudioPlayerComponent.prototype, "src", null);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], BMATAudioPlayerComponent.prototype, "play", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], BMATAudioPlayerComponent.prototype, "pause", void 0);
    __decorate([
        ViewChild('audio'),
        __metadata("design:type", ElementRef)
    ], BMATAudioPlayerComponent.prototype, "audioElement", void 0);
    __decorate([
        ContentChild(BMProgressComponent),
        __metadata("design:type", BMProgressComponent)
    ], BMATAudioPlayerComponent.prototype, "progressBar", void 0);
    __decorate([
        ContentChild(BMMediaOutputComponent),
        __metadata("design:type", BMMediaOutputComponent)
    ], BMATAudioPlayerComponent.prototype, "mediaOutput", void 0);
    BMATAudioPlayerComponent = BMATAudioPlayerComponent_1 = __decorate([
        Component({
            selector: 'bm-audio-player',
            template: "\n    <audio #audio\n      [autoplay]=\"autoplay\"\n      [controls]=\"controls\"\n      [loop]=\"loop\"\n      [muted]=\"muted\"\n      [preload]=\"preload\"\n      (loadeddata)=\"onloadeddata($event)\"\n      (durationchange)=\"ondurationchange()\"\n      (canplay)=\"oncanplay($event)\"\n      (play)=\"onplay($event)\"\n      (playing)=\"onplaying($event)\"\n      (pause)=\"onpause($event)\"\n      (waiting)=\"onwaiting($event)\"\n      (ended)=\"onended($event)\"\n    ></audio>\n    <button class=\"bm-audio-player__button\" aria-label=\"Toggle audio play\" i18n-aria-label\n      [class.bm-audio-player__button--playing]=\"isPlaying\"\n      [disabled]=\"isDisabled\"\n      (mousedown)=\"onButtonMousedown($event)\"\n      (mouseup)=\"togglePlay()\">\n      <i class=\"fa\" aria-hidden=\"true\"\n        [ngClass]=\"{\n          'fa-youtube-play': !isLoading,\n          'fa-spinner': isLoading\n        }\"\n      ></i>\n    </button>\n    <ng-content select=\"bm-progress, bm-media-output\"></ng-content>\n  ",
            styles: ["\n    bm-audio-player {\n      display: inline-flex;\n      align-items: center; }\n\n    .bm-audio-player__button {\n      border: 0;\n      background: none;\n      padding: 0;\n      font-size: 2rem;\n      margin: 0 0.2em;\n      color: var(--secondary-color, #bcbcbc); }\n      .bm-audio-player__button--playing {\n        color: var(--header-background-color, #3e71ad); }\n      .bm-audio-player__button[disabled] {\n        color: var(--header-color, #f8f8f8); }\n\n    .fa-spinner {\n      transform-origin: 50% 50%;\n      animation: spin 2s linear infinite; }\n\n    @keyframes spin {\n      0% {\n        transform: rotate(0deg); }\n      100% {\n        transform: rotate(360deg); } }\n    bm-progress bm-audio-player,\n    bm-progress bm-media-output {\n      margin: 0 0.2em; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
        }),
        __metadata("design:paramtypes", [Renderer])
    ], BMATAudioPlayerComponent);
    return BMATAudioPlayerComponent;
    var BMATAudioPlayerComponent_1;
}());
export { BMATAudioPlayerComponent };
//# sourceMappingURL=bm-audio.component.js.map