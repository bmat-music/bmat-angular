/**
 * Angular Component. Audio media player
 */
import { AfterContentInit, ElementRef, EventEmitter, Renderer } from '@angular/core';
export declare type PreloadValues = 'auto' | 'metadata' | 'none';
export declare class BMATAudioPlayerComponent implements AfterContentInit {
    private renderer;
    /**
     * Threshold of time to consider a mousedown a hold event.
     */
    static HOLD_TIME: number;
    /**
     * Playback rate when playing audio in accelerated mode.
     */
    static ACCELERATED_RATE: number;
    autoplay: boolean;
    controls: boolean;
    loop: boolean;
    muted: boolean;
    preload: PreloadValues;
    src: string;
    /**
     * Echo of play HTMLAudioElement event
     */
    play: EventEmitter<null>;
    /**
     * Echo of play HTMLAudioElement event
     */
    pause: EventEmitter<null>;
    audioElement: ElementRef;
    isDisabled: boolean;
    isLoading: boolean;
    isPlaying: boolean;
    private progressBar;
    private mediaOutput;
    private _mouseDownTime;
    private _timerSubscription;
    private _playBackTimerSubscription;
    private _isMouseDown;
    private _progressUpdater$;
    constructor(renderer: Renderer);
    ngAfterContentInit(): void;
    onButtonMousedown(event: MouseEvent): void;
    onloadeddata(event: Event): void;
    ondurationchange(): void;
    oncanplay(event: Event): void;
    onplay(event: Event): void;
    onplaying(event: Event): void;
    onpause(event: Event): void;
    onwaiting(event: Event): void;
    onended(event: Event): void;
    togglePlay(): void;
    private calculateElapsed();
}
