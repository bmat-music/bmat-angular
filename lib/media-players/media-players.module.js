var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * BMAT MediaPlayers Module
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BMATAudioPlayerComponent } from './audio';
import { BMMediaOutputComponent } from './bm-media-output';
import { BMATSharedModule } from '../shared';
var BMATMediaPlayers = /** @class */ (function () {
    function BMATMediaPlayers() {
    }
    BMATMediaPlayers_1 = BMATMediaPlayers;
    // tslint:disable-next-line:function-name
    BMATMediaPlayers.forRoot = function () {
        return {
            ngModule: BMATMediaPlayers_1,
            providers: [],
        };
    };
    BMATMediaPlayers = BMATMediaPlayers_1 = __decorate([
        NgModule({
            imports: [CommonModule, BMATSharedModule],
            declarations: [BMATAudioPlayerComponent, BMMediaOutputComponent],
            exports: [BMATAudioPlayerComponent, BMMediaOutputComponent],
            providers: [],
        })
    ], BMATMediaPlayers);
    return BMATMediaPlayers;
    var BMATMediaPlayers_1;
}());
export { BMATMediaPlayers };
//# sourceMappingURL=media-players.module.js.map