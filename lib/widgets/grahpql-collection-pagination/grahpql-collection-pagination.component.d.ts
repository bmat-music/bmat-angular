import { EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GraphQLPagination } from './graphql-collection-pagination.interfaces';
export declare class BMGraphQLPaginationComponent {
    pageInput: FormGroup;
    pagination: GraphQLPagination;
    goToPage: EventEmitter<number>;
    numberPages: number;
    isFirst: boolean;
    isLast: boolean;
    inputWidth: string;
    private currentPage;
    deltaPage($event: Event, delta: number): void;
    goToFirst($event: Event): void;
    goToLast($event: Event): void;
    submit($event: Event): void;
}
