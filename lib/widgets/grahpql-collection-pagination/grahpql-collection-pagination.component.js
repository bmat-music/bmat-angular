var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation, } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
var BMGraphQLPaginationComponent = /** @class */ (function () {
    function BMGraphQLPaginationComponent() {
        this.pageInput = new FormGroup({
            page: new FormControl(1),
        });
        this.goToPage = new EventEmitter();
        this.inputWidth = '5rem';
    }
    Object.defineProperty(BMGraphQLPaginationComponent.prototype, "pagination", {
        set: function (v) {
            if (v) {
                this.currentPage = 1 + Math.floor(v.start / v.limit);
                this.numberPages = Math.ceil(v.total / v.limit);
                this.isFirst = v.start === 0;
                this.isLast = v.length + v.start >= v.total;
                this.pageInput.patchValue({
                    page: this.currentPage,
                });
                this.inputWidth = 2 + this.numberPages.toString().length + 'rem';
            }
        },
        enumerable: true,
        configurable: true
    });
    BMGraphQLPaginationComponent.prototype.deltaPage = function ($event, delta) {
        $event.preventDefault();
        var destination = Math.min(Math.max(1, this.currentPage + delta), this.numberPages);
        this.goToPage.emit(destination);
    };
    BMGraphQLPaginationComponent.prototype.goToFirst = function ($event) {
        $event.preventDefault();
        this.goToPage.emit(1);
    };
    BMGraphQLPaginationComponent.prototype.goToLast = function ($event) {
        $event.preventDefault();
        this.goToPage.emit(this.numberPages);
    };
    BMGraphQLPaginationComponent.prototype.submit = function ($event) {
        $event.preventDefault();
        var destination = Math.min(Math.max(1, this.pageInput.value.page), this.numberPages);
        this.goToPage.emit(destination);
    };
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], BMGraphQLPaginationComponent.prototype, "pagination", null);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], BMGraphQLPaginationComponent.prototype, "goToPage", void 0);
    BMGraphQLPaginationComponent = __decorate([
        Component({
            selector: 'bm-graphql-pagination',
            template: "\n    <form (ngSubmit)=\"submit($event)\" [formGroup]=\"pageInput\" *ngIf=\"numberPages > 1\">\n      <button class=\"fa fa-angle-double-left bm-graphql-pagination__button\"\n        aria-label=\"Go to the first page\" i18n-aria-label\n        (click)=\"goToFirst($event)\"\n        [disabled]=\"isFirst\"></button>\n      <button class=\"fa fa-angle-left bm-graphql-pagination__button\"\n        (click)=\"deltaPage($event, -1)\"\n        aria-label=\"Go to previous page\" i18n-aria-label\n        [disabled]=\"isFirst\"></button>\n      <input type=\"number\"\n        class=\"bm-graphql-pagination__input\"\n        min=\"0\" [max]=\"numberPages\" \n        [style.width]=\"inputWidth\"\n        formControlName=\"page\" i18n-aria-label aria-label=\"Set page to go\" />\n      <button type=\"submit\" class=\"bm-button bm-button--primary\" i18n>Go</button>\n      <span i18n>of</span>\n      {{numberPages}}\n      <button class=\"fa fa-angle-right bm-graphql-pagination__button\"\n        (click)=\"deltaPage($event, 1)\"\n        [disabled]=\"isLast\"\n        aria-label=\"Go to next page\" i18n-aria-label></button>\n      <button class=\"fa fa-angle-double-right bm-graphql-pagination__button\"\n        (click)=\"goToLast($event)\"\n        aria-label=\"Go to the last page\" i18n-aria-labe\n        [disabled]=\"isLast\"></button>\n    </form>\n  ",
            styles: ["\n    bm-graphql-pagination {\n      display: inline-block; }\n\n    .bm-graphql-pagination__button {\n      border: none;\n      background: none;\n      color: #4399fd; }\n      .bm-graphql-pagination__button:disabled {\n        color: #333; }\n\n    .bm-graphql-pagination__input {\n      padding: 6px 0 6px 16px; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            encapsulation: ViewEncapsulation.None,
        })
        /**
         * Angular Component. Displays controls of pagination in tables
         */
    ], BMGraphQLPaginationComponent);
    return BMGraphQLPaginationComponent;
}());
export { BMGraphQLPaginationComponent };
//# sourceMappingURL=grahpql-collection-pagination.component.js.map