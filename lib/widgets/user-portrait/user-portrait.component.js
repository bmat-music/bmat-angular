var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component BMUserPortraitComponent
 * Dropdown user optuions
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, } from '@angular/core';
var BMUserPortraitComponent = /** @class */ (function () {
    function BMUserPortraitComponent(changeDetectorRef) {
        this.changeDetectorRef = changeDetectorRef;
        this.validSrc = false;
    }
    Object.defineProperty(BMUserPortraitComponent.prototype, "src", {
        get: function () {
            return this.srcCopy;
        },
        set: function (v) {
            var _this = this;
            this.srcCopy = v;
            this.validSrc = false;
            this.avatarImg = new Image(100, 100);
            this.avatarImg.src = this.srcCopy;
            this.avatarImg.onload = function () {
                _this.validSrc = true;
                _this.changeDetectorRef.markForCheck();
            };
            this.avatarImg.onerror = function () {
                _this.validSrc = false;
                _this.changeDetectorRef.markForCheck();
            };
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], BMUserPortraitComponent.prototype, "src", null);
    BMUserPortraitComponent = __decorate([
        Component({
            selector: 'bm-user-portrait',
            template: "\n    <ng-template #iconFiller><i class=\"bm-user-avatar bm-icon-customer\" aria-hidden=\"true\"></i></ng-template>\n    <img *ngIf=\"validSrc; else iconFiller;\" class=\"bm-user-avatar\" [src]=\"src\" alt=\"User Avatar\" i18n-alt />\n  ",
            styles: ["\n    :host {\n      display: inline-block; }\n      :host * {\n        vertical-align: middle; }\n\n    i.bm-user-avatar {\n      font-size: 2em; }\n\n    img.bm-user-avatar {\n      width: 2em;\n      height: 2em;\n      border-radius: 50%; }\n\n    :host(.x2) i.bm-user-avatar {\n      font-size: 4em; }\n\n    :host(.x2) img.bm-user-avatar {\n      width: 4em;\n      height: 4em;\n      border-radius: 50%; }\n\n    :host(.x3) i.bm-user-avatar {\n      font-size: 6em; }\n\n    :host(.x3) img.bm-user-avatar {\n      width: 6em;\n      height: 6em;\n      border-radius: 50%; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
        }),
        __metadata("design:paramtypes", [ChangeDetectorRef])
    ], BMUserPortraitComponent);
    return BMUserPortraitComponent;
}());
export { BMUserPortraitComponent };
//# sourceMappingURL=user-portrait.component.js.map