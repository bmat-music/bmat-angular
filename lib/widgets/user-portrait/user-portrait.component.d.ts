/**
 * Angular Component BMUserPortraitComponent
 * Dropdown user optuions
 */
import { ChangeDetectorRef } from '@angular/core';
export declare class BMUserPortraitComponent {
    private changeDetectorRef;
    src: string;
    validSrc: boolean;
    avatarImg: HTMLImageElement;
    private srcCopy;
    constructor(changeDetectorRef: ChangeDetectorRef);
}
