var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component BMUserPortraitComponent
 * Dropdown user optuions
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, } from '@angular/core';
var BMUserDetailComponent = /** @class */ (function () {
    function BMUserDetailComponent() {
        this.viewDetails = new EventEmitter();
    }
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMUserDetailComponent.prototype, "userData", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], BMUserDetailComponent.prototype, "viewDetails", void 0);
    BMUserDetailComponent = __decorate([
        Component({
            selector: 'bm-user-profile-detail',
            template: "\n    <h3>{{userData?.name}}</h3>\n    <p class=\"email\">{{userData?.email}}</p>\n    <p class=\"button-container\">\n      <button class=\"bm-button bm-button--primary bm-header-view-profile\" (click)=\"viewDetails.emit()\">View profile</button>\n    </p>\n  ",
            styles: ["\n    :host {\n      display: inline-block;\n      width: 22em;\n      max-width: 90%;\n      flex: 1 1 0; }\n      :host .email {\n        color: var(--header-background-color, #3e71ad);\n        font-size: 80%; }\n      :host .button-container {\n        margin-top: 1em; }\n\n    h3,\n    p {\n      margin: 0; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
        })
    ], BMUserDetailComponent);
    return BMUserDetailComponent;
}());
export { BMUserDetailComponent };
//# sourceMappingURL=header-user-profile-detail.component.js.map