/**
 * Angular Component BMUserPortraitComponent
 * Dropdown user optuions
 */
import { EventEmitter } from '@angular/core';
import { UserData } from '../../header-user-options';
export declare class BMUserDetailComponent {
    userData: UserData;
    viewDetails: EventEmitter<null>;
}
