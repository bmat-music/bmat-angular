/**
 * Interfaces of header user options
 */
export interface LanguageOption {
    code: string;
    name: string;
}
export interface UserData {
    name: string;
    email: string;
    avatarSrc?: string;
    selectedLanguage?: string;
}
