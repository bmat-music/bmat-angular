var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component BMHeaderUserOptionsComponent
 * Dropdown user optuions
 */
import { animate, state, style, transition, trigger, } from '@angular/animations';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, HostBinding, HostListener, Input, Output, ViewChild, } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
var BMHeaderUserOptionsComponent = /** @class */ (function () {
    function BMHeaderUserOptionsComponent(elementRef, changeDetectorRef) {
        this.elementRef = elementRef;
        this.changeDetectorRef = changeDetectorRef;
        this.ariaHaspopup = true;
        this.open = false;
        this.userData = {
            name: 'Admin',
            email: 'johnsmit@bmat.com',
            selectedLanguage: 'en',
        };
        this.viewProfile = new EventEmitter();
        this.changeLanguage = new EventEmitter();
        this.logOut = new EventEmitter();
        this.languageSelector = new FormGroup({
            selectedLanguage: new FormControl(),
        });
    }
    BMHeaderUserOptionsComponent.prototype.documentClick = function ($event) {
        var outside = true;
        var target = $event.target;
        var container = this.elementRef.nativeElement;
        if (!this.open) {
            return;
        }
        while (target !== document.body && outside) {
            if (target === container) {
                outside = false;
            }
            target = target.parentNode;
        }
        if (outside) {
            this.toggleOpen();
        }
        this.changeDetectorRef.detectChanges();
    };
    BMHeaderUserOptionsComponent.prototype.toggleOpen = function () {
        this.open = !this.open;
    };
    BMHeaderUserOptionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userData) {
            this.languageSelector.patchValue(this.userData, { emitEvent: false });
        }
        this.languageSelector.valueChanges.subscribe(function (val) {
            return _this.changeLanguage.emit(val);
        });
    };
    __decorate([
        HostBinding('attr.aria-haspopup'),
        __metadata("design:type", Object)
    ], BMHeaderUserOptionsComponent.prototype, "ariaHaspopup", void 0);
    __decorate([
        HostBinding('attr.aria-pressed'),
        __metadata("design:type", Object)
    ], BMHeaderUserOptionsComponent.prototype, "open", void 0);
    __decorate([
        ViewChild('toggleContainer'),
        __metadata("design:type", ElementRef)
    ], BMHeaderUserOptionsComponent.prototype, "toggleContainer", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMHeaderUserOptionsComponent.prototype, "userData", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], BMHeaderUserOptionsComponent.prototype, "availableLanguages", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], BMHeaderUserOptionsComponent.prototype, "viewProfile", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], BMHeaderUserOptionsComponent.prototype, "changeLanguage", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], BMHeaderUserOptionsComponent.prototype, "logOut", void 0);
    __decorate([
        HostListener('document:click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], BMHeaderUserOptionsComponent.prototype, "documentClick", null);
    BMHeaderUserOptionsComponent = __decorate([
        Component({
            selector: 'bm-header-user-options',
            template: "\n    <button #toggleContainer class=\"bm-header-toggle\" tabindex=\"0\" (click)=\"toggleOpen()\" aria-label=\"Toggle user profile\" i18n-aria-label>\n      <bm-user-portrait [src]=\"userData?.avatarSrc\"></bm-user-portrait>\n      <span class=\"bm-header-user-name\">{{ userData?.name }}</span><i class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i>\n    </button>\n    <div *ngIf=\"open\" class=\"bm-header-user-data-container\" [@flyInOut]=\"'in'\">\n      <svg class=\"bm-header-user-caret-box\" viewBox=\"0 0 100 50\" xmlns=\"http://www.w3.org/2000/svg\">\n        <polygon points=\"0,50 50,0 100,50\" />\n      </svg>\n      <div class=\"bm-header-user-data\">\n        <bm-user-portrait class=\"x2\" [src]=\"userData?.avatarSrc\"></bm-user-portrait>\n        <bm-user-profile-detail\n          [userData]=\"userData\"\n          (viewProfile)=\"viewProfile.emit()\"></bm-user-profile-detail>\n        <bm-header-profile-languages \n          *ngIf=\"availableLanguages && availableLanguages.length\"\n          [form]=\"languageSelector\"\n          [languageList]=\"availableLanguages\"></bm-header-profile-languages>\n         <div class=\"bm-header-user-log-out\">\n           <button class=\"bm-button bm-button--primary-inverse\" style=\"border: 0\" i18n (click)=\"logOut.emit()\">\n            <i class=\"fa fa-sign-out fa-2x\" aria-hidden=\"true\"></i>Sign out\n           </button>\n          </div>\n      </div>\n    </div>\n  ",
            styles: ["\n    :host {\n      font-size: large;\n      position: relative; }\n      :host .bm-header-toggle {\n        text-align: right;\n        color: white;\n        background: none;\n        border: 0 none;\n        font-size: 0.815rem; }\n\n    .bm-header-user-name {\n      padding: 0 0.5em;\n      vertical-align: middle; }\n\n    .bm-header-user-avatar {\n      display: inline-block;\n      width: 2em;\n      height: 2em; }\n      .bm-header-user-avatar * {\n        display: inline-block;\n        font-size: 2em;\n        vertical-align: middle; }\n\n    .bm-header-caret {\n      text-align: right; }\n\n    .bm-header-user-data-container {\n      position: absolute;\n      right: -16px;\n      z-index: 1000;\n      top: calc(48px - 0.5em);\n      width: 30rem;\n      max-width: 100vw;\n      background-color: transparent; }\n\n    .bm-header-user-data {\n      background-color: white;\n      box-shadow: 0 0 10px 0 rgba(143, 143, 143, 0.5);\n      border-radius: 2px;\n      width: 100%;\n      height: 100%;\n      box-sizing: border-box;\n      padding: 1em;\n      display: flex;\n      flex-wrap: wrap;\n      justify-content: flex-start; }\n      .bm-header-user-data bm-user-portrait {\n        width: 4em;\n        color: black; }\n      .bm-header-user-data bm-user-profile-detail {\n        flex: 1 1;\n        color: black;\n        padding: 0 1em; }\n\n    svg.bm-header-user-caret-box {\n      display: block;\n      width: 1em;\n      height: 0.5em;\n      margin-left: auto;\n      margin-right: 1em; }\n      svg.bm-header-user-caret-box polygon {\n        fill: white; }\n\n    bm-header-profile-languages {\n      flex: 1 1 100%; }\n\n    .bm-header-user-log-out {\n      flex: 1 1 100%;\n      text-align: center;\n      border-top: 1px solid var(--secondary-color, #bcbcbc);\n      margin-top: 0.5em;\n      padding-top: 0.5em; }\n      .bm-header-user-log-out * {\n        vertical-align: middle; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
            animations: [
                trigger('flyInOut', [
                    state('in', style({ transform: 'translateX(0)' })),
                    transition('void => *', [
                        style({ transform: 'translateX(100%)' }),
                        animate(100),
                    ]),
                    transition('* => void', [
                        animate(100, style({ transform: 'translateX(100%)' })),
                    ]),
                ]),
            ],
        }),
        __metadata("design:paramtypes", [ElementRef,
            ChangeDetectorRef])
    ], BMHeaderUserOptionsComponent);
    return BMHeaderUserOptionsComponent;
}());
export { BMHeaderUserOptionsComponent };
//# sourceMappingURL=header-user-options.component.js.map