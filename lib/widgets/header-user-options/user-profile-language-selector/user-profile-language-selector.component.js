var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component BMUserProfileLanguageComponent
 */
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
var BMUserProfileLanguageComponent = /** @class */ (function () {
    function BMUserProfileLanguageComponent() {
    }
    __decorate([
        Input(),
        __metadata("design:type", FormGroup)
    ], BMUserProfileLanguageComponent.prototype, "form", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], BMUserProfileLanguageComponent.prototype, "languageList", void 0);
    BMUserProfileLanguageComponent = __decorate([
        Component({
            selector: 'bm-header-profile-languages',
            template: "\n    <h4 i18n>Language</h4>\n    <div [formGroup]=\"form\">\n      <bm-radio *ngFor=\"let language of languageList\"\n        name=\"language_select\"\n        formControlName=\"selectedLanguage\"\n        [value]=\"language.code\">{{language.name}}</bm-radio>\n    </div>\n  ",
            styles: ["\n    :host {\n      color: initial;\n      margin: 0.7em 0; }\n\n    h4 {\n      text-transform: uppercase;\n      margin: 0;\n      font-size: 90%; }\n\n    bm-radio {\n      margin-right: 0.5em;\n      font-size: 80%; }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
        })
    ], BMUserProfileLanguageComponent);
    return BMUserProfileLanguageComponent;
}());
export { BMUserProfileLanguageComponent };
//# sourceMappingURL=user-profile-language-selector.component.js.map