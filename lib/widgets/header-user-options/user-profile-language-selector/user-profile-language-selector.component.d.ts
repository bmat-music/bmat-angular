import { FormGroup } from '@angular/forms';
import { LanguageOption } from '../header-user-options.interfaces';
export declare class BMUserProfileLanguageComponent {
    form: FormGroup;
    languageList: LanguageOption[];
}
