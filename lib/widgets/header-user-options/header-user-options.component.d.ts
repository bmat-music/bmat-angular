import { ChangeDetectorRef, ElementRef, EventEmitter, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LanguageOption, UserData } from './header-user-options.interfaces';
export declare class BMHeaderUserOptionsComponent implements OnInit {
    private elementRef;
    private changeDetectorRef;
    ariaHaspopup: boolean;
    open: boolean;
    toggleContainer: ElementRef;
    userData: UserData;
    availableLanguages: LanguageOption[];
    viewProfile: EventEmitter<null>;
    changeLanguage: EventEmitter<string>;
    logOut: EventEmitter<null>;
    languageSelector: FormGroup;
    constructor(elementRef: ElementRef, changeDetectorRef: ChangeDetectorRef);
    documentClick($event: Event): void;
    toggleOpen(): void;
    ngOnInit(): void;
}
