var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * BMAT Widget Module
 * Module with different quasi-static components
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BMATForms } from '../forms';
import { BMBmatMenuIconComponent } from './bmat-menu-icon';
import { BMGraphQLPaginationComponent } from './grahpql-collection-pagination';
import { BMHeaderUserOptionsComponent, BMUserDetailComponent, BMUserProfileLanguageComponent, } from './header-user-options';
import { BMPoweredByComponent } from './powered-by';
import { BMUserPortraitComponent } from './user-portrait';
var BMATWidgets = /** @class */ (function () {
    function BMATWidgets() {
    }
    BMATWidgets_1 = BMATWidgets;
    // tslint:disable-next-line:function-name
    BMATWidgets.forRoot = function () {
        return {
            ngModule: BMATWidgets_1,
            providers: [],
        };
    };
    BMATWidgets = BMATWidgets_1 = __decorate([
        NgModule({
            imports: [
                BMATForms,
                CommonModule,
                ReactiveFormsModule,
            ],
            declarations: [
                BMBmatMenuIconComponent,
                BMGraphQLPaginationComponent,
                BMHeaderUserOptionsComponent,
                BMPoweredByComponent,
                BMUserDetailComponent,
                BMUserPortraitComponent,
                BMUserProfileLanguageComponent,
            ],
            exports: [
                BMBmatMenuIconComponent,
                BMGraphQLPaginationComponent,
                BMHeaderUserOptionsComponent,
                BMPoweredByComponent,
            ],
            providers: [],
        })
    ], BMATWidgets);
    return BMATWidgets;
    var BMATWidgets_1;
}());
export { BMATWidgets };
//# sourceMappingURL=widgets.module.js.map