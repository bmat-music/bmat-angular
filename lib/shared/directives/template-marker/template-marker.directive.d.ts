import { TemplateRef, ViewContainerRef } from '@angular/core';
export declare class BMTemplateMarkerDirective {
    templateRef: TemplateRef<any>;
    viewContainerRef: ViewContainerRef;
    /**
     * To name the kind of the template. Values depend on the
     * parent component.
     */
    bmTemplate: string;
    constructor(templateRef: TemplateRef<any>, viewContainerRef: ViewContainerRef);
}
