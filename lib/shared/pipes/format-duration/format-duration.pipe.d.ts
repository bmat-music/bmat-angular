/**
 * Angular Pipe. ParseDurationPipe.
 * Transforms second to HH:MM:SS
 */
import { PipeTransform } from '@angular/core';
export declare class ParseDurationPipe implements PipeTransform {
    transform(value: any, format?: string): string;
}
