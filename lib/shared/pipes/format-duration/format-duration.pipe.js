var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Angular Pipe. ParseDurationPipe.
 * Transforms second to HH:MM:SS
 */
import { Pipe } from '@angular/core';
var ParseDurationPipe = /** @class */ (function () {
    function ParseDurationPipe() {
    }
    // tslint:disable-next-line:no-any
    ParseDurationPipe.prototype.transform = function (value, format) {
        if (format === void 0) { format = 'HH:MM:SS'; }
        var coerceInteger = Number.parseInt(value, 10);
        if (Number.isNaN(coerceInteger)) {
            return 'N/A';
        }
        var seconds = coerceInteger % 60;
        var minutes = Math.floor(coerceInteger / 60) % 60;
        var hours = Math.floor(coerceInteger / 3600);
        return format
            .split(':')
            .map(function (segment) {
            var length = segment.length;
            if (length === 0) {
                throw new TypeError('Invalid format. Empty segment.');
            }
            if (length > 2) {
                throw new TypeError('Invalid format for segment ' + segment);
            }
            if (segment.length === 2 && segment[0] !== segment[1]) {
                throw new TypeError('Invalid format for segment ' + segment);
            }
            var val;
            switch (segment[0]) {
                case 'H':
                    val = hours.toString();
                    break;
                case 'M':
                    val = minutes.toString();
                    break;
                case 'S':
                    val = seconds.toString();
                    break;
                default:
                    throw new TypeError('Invalid format for segment ' + segment);
            }
            while (val.length < length) {
                val = '0' + val;
            }
            return val;
        })
            .join(':');
    };
    ParseDurationPipe = __decorate([
        Pipe({
            name: 'bmParseDuration',
        })
    ], ParseDurationPipe);
    return ParseDurationPipe;
}());
export { ParseDurationPipe };
//# sourceMappingURL=format-duration.pipe.js.map