var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * BMAT Core Module
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BMProgressComponent } from './components/progress-bar';
import { BMCollapsableDirective } from './directives/collapsable';
import { BMTemplateMarkerDirective } from './directives/template-marker';
import { ParseDurationPipe } from './pipes/format-duration';
var BMATSharedModule = /** @class */ (function () {
    function BMATSharedModule() {
    }
    BMATSharedModule_1 = BMATSharedModule;
    // tslint:disable-next-line:function-name
    BMATSharedModule.forRoot = function () {
        return {
            ngModule: BMATSharedModule_1,
            providers: [],
        };
    };
    BMATSharedModule = BMATSharedModule_1 = __decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [
                BMCollapsableDirective,
                BMProgressComponent,
                BMTemplateMarkerDirective,
                ParseDurationPipe,
            ],
            exports: [
                BMCollapsableDirective,
                BMProgressComponent,
                BMTemplateMarkerDirective,
                ParseDurationPipe,
            ],
            providers: [],
        })
    ], BMATSharedModule);
    return BMATSharedModule;
    var BMATSharedModule_1;
}());
export { BMATSharedModule };
//# sourceMappingURL=shared.module.js.map