import { ModuleWithProviders } from '@angular/core';
export declare class BMATSharedModule {
    static forRoot(): ModuleWithProviders;
}
