/**
 * Angular Component BMProgressBar
 * Polyfill/Stylable progress bar
 */
import { ChangeDetectorRef } from '@angular/core';
export declare class BMProgressComponent {
    private changeDetectorRef;
    max: number;
    value: number;
    /**
     * Sets if the bar should display or not an animation after value changes
     */
    staticBar: boolean;
    innerStyle: {
        width?: string;
    };
    constructor(changeDetectorRef: ChangeDetectorRef);
}
