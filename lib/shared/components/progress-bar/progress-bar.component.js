var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Angular Component BMProgressBar
 * Polyfill/Stylable progress bar
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding, Input, } from '@angular/core';
var BMProgressComponent = /** @class */ (function () {
    function BMProgressComponent(changeDetectorRef) {
        this.changeDetectorRef = changeDetectorRef;
        this.max = 100;
        /**
         * Sets if the bar should display or not an animation after value changes
         */
        this.staticBar = false;
        this.innerStyle = {};
    }
    Object.defineProperty(BMProgressComponent.prototype, "value", {
        get: function () {
            if (this.innerStyle.width) {
                return Number.parseFloat(this.innerStyle.width.slice(0, -1));
            }
            return 0;
        },
        set: function (v) {
            if ((!v && v !== 0) || Number.isNaN(v)) {
                this.innerStyle = {};
            }
            else {
                var width = v / this.max;
                this.innerStyle = {
                    width: Math.round(10000 * width) / 100 + '%',
                };
            }
            this.changeDetectorRef.markForCheck();
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], BMProgressComponent.prototype, "max", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], BMProgressComponent.prototype, "value", null);
    __decorate([
        Input(),
        HostBinding('attr.static'),
        __metadata("design:type", Object)
    ], BMProgressComponent.prototype, "staticBar", void 0);
    BMProgressComponent = __decorate([
        Component({
            selector: 'bm-progress',
            template: "\n    <div class=\"bm-progress-bar\">\n        <span class=\"bm-inner-progress-bar\" [ngStyle]=\"innerStyle\"></span>\n    </div>\n  ",
            styles: ["\n    :host {\n      display: inline-block;\n      min-width: 10em;\n      max-width: 100%; }\n\n    .bm-progress-bar {\n      height: 0.5em;\n      border-radius: 0.4em;\n      width: 100%;\n      background-color: var(--secondary-color-hover, #959494);\n      position: relative; }\n\n    :host(:not([static=true])) .bm-inner-progress-bar {\n      transition: width 2s; }\n\n    .bm-inner-progress-bar {\n      height: 0.5em;\n      border-radius: 0.4em;\n      display: block;\n      background-color: var(--header-background-color, #3e71ad); }\n  "],
            changeDetection: ChangeDetectionStrategy.OnPush,
        }),
        __metadata("design:paramtypes", [ChangeDetectorRef])
    ], BMProgressComponent);
    return BMProgressComponent;
}());
export { BMProgressComponent };
//# sourceMappingURL=progress-bar.component.js.map