/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const webpack = require('webpack');

module.exports = {
  context: path.join(__dirname, '../src'),
  devtool: 'inline-source-map',
  output: {},
  module: {
    loaders: [
      {
        enforce: 'pre',
        test: /\.ts$/,
        loader: 'tslint-loader',
        options: {
          configFile: 'tslint.json',
        },
        exclude: ['node_modules'],
      },
      {
        test: /\.ts$/,
        loaders: [
          {
            loader: 'awesome-typescript-loader',
            query: {
              useForkChecker: true,
              configFileName: path.resolve(__dirname, '..', 'tsconfig.json')
            },
          },
          'angular2-template-loader',
        ],
      },
      {
        test: /\.ts$/,
        enforce: 'post',
        loader: 'istanbul-instrumenter-loader',
        exclude: /(s|S)pec\.ts$/,
        query: {
          esModules: true,
        },
      },
      {
        test: /\.html$/,
        loader: 'raw-loader',
      },
      {
        test: /\.(css|scss)$/,
        loader: 'raw-loader!sass-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.ts', '.css', '.scss'],
    modules: [
      path.resolve(__dirname, '../src'),
     'node_modules',
    ],
  },
  plugins: [
    new webpack.ContextReplacementPlugin(
      // The (\\|\/) piece accounts for path separators in *nix and Windows
      /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
      path.resolve(__dirname, '../src'),
      {} // a map of your routes
    ),
  ],
};
