const webpack = require('webpack');
const webpackMerge = require('webpack-merge'); // used to merge webpack configs
const devServerPort = process.env.SERVER_PORT || 4200;
const commonConfig = require('./webpack.common.config');
const hostName = process.env.HOST_NAME || 'localhost';
const copyWebpackPlugin = require('copy-webpack-plugin');

module.exports = webpackMerge(commonConfig, {
  devtool: 'source-map',
  devServer: {
    historyApiFallback: true,
    stats: 'minimal',
    hot: false,
    headers: { "Access-Control-Allow-Origin": "*" },
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: false,
      debug: true,
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
    }),
    new copyWebpackPlugin([{
      from: 'src/assets', to: 'assets',
    }]),
  ],
});
