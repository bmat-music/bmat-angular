const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader');

const outputPath = path.resolve(__dirname, '..', 'dist');

function orderChuncks(a, b) {
  const order = [
    'polyfills', 'vendor', 'main',
  ];
  return order.indexOf(a.names[0]) - order.indexOf(b.names[0]);
}

module.exports = {
  context: path.join(__dirname, '..'),
  mode: 'development',
  entry: {
    polyfills: 'polyfills.ts',
    main: 'main.ts',
    styles: 'styles.scss',
    packageStyles: '_package_styles.scss',
  },
  output: {
    path: outputPath,
    filename: '[name].js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loaders: [
          {
            loader: 'awesome-typescript-loader',
            query: {
              useForkChecker: true,
              configFileName: path.resolve(__dirname, '..', 'tsconfig.json')
            },
          }, 
          {
            loader: 'angular2-template-loader',
          }
        ],
        exclude: [/\.(spec|e2e|d)\.ts$/],
      },
      {
        test: /\.html$/,
        loader: 'raw-loader',
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loader: 'url-loader?limit=5000&name=[name].[ext]',
      },
      {
        test: /\.(css|scss)$/,
        include: [
          path.resolve(__dirname, '../src/lib'),
          path.resolve(__dirname, '../src/demo-app'),
        ],
        loader: 'raw-loader!sass-loader',
      },
      {
        test: /\.(css|scss)$/,
        exclude: [
          path.resolve(__dirname, '../src/lib'),
          path.resolve(__dirname, '../src/demo-app'),
        ],
        loaders: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
             query: {
              sourceMap: true,
              importLoaders: 1,
            },
          },
          {
            loader: 'resolve-url-loader',
             query: {
              sourceMap: true,
              importLoaders: 1,
            },
          },
          {
            loader: 'sass-loader',
            query: {
              sourceMap: true,
              includePaths: [
                'node_modules',
              ],
            },
          },
        ],
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loaders: [
          {
            loader: 'url-loader',
            query: {
              limit: 10000,
              mimetype: 'application/font-woff',
              name: 'fonts/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)(\?.+)?$/,
        loader: 'url-loader?name=public/fonts/[name].[ext]',
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.ts', '.css', '.scss'],
    modules: [
      'node_modules',
      'src',
    ],
  },
  plugins: [
    new CheckerPlugin(),
    new webpack.ContextReplacementPlugin(
      // The (\\|\/) piece accounts for path separators in *nix and Windows
      /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
      path.resolve(__dirname, '../src'),
      {} // a map of your routes
    ),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: 'index.html',
      hash: true,
      minify: false,
      inject: true,
      chunksSortMode: orderChuncks,
    }),
  ],
};
