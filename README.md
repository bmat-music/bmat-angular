# Readme
This is a set an Angular library that provides reusable classes that follow
BMAT style guide. It has been writed using Typescript.

## Installation
From the source folder, run
```bash
npm install
```

This package uses **@bmat/fonts** and **@bmat/icons** packages. They are not 
included in the package as dependencies because they can be re-imported 
in other **@bmat/\*** packages or published in a CDN.

## Testing
Once installed, run
```bash
npm run test
```
Inside the **coverage** folder, you can find both a HTML and a JSON
coverage report.

## Linting
Both Typescript and SASS files can be linted. For Typescript, run
Once installed, run
```bash
npm run lint
```
For SASS, run 
```bash
npm run lint:sass
```

## Building
You can build the app using
```bash
npm run build
```
This will compile *SASS* styles, embedd templates and styles inside Angular 
components and output to **lib** folder.

Inside **package.json**, you can find the subtasks of *build*.

## Provided classes
### Components
| Module | Component | Status |
| --- | --- | --- | --- |
| Data-Table | [Data-Table](src/lib/data-table/README.md) | In progress |
| Forms | [Checkbox](src/lib/forms/bm-checkbox/README.md) | In progress |
| Forms | [Duration Input](src/lib/forms/bm-duration-input/README.md) | Done |
| Forms | [File Uploader](src/lib/forms/bm-file-uploader/README.md) | Done |
| Forms | [Radio Button](src/lib/forms/bm-radio/README.md) | Done |
| Forms | [Search Input](src/lib/forms/bm-search-input/README.md) | In progress |
| Forms | [Multiselect](src/lib/forms/bm-multiselect/README.md) | In progress |
| Layout | [Sidenav layout](src/lib/layout/bm-sidenav/README.md) | In progress |
| Media Players | [Audio Player](src/lib/media-players/audio/README.md) | In progress |
| Shared | [Progress bar](src/lib/shared/components/progress-bar/README.md) | Done |
| Widgets | [Header User options](src/lib/widget/header-users-options/README.md) | Done |
| Widgets | [Powered By BMAT](src/lib/widget/powered-by/README.md) | Done |
| Widgets | [User portrait](src/lib/widget/user-portrait/README.md) | Done |

### Directives
| Module | Component | Status |
| --- | --- | --- | --- |
| Widgets | [Icon button](src/lib/widget/icon-button/README.md) | Done |

### Services

### Pipes
| Module | Component | Status |
| --- | --- | --- | --- |
| Shared | [Format Duration](src/lib/shared/pipes/format-duration/README.md) | Done |

## DemoApp
This package includes a demo page. Run
```bash
npm run start
```
and open [http://localhost:4200](http://localhost:4200)
